import {Route, Switch, useHistory, useRouteMatch} from "react-router-dom";
import React, {lazy, Suspense, useEffect} from "react";
import queryString from "query-string";
import LoadingPage from "../component/LoadingPage/LoadingPage";
import {inIframe} from "../constants/Utility";

import {withCommonPropsLite} from "../component/interfacehook/InterfaceHook";
import SnackbarRender from "../component/SnackbarRender/SnackbarRender";


// const FirstAccessDialog = lazy(() => import("../component/FirstAccessDialog/FirstAccessDialog"));
const PlayerDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/PlayerDialogDrawerHandler"));
const EpisodeDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/EpisodeDialogDrawerHandler"));
const SmallDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/SmallDialogDrawerHandler"));
const ChangelogDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/ChangelogDialogDrawerHandler"));
const ShowDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/ShowDialogDrawerHandler"));
const DescriptionDialogDrawerHandler = lazy(() => import("../component/DialogDrawerHandler/DescriptionDialogDrawerHandler"));
const Home = lazy(() => import("../screen/Home/Home"));
const SinglePlayerFTW = lazy(() => import("../component/SinglePlayerFTW/SinglePlayerFTW"));
const TorrentDownloadList = lazy(() => import("../component/TorrentDownloadList/TorrentDownloadList"));

export const BaseRouter = (props) => {
    let history = useHistory();
    let match = useRouteMatch();
    let {store} = props
    let {loading} = store
    let searchValues = (search) => {
        return queryString.parse(search)
    }

    if (!window.TNDhistory) {
        window.TNDhistory = history
    }
    if (!window.TNDmatch) {
        window.TNDmatch = match
    }
    if (!window.TNDlocation) {
        window.TNDlocation = history.location
    }
    if (!window.TNDsearchValues) {
        window.TNDsearchValues = searchValues
    }


    useEffect(() => {
        loading.set(false)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    let output = <>
        <DescriptionDialogDrawerHandler/>
        <ShowDialogDrawerHandler/>
        <SmallDialogDrawerHandler/>
        <EpisodeDialogDrawerHandler/>
        <PlayerDialogDrawerHandler/>
        {loading.get && <LoadingPage/>}
        <Home/></>

    if (inIframe() || searchValues(history.location.search).isEmbedded) {
        if(searchValues(history.location.search)?.fullDownloadList && JSON.parse(searchValues(history.location.search)?.fullDownloadList)){
            output = <TorrentDownloadList
            />
        }else{
            output = <SinglePlayerFTW
            />
        }

    }
    // Per provare la funzione in iframe
    // return <iframe
    //     width={"100%"}
    //     height={"100%"}
    //     src={"http://localhost:3069/?isEmbedded=true&idShow=64196&seasonNumber=4&episodeNumber=2&episodeLink=https://server2.streamingaw.online/DDL/ANIME/Overlord4/Overlord4_Ep_02_SUB_ITA.mp4&magnet=241b532d714407659f9c72dd7a00b33d1752893e"}
    //     // src={"http://localhost:3069/?idShow=64196&seasonNumber=4&episodeNumber=2&episodeLink=https://mega.nz/embed/BMYgWSpY#cNImrQEX0W9uN2yVjp0QTE71PqnhxGo-Svrtn3MKyXQ&magnet=241b532d714407659f9c72dd7a00b33d1752893e"}
    //
    // />

    return (<>
            <Suspense
                fallback={<LoadingPage/>}
            >
                <Switch>
                    <Route path={"/"}>
                    {/*<DialogDeviceMode/>*/}
                        <SnackbarRender/>
                        <ChangelogDialogDrawerHandler/>
                        {/*<FirstAccessDialog/>*/}
                        {output}
                    </Route>
                </Switch>
            </Suspense>
        </>
    )
}

export default withCommonPropsLite(BaseRouter)

