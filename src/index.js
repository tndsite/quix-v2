import {createRoot} from 'react-dom/client';
import './index.css';
import Quix from './Quix';
import {AppProvider} from "./component/MobxContext/AppContext";
import './asset/elucidCircularA/index.css';


const container = document.getElementById('root');
const root = createRoot(container);
global = {
    ...global,
    quix: {

    }
}

root.render(<AppProvider><Quix/></AppProvider>);


