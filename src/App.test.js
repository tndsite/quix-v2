import {render, screen} from '@testing-library/react';
import Quix from './Quix';

test('renders learn react link', () => {
  render(<Quix />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
