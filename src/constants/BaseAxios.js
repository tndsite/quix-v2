import axios from "axios";
import {getCacheLocal, setCacheLocal} from "./DbLocal";
import {BACKEND_PATH} from "./Constants";

const BaseAxios = axios.create({
    baseURL: BACKEND_PATH
});


const API_KEY_TRANSLATIONS_TOOL =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImIzNDdiYTAzLTk4YjItNGFlZi1hZDdkLWMxNjA0YmYxM2QwMiIsInByb2R1Y3RJZCI6IjhhYzRkZmRhLWUwM2EtNGYzMC05MTA2LTViYTJjYjA0ZDEzZiIsInNlcnZpY2VJZCI6NiwicHJvamVjdFNlZWRJZCI6ImIzNDdiYTAzLTk4YjItNGFlZi1hZDdkLWMxNjA0YmYxM2QwMiIsImlhdCI6MTcyMjI0NjQ4N30.cwQnr_lZFwZtvEBFFcmc-DMJfwMjyJbH2K4elvZYuik"
const API_KEY_TMDB =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjQxNzBlYTMxLTk0MGItNGRhZS05MWRjLWYxODZkY2FhMzUzZiIsInByb2R1Y3RJZCI6IjhhYzRkZmRhLWUwM2EtNGYzMC05MTA2LTViYTJjYjA0ZDEzZiIsInNlcnZpY2VJZCI6MywicHJvamVjdFNlZWRJZCI6IjQxNzBlYTMxLTk0MGItNGRhZS05MWRjLWYxODZkY2FhMzUzZiIsImlhdCI6MTcyMjI0NjQyNX0.Mo403gt40NyS3F1ynsEj0CVWkk46YIijJSuZO3NFb3g"
const getApiKeyByPath = (path) => {
    if (path.includes("service/translations-tool/")) {
        return API_KEY_TRANSLATIONS_TOOL
    } else if (path.includes("service/tmdb/")) {
        return API_KEY_TMDB
    }
}

BaseAxios.getTND = async (url, options) => {

    let cache = await getCacheLocal(url)
    let cacheTime = await getCacheLocal(url + "time")
    let differenceInSeconds = ((new Date().getTime()) - cacheTime) / 1000
    if (cache && differenceInSeconds < 86400) {
        let request = {
            url,
            method: "get",
            meta: {},
            headers: {}
        }
        request.meta.requestStartedAt = new Date().getTime();
        request.headers["Access-Control-Allow-Origin"] = "*";
        request.headers["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept";
        request.headers["Access-Control-Allow-Methods"] = "GET,PUT,POST,DELETE,PATCH,OPTIONS";
        if ((request.headers["Authorization"] === undefined || request.headers["Authorization"] === null || request.headers["Authorization"] === "")) {
            request.headers["Authorization"] = getApiKeyByPath(request.url);
        }
        request.meta.logData = {
            startLog: ("REQUEST " + request.method.toUpperCase() + " " + request.url),
            startData: request.data ? {data: request.data, request} : request
        }
        consoleHandler({
            config: {...request},
            data: JSON.parse(cache)
        })
        return {data: JSON.parse(cache)}
    }

    return await BaseAxios.get(url, options)
}


// 2. Log della richiesta nell api
BaseAxios.interceptors.request.use(async request => {


    request.headers["Access-Control-Allow-Origin"] = "*";
    request.headers["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept";
    request.headers["Access-Control-Allow-Methods"] = "GET,PUT,POST,DELETE,PATCH,OPTIONS";
    if ((request.headers["Authorization"] === undefined || request.headers["Authorization"] === null || request.headers["Authorization"] === "")) {
        request.headers["Authorization"] = getApiKeyByPath(request.url);
    }
    request.meta = request.meta || {}
    request.meta.requestStartedAt = new Date().getTime();
    request.meta.logData = {
        startLog: ("REQUEST " + request.method.toUpperCase() + " " + request.url),
        startData: request.data ? {data: request.data, request} : request
    }
    return request;
});

const consoleHandler = (response) => {
    console.groupCollapsed(response.config.meta.logData.startLog + " " + `Time: ${new Date().getTime() - response.config.meta.requestStartedAt} ms`)
    if (new Date().getTime() - response.config.meta.requestStartedAt > 1500) {
        console.error("LONG TIME RESPONSE: " + `Time: ${new Date().getTime() - response.config.meta.requestStartedAt} ms`)
    }
    console.debug("Starting config:", response.config.meta.logData.startData);
    console.debug("Response:", response.data)
    let columns = {}
    for (let i in response.data) {
        if (response.data[i]
        ) {
            if (
                typeof response.data[i] !== 'object'
                &&
                !Array.isArray(response.data[i])
            ) {
                columns[i] = response.data[i]
            } else {
                for (let oIndex in response.data[i]) {
                    if (typeof response.data[i][oIndex] !== 'object'
                        &&
                        !Array.isArray(response.data[i][oIndex])) {
                        if (response.data[i][oIndex]) {
                            if (!columns[i]) {
                                columns[i] = {
                                    [oIndex]: response.data[i][oIndex]
                                }
                            } else {
                                columns[i] = {
                                    ...columns[i],
                                    [oIndex]: response.data[i][oIndex]
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (Object.keys(columns).length > 0) {
        console.table(columns);
    }
    console.groupEnd();
}

// 3. Log della risposta dall api
BaseAxios.interceptors.response.use(
    async response => {
        await setCacheLocal(response.config.url, JSON.stringify(response.data))
        await setCacheLocal(response.config.url + "time", new Date().getTime())
        consoleHandler(response)

        return response;
    },
    error => {
        if (error.response) {
            console.groupCollapsed("%c ERROR Status: " + (error.response && error.response.status) + " Code: " + error.code + " " + (error.response && error.response.config.meta.logData.startLog) + " " + `Time: ${new Date().getTime() - (error.response && error.response.config.meta.requestStartedAt)} ms`, 'border:2px solid red;background: #5c0000; color: #ff8080')
            console.table({...error.response.data, status: error.response.status, code: error.code})
            console.error("ERROR RESPONSE", error.response);
            console.error("ERROR", error,);
            console.groupEnd();
        } else if (error.message !== "FEE001") {
            console.error("ERROR", error)
        }
        return Promise.reject(error);
    }
);


export default BaseAxios

