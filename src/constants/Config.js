export const GOOGLE_ACCESS_TOKEN = "SESSION_GOOGLE_TOKEN";
export const GOOGLE_REFRESH_TOKEN = "GOOGLE_ID_TOKEN";
export const LANGUAGE = (navigator.language && navigator.language.substr(0,2).toLowerCase()) || "en";
export const BASE_PATH="service/tmdb/"
