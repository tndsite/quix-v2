import BaseAxios from "./BaseAxios";
import {APP_LOADING, LOADING, LOCAL_LOADING, LOCAL_LOADING_LITE, NO_LOADING} from "./Constants";
import {FormattedMessage} from "react-intl";
import {BASE_PATH} from "./Config";
import axios from "axios";

export const uuidGenerator = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        // eslint-disable-next-line
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}


export function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'// port and path
        , 'i'); // fragment locator
    return !!pattern.test(str);


}

export const isDeviceMobile = () => {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)
}


export async function copyToClipboard(text) {
    if (isDeviceMobile()) {
        const elem = document.createElement('textarea');
        elem.value = text;
        document.body.appendChild(elem);
        elem.select();
        document.execCommand('copy');
        document.body.removeChild(elem);
    } else {
        await navigator.clipboard.writeText(text);
    }


}

export const getBase64FromUrl = async (src, fileName) => {
    try {
        let o = await axios.get(
            "https://image.tmdb.org/t/p/w500" + src,
            {
                responseType: "arraybuffer",
            }
        )
        return Buffer.from(o.data).toString("base64");
    } catch (e) {
        const response = await BaseAxios.getTND(BASE_PATH + "get-image?imagePath=" + src);
        return response.data

    }
};

export function blobToBase64(blob) {
    return new Promise((resolve, _) => {
        const reader = new FileReader();
        reader.onloadend = () => resolve(reader.result);
        reader.readAsDataURL(blob);
    });
}


export function inIframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

export const getTypeVideo = (link) => {
    if (!link) {
        return
    }
    let tmpExt = link.split(".")
    let extension = tmpExt[tmpExt.length - 1]
    switch (extension) {
        default:
        case "mp4":
            return "video/webm";
        case "m3u8":
            return "application/x-mpegURL";
        case "mkv":
        case "webm":
            return "video/webm;codecs=vp8,opus";
    }
}

export const commonHandledRequest = async (props) => {
    let {
        translationKeyError, action, loadingType, translationKeySuccess, delayLoadingEnd = 0, snackbarType,
        cbOnError
    } = props
    let store = window.TNDglobalStoreMobx
    let {snackbarHandler, localLoading, localLoadingLite, appLoading, loading: defLoading} = store
    let loading
    switch (loadingType) {
        case NO_LOADING:
            loading = {
                set: () => {
                }
            }
            break
        case LOADING:
            loading = defLoading
            break
        case LOCAL_LOADING:
            loading = localLoading
            break
        case APP_LOADING:
            loading = appLoading
            break
        case LOCAL_LOADING_LITE:
        default:
            loading = localLoadingLite
            break
    }
    try {
        loading.set(true)
        await action()
        setTimeout(() => {
            loading.set(false)
            if (translationKeySuccess) {
                let txt = <FormattedMessage id={translationKeySuccess + ".snackbar.success"}/>
                switch (snackbarType) {
                    case "success":
                    default:
                        snackbarHandler.success(txt)
                        break
                    case "info":
                        snackbarHandler.info(txt)
                        break

                }
            }
        }, delayLoadingEnd)

    } catch (error) {
        loading.set(false)
        console.error("Error commonHandledRequest", error, error.response)
        if (cbOnError) {
            cbOnError?.(error)
        } else {
            commonErrorHandler({error, errorText: <FormattedMessage id={translationKeyError + ".snackbar.error"}/>})
        }
    }
}


export const commonErrorHandler = (props) => {
    let {errorText} = props;
    let store = window.TNDglobalStoreMobx
    // let intl = window.TNDintl
    let {snackbarHandler} = store


    // if (tmpEr) {
    //     errorText = intl.formatMessage({id: "error.translation.code." + tmpEr})
    // }

    snackbarHandler.warning(errorText);

}
