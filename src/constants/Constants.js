import {BASE_PATH} from "./Config";

export const SEARCHED_VALUE="searchedValue"
export const LOADING="loading"
export const APP_LOADING="loading"
export const NO_LOADING="no_loading"
export const LOCAL_LOADING="local_loading"
export const LOCAL_LOADING_LITE="local_loading_lite"
export const IS_STAGING = process.env.REACT_APP_IS_STAGING === "true";
export const IS_LOCALHOST = process.env.REACT_APP_IS_LOCALHOST === "true";
// export const BACKEND_PATH = IS_LOCALHOST ? "http://localhost:3000/" : IS_STAGING ? "https://service.tnl.one/" : "https://production-api.tnl.one/";
export const BACKEND_PATH = IS_LOCALHOST ? "http://localhost:3000/" : "https://production-api.tnl.one/";