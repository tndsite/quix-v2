import Dexie from "dexie";
import "dexie-observable"
import {getConfLocal} from "./DbLocal";
import {GOOGLE_ACCESS_TOKEN} from "./Config";
import {importDB, importInto, peakImportFile} from "dexie-export-import";
import {databasePush} from "./GoogleApi";

export const momentFormat = "YYYY/DD/MM-HH:mm:ss"

export const DB_NAME = "Definitive2.4"


export let db = new Dexie(DB_NAME)


export const importDb = async (blob, options) => {

    await importInto(db, blob, {
        overwriteValues: true,
        // clearTablesBeforeImport: true,// NON FUNZIONA NON T'AZZARDARE A METTERLO MANDA TUTTO IN OUT OF SYNC
        progressCallback: (asd) => {
            console.log("Importing db", asd)
        }
    });


        // db.on('changes', async (changes) => {
        //     if (await getConfLocal(GOOGLE_ACCESS_TOKEN)) {
        //         console.log("Pushing db changes", changes)
        //         databasePush()
        //             .then(() => {
        //                 console.log("Pushato")
        //             })
        //             .catch(console.error);
        //     }
        //
        // });


    // const importMeta = await peakImportFile(blob);
    //
    // console.log("Database importMeta.formatName:", importMeta.formatName);
    // console.log("Database importMeta.formatVersion:", importMeta.formatVersion);
    // console.log("Database name:", importMeta.data.databaseName);
    // console.log("Database version:", importMeta.data.databaseVersion);



}

const upgradeDb = () => {

    db.version(1).stores({
        show: '[idShow],idShow, showName,inputDate,followed',
        season: '[idShow+seasonNumber],idShow,seasonNumber,seasonName,inputDate',
        episode: '[idShow+seasonNumber+episodeNumber],seasonNumber,idShow,episodeNumber,episodeName,seen, seenDate',
        link: '[idShow+seasonNumber+episodeNumber+link],idShow,seasonNumber,episodeNumber,link,inputDate,[idShow+seasonNumber+episodeNumber]'
    });
    db.version(2).stores({});
    db.version(3).stores({});
    db.version(4).stores({});
    db.version(5).stores({
        rawEpisode: '[idShow+link],idShow,link,inputDate,episodeName,seen,seenDate',
        uncategorizedData: '[fileName+link],fileName,link,mimeType',
    });
    db.version(6).stores({
        mediaServer: '[host]'
    });
    db.version(7).stores({
        show: '[idShow],idShow, showName,inputDate,followed,tagFilter',
    });
    db.version(8).stores({
        mediaServer: '++id,host'
    });
    db.version(9).stores({
        configuration: '[key],key, value'
    });
    db.version(10).stores({
        movie: '[idMovie],idMovie,name,links,inputDate,seen,seenDate,followed,toWatch'
    });
    db.version(10).stores({
        link: '[idShow+seasonNumber+episodeNumber+link],idShow,seasonNumber,episodeNumber,link,isPlayable,inputDate,[idShow+seasonNumber+episodeNumber]'
    });

    db.on('changes', async (changes) => {
        if (await getConfLocal(GOOGLE_ACCESS_TOKEN)) {
            console.log("Pushing db changes", changes)
            databasePush()
                .then(() => {
                    console.log("Pushato")
                })
                .catch(console.error);
        }

    });
}
upgradeDb()

// db.show.count().catch(async (e) => {
//     await Dexie.delete(DB_NAME);
//     window.location.reload();
// })


export async function recreateDB() {

    await db.close()
    await db.delete()
    await db.open()

}
