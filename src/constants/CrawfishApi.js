import ImageTest from "../asset/image/logo-quix-defaultNoImg.png"
import {BASE_PATH} from "./Config";
import BaseAxios from "../constants/BaseAxios"
import {getCacheLocal, setCacheLocal} from "./DbLocal";
import {getBase64FromUrl} from "./Utility";

export const searchShow = async (q, page = 1) => {
    try {
        let url
        if (q) {
            url = BASE_PATH + "3/search/tv?page=" + page + "&query=" + q
        } else {
            url = BASE_PATH + "3/trending/tv/week"
        }
        let res = await BaseAxios.getTND(url)

        let output = []
        if (res.data && res.data.results) {
            output = res.data.results
        }

        // while (res.data.total_pages > page) {
        while (3 > page) {
            page += 1
            url = BASE_PATH + "3/search/tv?page=" + page + "&query=" + q
            res = await BaseAxios.getTND(url)

            // console.info("SHOW")
            if (res.data && res.data.results) {
                output = [...output, ...res.data.results]
            }

        }
        return {results: output, totalPages: res.data.total_pages}
    } catch (e) {
        console.error("Error searching the show", e, e.response)
        return []
    }

}
export const searchDiscoverShow = async (provider = 8, country = "US", page = 1) => {
    try {
        let url = BASE_PATH +
            "3/discover/tv?language=en&page=" + page + "&with_watch_providers=" + provider + "&sort_by=release_date.desc&watch_region=" + country
        let res = await BaseAxios.getTND(url)
        let output = []
        if (res.data && res.data.results) {
            output = [...output, ...res.data.results]
        }

        return {results: output, totalPages: res.data.total_pages}
    } catch (e) {
        console.error("Error searching the discover", e, e.response)
        return []
    }

}
export const searchProvidersShow = async (lang = "US",) => {
    try {
        let url = BASE_PATH + "3/watch/providers/tv?language=en&watch_region=" + lang
        let res = await BaseAxios.getTND(url)
        let output = []
        if (res.data && res.data.results) {
            output = res.data.results
        }
        return output
    } catch (e) {
        console.error("Error searching the provider", e, e.response)
        return []
    }
}

export const searchRegionProviders = async () => {
    try {
        let url = BASE_PATH + "3/watch/providers/regions"
        let res = await BaseAxios.getTND(url)

        let output = []
        if (res.data && res.data.results) {
            output = res.data.results
        }
        return output
    } catch (e) {
        console.error("Error searching the country", e, e.response)
        return []
    }
}


export const getImage = async (imagePath) => {
    // return "https://image.tmdb.org/t/p/w500" + imagePath

    try {
        let store = window.TNDglobalStoreMobx
        let {images} = store
        // console.info("immagine1", imagePath)
        if (imagePath) {
            if (images[imagePath]) {
                return images[imagePath]
            }
            let savedImage = await getCacheLocal(imagePath + "image")
            if (savedImage) {
                images.set(imagePath, savedImage)
                return savedImage
            }


            let base64 = await getBase64FromUrl(imagePath)
            await setCacheLocal(imagePath + "image", "data:image/png;base64," + base64)
            images.set(imagePath, "data:image/png;base64," + base64)
            return "data:image/png;base64," + base64
        } else {
            return ImageTest
        }
    } catch (e) {
        return "https://image.tmdb.org/t/p/w500" + imagePath
    }


}

export const getSerieTVDetail = async (id) => {
    try {
        let url = BASE_PATH + "3/tv/" + id
        return await BaseAxios.getTND(url)

    } catch (e) {
        if (e && e.response && e.response.status === 404) {
            return
        } else {
            console.error("Error searching the getSerieTV", e,)
            return false
        }

    }

}

export const getEpisodes = async (seasonNumber, id) => {
    try {

        let url = BASE_PATH + "3/tv/" + id + "/season/" + (seasonNumber)
        return await BaseAxios.getTND(url)

    } catch (e) {
        console.error("Error searching the getEpisodes", e,)
        return false
    }
};