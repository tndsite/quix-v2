import moment from "moment";
import {db, momentFormat} from "./Db";
import axios from "axios";
import {uuidGenerator} from "./Utility";

const externalLink = "https://api.paste.ee/v1/pastes/"

export const setConf = async (key, value) => {
    await db.configuration.put({
        key,
        value,
    });

};

export const getConf = async (key) => {
    let temp = await db.configuration.where("key").equals(key).first();
    if (temp) {
        return temp.value;
    } else {
        return null;
    }
};

export const dataExist = async () => {
    return await db.show.count() > 0;
}


export const saveUrlEpisode = async (input) => {
    await importLinkMassive([input]);
};

export const importLinkMassive = async (data) => {

    for (let input of data) {
        let {tvShowId, episodeNumber, seasonNumber, showName, episodeName, link, isPlayable} = input;
        //By default if null isPlayable is true
        if (isPlayable === null || isPlayable === undefined) {
            isPlayable = true
        }
        if (!tvShowId) {
            tvShowId = input.id || input.idShow;
        }

        await db.show.put({
            idShow: parseInt(tvShowId),
            showName,
            followed: true,
            inputDate: moment().format(momentFormat),
        });
        await db.season.put({
            idShow: parseInt(tvShowId),
            seasonNumber,
            seasonName: showName,
        });
        await db.episode.put({
            idShow: parseInt(tvShowId),
            seasonNumber,
            episodeNumber,
            episodeName,
        });
        await db.link.put({
            idShow: parseInt(tvShowId),
            seasonNumber,
            episodeNumber,
            link,
            isPlayable
        });
    }
};


export const shareableLinkDataSingleSeason = async (idShow, seasonNumber, episodeNumbers, seasonToRedirect) => {
    let temp = []
    let contents = {
        show: {},
        season: [],
        link: [],

    }
    for (let e of episodeNumbers) {
        temp.push(...await getEpisodeLink(idShow, seasonNumber, e))
    }
    contents.link = temp
    let showSeasonInfo = await getShowSeasonInfo(idShow, seasonNumber)
    contents.show = showSeasonInfo.show
    contents.season = [showSeasonInfo.season]

    if (seasonToRedirect !== false) {
        contents.redirectToSeason = seasonToRedirect
    }
    return await getSharableLink(contents)
};

export const shareableLinkDataShow = async (idShow, seasons, episodeNumbers, seasonToRedirect) => {
    let temp = []
    let contents = {
        show: {},
        season: [],
        link: [],

    }
    for (let s of seasons) {
        for (let e of episodeNumbers) {
            temp.push(...await getEpisodeLink(idShow, s, e))
        }
    }
    contents.link = temp
    let showSeasonInfo = await getShowSeasonInfo(idShow, false)
    contents.show = showSeasonInfo.show
    contents.season = showSeasonInfo.season
    if (seasonToRedirect !== false) {
        contents.redirectToSeason = seasonToRedirect
    }
    return await getSharableLink(contents)
};

const getSharableLink = async (contents) => {
    try {
        let response = await axios.request({
            url: externalLink,
            headers: {
                "X-Auth-Token": "aE4Luc4LSG6JMGsiC5ewotJEAlMBkPQllICpCa08J"
            },
            method: "POST",
            data: {
                sections: [
                    {
                        "name": uuidGenerator(),
                        "contents": JSON.stringify(contents)
                    }
                ]
            }
        })
        return window.location.origin + "/?sh=" + response.data.id
    } catch (e) {
        console.error("Errore durante la generazione dello snippet", e, e.response)
    }
}


export const readSharedData = async (link) => {
    try {
        let temp = await axios.get(externalLink + link, {
            headers: {
                "X-Auth-Token": "aE4Luc4LSG6JMGsiC5ewotJEAlMBkPQllICpCa08J"
            },
        });
        let data = JSON.parse(temp.data.paste.sections[0].contents);
        let output = {idShow: data.show.idShow,};
        if (data.redirectToSeason) {
            output.redirectToSeason = data.redirectToSeason
        }
        for (let d of data.link) {
            let {idShow, episodeNumber, seasonNumber, link, isPlayable} = d;
            if (isPlayable === null || isPlayable === undefined) {
                isPlayable = true
            }
            await db.link.put({
                idShow: parseInt(idShow),
                seasonNumber,
                episodeNumber,
                link,
                isPlayable
            });
        }
        for (let d of data.season) {
            let {idShow, seasonNumber, seasonName} = d;
            await db.season.put({
                idShow: parseInt(idShow),
                seasonNumber: parseInt(seasonNumber),
                seasonName,
            });
        }
        await db.show.put({
            idShow: parseInt(data.show.idShow),
            showName: data.show.showName,
            followed: true,
            inputDate: moment().format(momentFormat),
        });
        return output;
    } catch (e) {
        console.error("Error reading shared data", e);
        throw e;
    }

}


// Metodi per le serie
export const showIsToWatch = async (tvShowId,) => {
    if (!tvShowId) {
        return false
    }
    let temp = await db.show.where({idShow: tvShowId}).first();
    return temp && temp.tagFilter === "W";
};
export const saveFollowedToShowShow = async (input, value, key) => {
    try {
        let {id, name, original_name} = input;
        let followed = value, tagFilter = value
        if (key === "follow") {
            tagFilter = "T"
            if (await showIsToWatch(id)) {
                tagFilter = "W"
            }
        } else if (key === "toWatch") {
            followed = await showIsFollowed(id)
        }
        console.info("ASD")
        await db.show.put({
            idShow: parseInt(id),
            showName: name || original_name,
            followed,
            tagFilter,
            inputDate: moment().format(momentFormat),
        });
        console.info("ASD1")
    } catch (e) {
        console.error("Error saveFollowedToShowShow: ", e)
    }

};
export const updateShowName = async (idShow, newName) => {
    try {
        // console.info("updateShowName",idShow,newName)
        let show = await db.show.where({idShow}).first();
        if (show && show.showName !== newName) {
            await db.show.put({...show, showName: newName,});
        }
    } catch (e) {
        console.error("Error updateShowName: ", e)
    }
}
export const getLastSeenSeries = async () => {

    let episodes = await db.episode.orderBy('idShow').filter(x => !!x.seenDate && moment(x.seenDate, "YYYY/DD/MM-HH:mm:ss").isSameOrAfter(moment().subtract(2, "weeks"))).reverse().sortBy("seenDate")
    let filteredEpisodes = []

    episodes.forEach(x => {
        if (!filteredEpisodes.some(xx => xx.idShow === x.idShow)) {
            filteredEpisodes.push(x)
        }
    })

    return filteredEpisodes


};


export const getToWatchSeries = async (text = "", page) => {
    if (text) {
        return await db.show.where("showName").startsWith(text.toLowerCase()).and(x => x.tagFilter === "W").reverse().sortBy("inputDate")
    } else {
        return await db.show.where("tagFilter").equals("W").reverse().sortBy("inputDate")
    }
};
export const getFollowedSeries = async (text = "",) => {
    if (text) {
        return await db.show.where("showName").startsWith(text.toLowerCase()).and(x => x.followed).reverse().sortBy("inputDate")
    } else {
        return await db.show.filter(e => e.followed === true).reverse().sortBy("inputDate")
    }
};

export const getSeries = async (text = "",) => {
    if (text) {
        return await db.show.where("showName").startsWith(text.toLowerCase()).and(x => x.followed).reverse().sortBy("inputDate")
    } else {
        return await db.show.reverse().sortBy("inputDate")
    }

};


export const deleteSerie = async (idShow) => {
    await db.show.where("idShow").equals(idShow).delete()
}

export const deleteSeriesAndLink = async (idShow, seasonNumber, deleteShow) => {
    if (deleteShow) {
        await db.show.where("idShow").equals(idShow).delete()
    }
    await db.season.where({idShow: parseInt(idShow), seasonNumber}).delete();
    await db.episode.where({idShow: parseInt(idShow), seasonNumber}).delete();
    await db.link.where({idShow: parseInt(idShow), seasonNumber}).delete();
};

export const deleteLink = async (tvShowId, link, seasonNumber, episodeNumber) => {

    return await db.link.where({idShow: parseInt(tvShowId), seasonNumber, link, episodeNumber}).delete();
};


export const getTimeLink = async (input) => {
    let {tvShowId, episodeNumber, seasonNumber, link} = input;
    if (!tvShowId) {
        tvShowId = input.id || input.idShow;
    }

    let temp = await db.link.where({idShow: tvShowId, seasonNumber, link, episodeNumber}).toArray();
    if (temp && temp[0] && temp[0].time) {
        return temp[0].time;
    } else {
        return null;
    }
};


export const saveTimeLink = async (input) => {
    let {tvShowId, episodeNumber, seasonNumber, link, time, isPlayable} = input;
    if (isPlayable === null || isPlayable === undefined) {
        isPlayable = true
    }
    if (!tvShowId) {
        tvShowId = input.id || input.idShow;
    }

    await db.link.put({
        idShow: tvShowId,
        seasonNumber,
        episodeNumber,
        link,
        isPlayable,
        time
    });
};

export const getShowSeasonInfo = async (idShow, seasonNumber) => {

    if (seasonNumber !== false) {
        return {
            show: await db.show.where({idShow: parseInt(idShow)}).first(),
            season: await db.season.where({idShow: parseInt(idShow), seasonNumber}).first(),
        }
    } else {
        return {
            show: await db.show.where({idShow: parseInt(idShow)}).first(),
            season: await db.season.where({idShow: parseInt(idShow)}).toArray(),
        }
    }
};

export const episodeSeen = async (input, seen) => {
    let {tvShowId, episodeNumber, seasonNumber, episodeName} = input;
    if (!tvShowId) {
        tvShowId = input.id;
    }
    let seenDate = moment().format(momentFormat)
    if (!seen) {
        seenDate = null
    }
    await db.episode.put({
        idShow: tvShowId,
        episodeNumber,
        seasonNumber,
        seen,
        episodeName,
        seenDate
    });
};

export const getEpisodeLink = async (tvShowId, seasonNumber, episodeNumber) => {
    return await db.link.where({idShow: parseInt(tvShowId), seasonNumber, episodeNumber}).toArray();
};
export const haveSeen = async (tvShowId, seasonNumber, episodeNumber) => {

    let temp = await db.episode.where({idShow: tvShowId, seasonNumber, episodeNumber}).first();
    return temp && temp.seen;
};

export const showIsFollowed = async (tvShowId,) => {
    if (!tvShowId) {
        return false
    }
    let temp = await db.show.where({idShow: tvShowId}).first();
    return temp && temp.followed;
};


// Metodi per i film
export const movieIsFollowed = async (id) => {
    if (!id) {
        return false
    }
    let temp = await db.movie.where({idMovie: id}).first();
    return temp && temp.followed;
};

export const movieIsToWatch = async (id,) => {
    if (!id) {
        return false
    }
    let temp = await db.movie.where({idMovie: id}).first();
    return temp && temp.tagFilter === "W";
};


export const saveMovieFollow = async (input, value, key) => {
    let {id, title, original_title} = input;
    let followed = value;
    let tagFilter = value;
    if (key === "follow") {
        tagFilter = "T"
        if (await movieIsToWatch(id)) {
            tagFilter = "W"
        }
    } else if (key === "toWatch") {
        followed = await movieIsFollowed(id)
    }
    await db.movie.put({
        idMovie: parseInt(id),
        showName: title || original_title,
        followed,
        tagFilter,
        inputDate: moment().format(momentFormat)
    });
};
// Metodi per i raw
export const haveSeenRawEpisode = async (tvShowId, link) => {
    let temp = await db.rawEpisode.where({idShow: tvShowId, link}).first();
    return temp && temp.seen;
};

export const getRawEpisode = async (idShow) => {
    try {
        return (await db.rawEpisode.where("idShow").equals(parseInt(idShow)).toArray()).sort((a, b) => {
            if (parseInt(a.episodeName) < parseInt(b.episodeName)) return -1;  // any negative number works
            if (parseInt(a.episodeName) > parseInt(b.episodeName)) return 1;   // any positive number works
            return 0; // equal values MUST yield zero
        });
    } catch (e) {
        console.error(e)
    }
};
export const getTimeLinkRaw = async (input) => {
    let {idShow, link} = input;

    let temp = await db.rawEpisode.where({idShow: parseInt(idShow), link}).toArray();
    if (temp && temp[0] && temp[0].time) {
        return temp[0].time;
    } else {
        return null;
    }
};

export const saveTimeLinkRaw = async (input) => {
    let {idShow, episodeName, link, time} = input;
    await db.rawEpisode.put({
        idShow: parseInt(idShow),
        episodeName,
        link,
        time
    });
};

export const rawEpisodeSeen = async (input, seen) => {

    let {idShow, link, episodeName} = input;
    await db.rawEpisode.put({
        idShow,
        link,
        episodeName,
        seen,
        seenDate: moment().format(momentFormat)
    });
};
