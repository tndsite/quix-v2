import Dexie from "dexie";
import "dexie-observable"

export const momentFormat = "YYYY/DD/MM-HH:mm:ss"

export const DB_NAME = "Local2.4"


export let db = new Dexie(DB_NAME);

db.version(1).stores({
    configuration: '[key],key, value'
});
db.version(2).stores({
    cache: '[key],key, value'
});


export const setConfLocal = async (key, value) => {
    await db.configuration.put({
        key,
        value
    });
};

export const getConfLocal = async (key) => {

    let temp = await db.configuration.where("key").equals(key).first();
    if (temp) {
        return temp.value;
    } else {
        return null;
    }
};

export const removeConfLocal = async (key) => {
    if(await getConfLocal(key)) {
        await db.configuration.put({
            key,
            value:null
        });
    }
};


export const setCacheLocal = async (key, value) => {
    await db.cache.put({
        key,
        value
    });
};

export const getCacheLocal = async (key) => {

    let temp = await db.cache.where("key").equals(key).first();
    if (temp) {
        return temp.value;
    } else {
        return null;
    }
};