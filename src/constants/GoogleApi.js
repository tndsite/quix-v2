import {GOOGLE_ACCESS_TOKEN, GOOGLE_REFRESH_TOKEN} from "./Config";
import axios from "axios";
import {exportDB} from "dexie-export-import";
import {db, DB_NAME, importDb} from "./Db"
import {getConfLocal, setConfLocal} from "./DbLocal";

export const getAccessToken = async (serverAuthCode) => {
    try {
        let body = new URLSearchParams({
            client_id: "12998500978-9rk2hki7jntah53m20m9nag4bgk1kr1o.apps.googleusercontent.com",
            client_secret: "WRNKVn9khBv7LGHm7WscXvKu",
            code: serverAuthCode,
            grant_type: 'authorization_code',
            redirect_uri: window.location.origin
        });
        let authResponse = await axios.post('https://oauth2.googleapis.com/token', body);
        console.log("AUTH RESPONSE: ", authResponse);
        await setConfLocal(GOOGLE_REFRESH_TOKEN, authResponse.data.refresh_token)
        return authResponse.data.access_token;
    } catch (e) {
        console.error("ACCESS TOKEN RESPONSE ERROR: ", e);
    }

}
export const getRefreshToken = async (refreshToken) => {
    try {
        let authResponse = await axios.post('https://www.googleapis.com/oauth2/v4/token',
            {
                client_id: "12998500978-9rk2hki7jntah53m20m9nag4bgk1kr1o.apps.googleusercontent.com",
                client_secret: "WRNKVn9khBv7LGHm7WscXvKu",
                refresh_token: refreshToken,
                grant_type: 'refresh_token',
                redirect_uri: window.location.origin + "/"
            });
        console.log("REFRESH TOKEN RESPONSE: ", authResponse);
        return authResponse.data.access_token;
    } catch (e) {
        console.error("REFRESH TOKEN RESPONSE ERROR: ", e);
    }

}


export const databasePush = async (blob) => {
    let token = await getConfLocal(GOOGLE_ACCESS_TOKEN);
    if (!token) {
        return;
    }
    try {
        if (!blob) {
            blob = await exportDB(db, {noTransaction: true});
        }
        let list = await axios.get("https://www.googleapis.com/drive/v3/files?q=name+%3d+%27" + DB_NAME + ".json%27&orderBy=modifiedTime desc&spaces=appDataFolder", {
            headers: {
                Authorization: "Bearer " + token
            }
        });
        if (list != null && list.data.files && list.data.files[0]) {
            let updateResponse = await axios.patch("https://www.googleapis.com/upload/drive/v3/files/" + list.data.files[0].id + "?uploadType=resumable", {
                "name": DB_NAME + ".json",
                "properties": {key: "uniqueId", value: DB_NAME + ".json"}
            }, {
                headers: {
                    Authorization: "Bearer " + token
                }
            });
            await axios.put(updateResponse.headers.location, blob, {
                headers: {
                    Authorization: "Bearer " + token, "Content-Type": "multipart/form-data"
                }
            })
        } else {
            let createResponse = await axios.post("https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable", {
                "name": DB_NAME + ".json",
                "properties": {key: "uniqueId", value: DB_NAME + ".json", "parents": ['appDataFolder']},
                "parents": ['appDataFolder'],
            }, {
                headers: {
                    Authorization: "Bearer " + token
                }
            });
            await axios.put(createResponse.headers.location, blob, {
                headers: {
                    Authorization: "Bearer " + token, "Content-Type": "multipart/form-data"
                }
            })
        }
        console.log("PUSHING: ", blob.size)
        return {toSync: false, size: blob.size}
    } catch (e) {
        console.error("Error uploading data: ", e.response)
    }
}


export const databasePull = async (override) => {
    try {
        let token = await getConfLocal(GOOGLE_ACCESS_TOKEN)
        console.log("Ma sta list 0", token)
        let list = await axios.get("https://www.googleapis.com/drive/v3/files?q=name+%3d+%27" + DB_NAME + ".json%27&orderBy=modifiedTime desc&spaces=appDataFolder", {
            headers: {
                Authorization: "Bearer " + token
            }
        });
        console.log("Ma sta list", list)
        if (list != null && list.data.files && list.data.files[0]) {
            console.log("ID FILE SYNC Db pull: ", list.data.files[0])
            let getResponse = await axios.get("https://www.googleapis.com/drive/v3/files/" + list.data.files[0].id + "?alt=media", {
                headers: {
                    Authorization: "Bearer " + token
                }
            });
            let content = getResponse.data;
            let blob = new Blob([
                JSON.stringify(content)
            ], {
                type: 'text/json'
            });
            console.log("REMOTE SIZE: ", blob.size, content)

            await importDb(blob);
            // window.location.reload()

            console.log("Import completed: ", blob.size)
        } else {
            // await databasePush(token)
        }
    } catch (e) {

        if (e.message.includes("Database version differs")) {
            console.log("FAILED IMPORT deleting: ", e.message)
            // await recreateDB()
            // await databasePull()
        } else {
            console.log("FAILED IMPORT: ", e.message)
            // window.location.reload()
            throw e;
        }
    }

};
