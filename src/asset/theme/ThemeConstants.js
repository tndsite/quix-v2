export const themeConstants = (defaultTheme) => ({
    typography: {
        fontSize: 16,
        // [defaultTheme.breakpoints.up('xl')]: {
        //     fontSize:24,
        // },
        h3: {
            fontSize: '5em',
        },
        h4: {
            fontSize: '4em',
        }, h5: {
            fontSize: '2em',
        }, h6: {
            fontSize: '1.5em',
        },
        title: {
            color: "var(--title-color)",
            fontSize: "26px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "-0.5px"
        },
        cardTitle: {
            color: "var(--title-color)",
            fontSize: "16px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "1px"
        },
        semiTitle: {
            color: "var(--text-color)",
            fontSize: "20px",
            fontWeight: 400,
            lineHeight: "133%",
            letterSpacing: "-0.5px"
        }, subTitle: {
            color: "var(--text-color)",
            fontSize: "17px",
            fontWeight: 400,
            lineHeight: "133%",
            letterSpacing: "-0.5px"
        }, smallText: {
            color: "var(--text-color)",
            fontSize: "15px",
            fontWeight: 400,
            lineHeight: "150%",
            letterSpacing: "-0.5px"
        },
        miniText:
            {
                color: "var(--text-color)",
                fontSize: "13px",
                fontWeight: 400,
                lineHeight: "150%",
                letterSpacing: "-0.5px"
            }
        ,
        microText: {
            color: "#9f9f9f",
            fontSize: "12px",
            fontWeight: 400,
            lineHeight: "150%",
            letterSpacing: "-0.5px"
        },
        noData: {
            color: "var(--text-color)",
            fontSize: "30px",
            fontWeight: 400,
            lineHeight: "120%",
            letterSpacing: "-0.5px"
        }
    },
    palette: {
        mode: 'dark',
        primary: {
            light: '#757ce8',
            main: '#3f7ec0',
            dark: '#3f7ec0',
            contrastText: '#fff',
            goldenStar: "#e5b92c"
        },
        secondary: {
            main: "#c12d36"
        }
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    textTransform: "camelcase"
                }
            }
        },
        MuiLinearProgress: {
            styleOverrides: {
                root: {
                    borderRadius: "10px"
                }
            }
        },
        MuiListItemButton: {
            styleOverrides: {
                root: {
                    borderRadius: "20px",
                    paddingLeft: "10px",
                    paddingRight: "10px",
                }
            }
        },
        MuiTableContainer: {
            styleOverrides: {
                root: {
                    height: "100%",
                },
            },
        },
        MuiContainer: {
            styleOverrides: {
                root: {
                    paddingLeft: "0px",
                    paddingRight: "0px",
                    height: "100%",
                    [defaultTheme.breakpoints.up('xs')]: {
                        paddingLeft: "0px",
                        paddingRight: "0px",
                    }
                },
            },
        }
    },
})
