import {withCommonPropsLite} from "../../component/interfacehook/InterfaceHook";

const DarkThemeIcon = ({store,isMobile}) => {
    let {theme} = store
    let fill = !isMobile ? "white" : "#99A1EC"
    let stroke = theme.style === "dark" ? "#99A1EC" : "white"
    return <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M11.9977 1.74368C12.1398 2.19303 11.9489 2.68061 11.5395 2.91399C9.4232 4.1202 8 6.3947 8 9.00037C8 12.8664 11.134 16.0004 15 16.0004C17.6055 16.0004 19.8798 14.5774 21.0861 12.4614C21.3195 12.052 21.8071 11.8611 22.2564 12.0032C22.7058 12.1454 22.9949 12.5819 22.9504 13.0511C22.4206 18.6338 17.7208 23.0001 12 23.0001C5.92487 23.0001 1 18.0753 1 12.0001C1 6.27902 5.36684 1.57903 10.9499 1.04967C11.4191 1.00518 11.8556 1.29432 11.9977 1.74368ZM7.3151 4.31392C4.72743 5.89472 3 8.74598 3 12.0001C3 16.9707 7.02944 21.0001 12 21.0001C15.2539 21.0001 18.105 19.273 19.6858 16.6856C18.3212 17.5195 16.7169 18.0004 15 18.0004C10.0294 18.0004 6 13.9709 6 9.00037C6 7.28316 6.48105 5.67865 7.3151 4.31392Z" fill={fill}/>
    </svg>









}

export default withCommonPropsLite(DarkThemeIcon)