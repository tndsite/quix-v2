import Peer from "peerjs";
import {useEffect, useState} from "react";
import {Button, Stack} from "@mui/material";

export const peerOptions = {
    config: {
        iceServers: [
            {
                urls: "stun:23.21.150.121"
            },
            {
                urls: [
                    'stun:stun.l.google.com:19302',
                    'stun:global.stun.twilio.com:3478'
                ]
            },
            {
                "username": "admin",
                "credential": "Password1!",
                "urls": "turn:185.149.22.163:3478"
            },
            {
                "username": "admin",
                "credential": "Password1!",
                "urls": "turn:23.94.202.235:3478"
            }
        ]
    }
}


let PeerTestReceive = (props) => {


    let [peer, setPeer] = useState(new Peer("asdg45ds1g5541g6s5d145g6s54d51g6s5dg141",peerOptions))

    useEffect(() => {
        let PeerReceive = (props) => {
                console.log("sto per aprire conn per ricevere");
            peer.on('connection', function (conn) {
                console.log("conn aperta a ricevere", conn);
                conn.on('data', function (data) {
                    console.log("ho ricevuto i dati", data);
                    // Will print 'hi!'
                });
            });
        }


        PeerReceive()


    }, [])

    return "asd"

}
let PeerTestConnect = (props) => {

    let [peer, setPeer] = useState(new Peer("asdg45ds1g5541g6s5d145g6s54d51g6s5dg14",peerOptions))


        let PeerConnect = (props) => {
            console.log("mi sto per connettere");
            var conn = peer.connect("asdg45ds1g5541g6s5d145g6s54d51g6s5dg141",{reliable:true});
            console.log("sono connesso", conn);
// on open will be launch when you successfully connect to PeerServer
            conn.on('open', function () {
                console.log("sono aperto", conn);
                // here you have conn.id
                conn.send('hi!');
            });
        }




    return <Button onClick={PeerConnect}>Connect</Button>

}

let PeerTest = (props) => {
    return <Stack sx={{marginTop:"150px"}}>
        <PeerTestReceive/>
        <PeerTestConnect/>
    </Stack>
}
export default PeerTest