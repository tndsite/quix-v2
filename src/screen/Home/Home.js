import Header from "../../component/Header/Header";
import {
    AccordionDetails,
    AccordionSummary,
    Button,
    Container,
    Divider,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    Skeleton,
    Stack,
    Typography,
    useMediaQuery,
} from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import TvShowBig from "../../asset/image/Home/tv-show-big.jpg"
import TvAssembled from "../../asset/image/Home/tv-assembled.png"
import Human from "../../asset/image/Home/human.png"
import SearchBar from "../../component/SearchBar/SearchBar";
import CardItem from "../../component/CardItem/CardItem";
import React, {useEffect, useRef, useState} from "react";
import {
    getSerieTVDetail,
    searchDiscoverShow,
    searchProvidersShow,
    searchRegionProviders,
    searchShow
} from "../../constants/CrawfishApi";
import {
    getConf,
    getFollowedSeries,
    getLastSeenSeries,
    getSeries,
    getToWatchSeries,
    readSharedData,
    setConf
} from "../../constants/StorageHelper";
import {observer} from "mobx-react";
import CardItemExpanded from "../../component/CardItem/CardItemExpanded";
import {ExpandMore} from "@mui/icons-material";
import Accordion from '@mui/material/Accordion';
import {uuidGenerator} from "../../constants/Utility";
import {withCommonPropsLite} from "../../component/interfacehook/InterfaceHook";
import ShowDetail from "../ShowDetail/ShowDetail";
import useLoadingSystem from "../../component/LoadingPage/useLoadingSystem";
import PrivacyPolicy from "../../component/PrivacyPolicy";


const Home = (props) => {
    let {
        searchValues,
        store,
        history,
        isMobile,
    } = props
    let {
        searchedValue,
        loading,

        checkFullHome,
        showDialogHandler,
        descriptionDialogHandler,
        smallDialogHandler
    } = store
    let containerRef = useRef()


    const handleNavigateToDetail = async (item,) => {
        showDialogHandler.set({
            open: true,
            customContent: <Stack>
                <ShowDetail id={item.id}
                            show={item}
                            refreshData={() => {
                            }}
                            resetDrawerStateLocal={async () => {
                                history.push("/")
                                showDialogHandler.reset()
                                descriptionDialogHandler.reset()
                            }}/>
                {isMobile && <CardItemExpanded
                    item={item}
                />}
            </Stack>,
            variant: "persistent",
            customActions: <></>,
            noTitle: true,
            name: item.name,
            customStyle: isMobile ? {width: "100%", title: {padding: 0}} : {title: {padding: 0}},
            key: uuidGenerator()
        })
        if (!isMobile) {
            descriptionDialogHandler.set({open: true, item})
        }
    }

    useEffect(() => {

        (async () => {
            if (searchValues.sh) {
                smallDialogHandler.set({
                    open: true,
                    isDialog: true,
                    title: "Import link found",
                    content: "Do you want to import all the data from the link?",
                    rightButtonText: "Import",
                    leftButtonText: "Cancel",
                    paperSizeIsSmallest: true,
                    handleClose: () => {
                        smallDialogHandler.reset()
                        history.push("/")
                    },
                    handleSubmit: async () => {
                        smallDialogHandler.reset()
                        loading.set(true)
                        let data = await readSharedData(searchValues.sh)
                        history.push("/")
                        if (data.redirectToSeason) {
                            await setConf("showSeasonSelected" + data.idShow, data.redirectToSeason)
                        }
                        let res = await getSerieTVDetail(data.idShow)
                        loading.set(false)
                        await handleNavigateToDetail(res.data)
                    }
                })
            }

            if (searchValues.idShow) {
                let res = await getSerieTVDetail(searchValues.idShow)
                await handleNavigateToDetail(res.data)
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    //TODO splittarlo sulle sezioni singole
    let handleSearch = async (value) => {
        loading.set(true)
        searchedValue.set(value)
        checkFullHome.set(true)

        loading.set(false)
    }

    const EmptyHome = () => {

        return <>
            <Stack
                sx={{
                    height: "100%",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "120px 0",
                    position: "relative"
                }}
                spacing={{xs: 1, sm: 2, md: 4}}
                direction={"column"}>
                {/* SearchPage */}
                <img alt="tv-show-big" src={TvShowBig}
                     style={{opacity: 0.4, objectFit: "contain", width: "100%", top: 0, maxWidth: "1600px"}}/>
                <div style={{
                    display: "flex",
                    maxWidth: "640px",
                    flexDirection: "column",
                    position: "absolute",
                    zIndex: 1,
                    padding: "0 10px"
                }}>
                    <Typography variant={"h3"} fontWeight={600} sx={{lineHeight: "80px", color: "white"}}
                                textAlign={"center"}>
                        Watch Unlimited Movies, TV Shows
                        <span style={{color: "var(--first-color)"}}>
                    {` & More `}
                    </span>
                    </Typography>
                    <Typography textAlign={"center"} sx={{lineHeight: "27px", color: "white"}}>
                        You have seen no series.
                    </Typography>
                    <Typography textAlign={"center"} sx={{lineHeight: "27px", color: "white"}}>
                        Start going to search tab and adding new link to your favourites TV series.
                    </Typography>
                    <SearchBar

                        enableButton={true}
                        onChange={handleSearch}
                    />
                </div>
            </Stack>
            <Stack
                sx={{
                    height: "100%",
                    alignItems: "center",
                    justifyContent: "center"
                }}
                spacing={{xs: 1, sm: 2, md: 4}}
                direction={{xs: "column", sm: "row"}}>
                <Stack
                    xs={12}
                    sx={{display: "flex", justifyContent: "flex-end"}} direction={"column"}
                >
                    <img alt={"tv-assembled"} src={TvAssembled}
                         style={{height: "100%", width: "100%", maxWidth: "600px"}}/>
                </Stack>
                <Divider orientation={"vertical"}/>
                <Stack sx={{
                    display: "flex",
                    justifyContent: "flex-end",
                    paddingLeft: "10px"
                }}

                       direction={"column"}
                >
                    {/*Info*/}
                    <Typography variant={"h4"} fontWeight={"bold"} sx={{lineHeight: "60px", color: "white"}}>
                        Take
                        <span style={{color: "var(--first-color)"}}>
                {` note `}
                </span>
                        of viewed <br/>tv show/movies
                    </Typography>
                    <Typography sx={{lineHeight: "27px", color: "white"}}>
                        Keep track of tv show/movies to watch
                    </Typography>
                </Stack>
            </Stack>
            <Stack
                sx={{
                    height: "100%",
                    alignItems: "center",
                    justifyContent: "center"
                }}
                spacing={{xs: 1, sm: 2, md: 4}}
                direction={{xs: "column-reverse", sm: "row"}}
            >
                <Stack sx={{
                    display: "flex",
                    justifyContent: "flex-end",
                }}
                       direction={"column"}>
                    <Typography variant={"h4"} fontWeight={"bold"}
                                sx={{lineHeight: "60px", color: "white"}}

                    >
                        Have an episode detail <br/> like
                        <span style={{color: "var(--first-color)"}}>
                {` viewed `}
                </span>
                        or not
                    </Typography>
                    <Typography sx={{lineHeight: "27px", color: "white"}}>
                        Quickly find your favourite show <br/>on the most shared streaming services.
                    </Typography>
                </Stack>
                <Divider orientation={"vertical"}/>
                <Stack sx={{display: "flex", justifyContent: "flex-end"}} direction={"column"}>
                    {/*Image*/}
                    <img alt={"human"} src={Human} style={{height: "100%", width: "100%", maxWidth: "600px"}}/>
                </Stack>
            </Stack></>
    }

    const FullHome = () => {

        return <Stack paddingTop={"120px"}
        >

            <KeepWatchingSection
                {...{
                    store,
                }}
            />
            <MyItemsSection
                {...{
                    store,
                }}
            />

            <DiscoverSection
                {...{
                    store,
                }}
            />

            <SearchedItemsSection
                {...{
                    store,
                }}
            />


        </Stack>
    }

    return (<Container
            id={"mainContentScrollable"}
            maxWidth="false" sx={{height: "100%", background: "black", overflow: "auto"}} ref={containerRef}>
            <Header
                containerScroller={containerRef}
                customElement={!checkFullHome.get ? () => null :
                    () => <SearchBar
                        inHeader={true}
                        store={store}
                        onChange={handleSearch}
                    />}
            />
            {checkFullHome.get && <FullHome/>}
            {!checkFullHome.get && <EmptyHome/>}
            <PrivacyPolicy variant={true}/>

        </Container>
    )


}

export default withCommonPropsLite(Home)

const SearchedItemsSection = withCommonPropsLite(({
                                                      store,

                                                  }) => {

    let {searchedValue, loading, searchedItems, items} = store
    let checkNothingFoundSearchedItems = !!searchedValue.get && items.get?.length > 0 && searchedItems.get?.length === 0


    useEffect(() => {
        searchedItems.set([...items.get])
        loading.set(false)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (checkNothingFoundSearchedItems || (searchedItems.get?.length > 0 && searchedValue.get)) &&
        <Accordion defaultExpanded>
            <AccordionSummary
                expandIcon={<ExpandMore/>}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                {(checkNothingFoundSearchedItems || (searchedItems.get?.length > 0 && searchedValue.get)) &&
                    <Stack sx={{padding: "10px"}}>
                        <Typography typography={"h5"}>
                            Searched TV series ({searchedItems.get?.length})
                        </Typography>

                    </Stack>}
            </AccordionSummary>
            <AccordionDetails>
                {checkNothingFoundSearchedItems &&
                    <Typography typography={"h4"} textAlign={"center"}>
                        Nothing found
                    </Typography>
                }
                <Grid container>
                    {searchedValue.get && searchedItems.get.map((item, index) => {
                        return <CardItem
                            refreshData={() => {
                            }}
                            key={"cardItemSearch" + item.id}
                            id={"cardItemSearch" + item.id}
                            item={item}
                        />
                    })}
                </Grid>
            </AccordionDetails>

        </Accordion>

})


const MyItemsSection = withCommonPropsLite(({
                                                store,
                                                isMobile
                                            }) => {
    let {
        searchedValue, myItemsSectionOpen, loading, myItems, searchedMyItems, checkFullHome, items,
        searchedItems,
    } = store
    let [filterMyItems, setFilterMyItems] = useState(localStorage.getItem("filterMyItems") ?? "All")
    let [howManySkeleton, setHowManySkeleton] = useState(10)
    let checkNothingFoundMyItems = !!searchedValue.get && myItems.get.length > 0 && searchedMyItems.get.length === 0
    let [loader, stopLoading, startLoading] = useLoadingSystem(false)
    const refreshData = async (filter = filterMyItems) => {
        try {
            let res
            switch (filter) {
                case "Watching":
                    res = await getToWatchSeries()
                    break
                case "Following":
                    res = await getFollowedSeries()
                    break
                case "All":
                    res = await getSeries()
                    break
                default:
                    return refreshData(filterMyItems)

            }
            let tmp = res.map(x => {
                return {id: x.idShow, name: x.showName}
            })
            checkFullHome.set(tmp.length > 0 || (!!searchedValue.get))
            myItems.set(tmp)
            searchedMyItems.set(tmp)
            checkNothingFoundMyItems = !!searchedValue.get && tmp.length === 0
        } catch (e) {
            console.error("Errore retrieving Series", e, e.response)
        } finally {

            loading.set(false)
        }
    }

    useEffect(() => {

        (async () => {
            if (searchedValue.get) {
                setHowManySkeleton(2)
                startLoading()
                let itemTmp = await searchShow(searchedValue.get)
                items.set(itemTmp.results)

                let tmpSearchedMyItems = []
                let tmpMyItems = [...myItems.get]

                let tmpSearchedItems = [...itemTmp.results]

                if (tmpSearchedItems.length > 0) {
                    tmpSearchedItems.forEach((x) => {
                        tmpMyItems.forEach(y => {
                            if (x.id === y.id) {
                                tmpSearchedMyItems.push(y)
                            }
                        })

                    })
                    searchedMyItems.set(tmpSearchedMyItems)
                    if (tmpSearchedMyItems.length > 0) {
                        tmpSearchedMyItems.forEach(x => {
                            if (tmpSearchedItems.some(y => y.id === x.id)) {

                                tmpSearchedItems.forEach((tmp, index) => {
                                    if (tmp.id === x.id) {
                                        tmpSearchedItems.splice(index, 1)
                                    }
                                })

                            }
                        })
                    }
                    searchedItems.set(tmpSearchedItems)
                }


            } else {
                searchedItems.set([...items.get])
                searchedMyItems.set([...myItems.get])
            }
            stopLoading()
            setHowManySkeleton(10)
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [searchedValue.get])

    useEffect(() => {

        (async () => {
            myItemsSectionOpen.set(await getConf("myItemsSectionOpen"))
            if (myItems.refresh) {
                startLoading()
                let filter = await getConf("mySeriesFilter")
                if (filter) {
                    localStorage.setItem("filterMyItems", filter)
                    setFilterMyItems(filter)
                }

                await refreshData(filter,)
                stopLoading()
                myItems.setRefresh(false)
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [myItems.refresh, myItemsSectionOpen.get])

    return <Accordion expanded={!!myItemsSectionOpen.get}
                      onChange={async (event, expanded) => {
                          myItemsSectionOpen.set(expanded)
                          await setConf("myItemsSectionOpen", expanded)
                      }}

    >
        <AccordionSummary
            sx={{
                pointerEvents: "none"
            }}
            expandIcon={<ExpandMore
                sx={{
                    pointerEvents: "auto"
                }}
            />}

            aria-controls="panel1a-content"
            id="panel1a-header"
        >

            <Stack sx={{padding: "10px"}} width={"100%"}
                   onClick={(event) => {
                       event.preventDefault()
                       event.stopPropagation()
                   }}
            >
                <Stack direction={"row"} width={"100%"} alignItems={"center"}
                >
                    <Stack width={"100%"}>
                        <Typography typography={"h5"}>
                            My TV series ({searchedMyItems.get.length})
                        </Typography>
                    </Stack>
                    {(checkNothingFoundMyItems || searchedMyItems.get.length > 0) && myItemsSectionOpen.get &&
                        <Stack width={"100%"} direction={"row"} justifyContent={"flex-end"}>
                            <FormControl>
                                <InputLabel id="filter-label">Filter</InputLabel>
                                <Select
                                    sx={{
                                        pointerEvents: "auto"
                                    }}
                                    labelId="filter-label"
                                    id="filter-select"
                                    defaultValue={filterMyItems}
                                    label={"Filter"}
                                    onChange={async (event) => {

                                        setFilterMyItems(event.target.value)
                                        await setConf("mySeriesFilter", event.target.value)
                                        await refreshData(event.target.value)
                                    }}
                                >
                                    <MenuItem value={"All"}>All</MenuItem>
                                    <MenuItem value={"Watching"}>Watching</MenuItem>
                                    <MenuItem value={"Following"}>Following</MenuItem>
                                </Select>
                            </FormControl>
                        </Stack>}
                </Stack>

            </Stack>
        </AccordionSummary>
        {myItemsSectionOpen.get && <AccordionDetails>
            {!loader && checkNothingFoundMyItems &&
                <Typography typography={"h4"} textAlign={"center"}>
                    Nothing found
                </Typography>
            }
            <Grid container>
                {loader && Array(howManySkeleton).fill(0).map(() => <Grid
                    key={uuidGenerator()}
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    lg={3}
                    xl={2}
                    sx={{
                        paddingBottom: isMobile ? "10px" : "30px",
                        transition: "1s",
                    }}><Skeleton
                    variant={"rectangular"}
                    sx={{
                        height: "160px",
                        width: "98%"
                    }}/></Grid>)}
                {!loader && searchedMyItems.get.map((item, index) => {
                    return <CardItem
                        refreshData={refreshData}
                        key={"cardItem" + item.id}
                        id={"cardItem" + item.id}
                        item={item}
                    />
                })}
            </Grid>
        </AccordionDetails>}

    </Accordion>
})

const DiscoverSection = withCommonPropsLite(({
                                                 store,
                                                 isMobile
                                             }) => {

    let {
        searchedValue,
        discoverSectionOpen,
        searchedMyItems,
        discoverItems,
    } = store
    let searchedDiscoverItems = []
    let checkNothingFoundDiscover = !!searchedValue.get && discoverItems.get.length > 0 && searchedDiscoverItems.length === 0
    searchedDiscoverItems = discoverItems.get
    let [loader, stopLoading, startLoading] = useLoadingSystem(true)
    let [loaderCountry, stopLoadingCountry, startLoadingCountry] = useLoadingSystem(true)
    let [loaderProvider, stopLoadingProvider, startLoadingProvider] = useLoadingSystem(true)
    let isDesktop = useMediaQuery('(min-width:1600px)');
    useEffect(() => {
        (async () => {

            let tmpOpen = await getConf("discoverSectionOpen")
            discoverSectionOpen.set(tmpOpen)
            if (tmpOpen && discoverSectionOpen.get) {
                startLoading()
                startLoadingCountry()
                startLoadingProvider()
                let providerFilter = await getConf("providerFilter")
                if (providerFilter) {
                    discoverItems.setProviderSelected(providerFilter)
                    discoverItems.setOldProviderSelected(providerFilter)
                }
                let countryFilter = await getConf("countryFilter")
                if (countryFilter) {
                    discoverItems.setCountrySelected(countryFilter)
                    discoverItems.setOldCountrySelected(countryFilter)
                }
                discoverItems.setCountryList(await searchRegionProviders())
                await refreshData()

            } else {
                discoverItems.set([])
                discoverItems.setPage(1)
                discoverItems.setOldPage(1)
            }

        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [discoverSectionOpen.get])

    useEffect(() => {
        (async () => {
            let tmpOpen = await getConf("discoverSectionOpen")
            if (
                discoverItems.oldCountrySelected !== discoverItems.countrySelected
                &&
                tmpOpen
            ) {
                startLoading()
                startLoadingProvider()
                discoverItems.set([])
                discoverItems.setProviderList([])
                discoverItems.setProviderSelected(8)
                discoverItems.setOldProviderSelected(8)
                await setConf("providerFilter", 8)
                await refreshData()
                discoverItems.setOldCountrySelected(discoverItems.countrySelected)
                stopLoading()
                stopLoadingProvider()
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [discoverItems.countrySelected])

    useEffect(() => {
        (async () => {
            let tmpOpen = await getConf("discoverSectionOpen")
            if (
                discoverItems.oldProviderSelected !== discoverItems.providerSelected
                &&
                tmpOpen
            ) {
                startLoading()
                discoverItems.set([])
                await refreshDiscoverFilter()
                discoverItems.setOldProviderSelected(discoverItems.providerSelected)
                stopLoading()
            }
            if (
                discoverItems.oldPage !== discoverItems.page
                &&
                tmpOpen
            ) {
                startLoading()
                await refreshDiscoverFilter()
                discoverItems.setOldPage(discoverItems.page)
                stopLoading()
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [discoverItems.providerSelected, discoverItems.page])


    const refreshDiscoverFilter = async () => {
        let tmpDiscover = {result: []}
        tmpDiscover = await searchDiscoverShow(discoverItems.providerSelected, discoverItems.countrySelected, discoverItems.page)
        discoverItems.push(tmpDiscover.results)
        if (discoverItems.totalPage !== tmpDiscover.totalPages) {
            discoverItems.setTotalPage(tmpDiscover.totalPages)
        }

    }

    const refreshData = async () => {
        try {

            let prov
            // let tmpDf = discoverItems.providerSelected

            prov = await searchProvidersShow(discoverItems.countrySelected)
            discoverItems.setProviderList(prov)
            // if (!prov.some(x => x.provider_id === tmpDf)) {
            //     if (prov && prov.length > 0) {
            //         tmpDf = prov[0].provider_id
            //         await setConf("providerFilter", tmpDf)
            //         discoverItems.setProviderSelected(tmpDf)
            //     }
            // }
            await refreshDiscoverFilter()
            stopLoading()
            stopLoadingCountry()
            stopLoadingProvider()

        } catch (e) {
            console.error("Errore retrieving Series", e, e.response)
        }
    }


    const addDiscoverItemToMyItems = (itemId) => {
        let tmpSearchedMyItems = []

        if (discoverItems.length > 0) {
            discoverItems.forEach((x) => {
                if (x.id === itemId) {
                    tmpSearchedMyItems.push(x)
                }

            })
            searchedMyItems.set([...tmpSearchedMyItems.filter(x => {
                return !searchedMyItems.get.some(xx => xx.id === x.id)
            }).map(x => {
                return {id: x.id, name: x.title}
            }), ...searchedMyItems.get
            ])


        }
    }

    return !searchedValue.get && <Accordion expanded={!!discoverSectionOpen.get}

                                            onChange={async (event, expanded) => {
                                                await setConf("discoverSectionOpen", expanded)
                                                discoverSectionOpen.set(expanded)
                                                // if (expanded && searchedDiscoverItems.length === 0) {
                                                //     await refreshDiscoverFilter()
                                                // }
                                            }}
    >
        <AccordionSummary
            sx={{
                pointerEvents: "none"
            }}
            expandIcon={<ExpandMore
                sx={{
                    pointerEvents: "auto"
                }}
            />}

            aria-controls="panel1a-content"
            id="panel1a-header"
        >
            <Stack sx={{padding: "10px"}} width={"100%"}
                   onClick={(event) => {
                       event.preventDefault()
                       event.stopPropagation()
                   }}
            >
                <Stack direction={isMobile ? "column" : "row"} gap={"10px"} width={"100%"} alignItems={"center"}>
                    <Stack width={"100%"}>
                        <Typography typography={"h5"}>
                            Discover {discoverSectionOpen.get && "(" + searchedDiscoverItems.length + ")"}
                        </Typography>
                    </Stack>
                    {discoverSectionOpen.get &&
                        <Stack width={"100%"} direction={"row"} justifyContent={"flex-end"}>
                            {loaderCountry ? <Skeleton
                                variant={"rectangular"}
                                sx={{
                                    height: "60px",
                                    width: "82px",
                                    marginRight: "10px"
                                }}/> : <FormControl>
                                <InputLabel id="filter-label">Country</InputLabel>

                                <Select
                                    labelId="filter-label"
                                    id="filter-select"
                                    sx={{
                                        marginRight: "10px",
                                        pointerEvents: "auto"
                                    }}
                                    value={discoverItems.countrySelected}
                                    label={"Country"}
                                    onChange={async (event) => {
                                        await setConf("countryFilter", event.target.value)
                                        discoverItems.setCountrySelected(event.target.value)
                                    }}
                                >
                                    {discoverItems.countryList.map((x) => {
                                        return <MenuItem key={"provider" + x.iso_3166_1}
                                                         value={x.iso_3166_1}>{x.native_name}</MenuItem>
                                    })}
                                </Select>
                            </FormControl>
                            }
                            {loaderProvider ? <Skeleton
                                variant={"rectangular"}
                                sx={{
                                    height: "60px",
                                    width: "82px",
                                    marginRight: "10px"
                                }}/> : <FormControl>
                                <InputLabel id="filter-label">Provider</InputLabel>
                                <Select
                                    labelId="filter-label"
                                    id="filter-select"
                                    value={discoverItems.providerSelected}
                                    sx={{
                                        pointerEvents: "auto"
                                    }}
                                    label={"Provider"}
                                    onChange={async (event) => {
                                        await setConf("providerFilter", event.target.value)
                                        discoverItems.setProviderSelected(event.target.value)
                                    }}
                                >
                                    {discoverItems.providerList.map((x) => {
                                        return <MenuItem key={"provider" + x.provider_id}
                                                         value={x.provider_id}>{x.provider_name}</MenuItem>
                                    })}
                                </Select>
                            </FormControl>}

                        </Stack>}
                </Stack>
            </Stack>
        </AccordionSummary>
        <AccordionDetails>

            {checkNothingFoundDiscover &&
                <Typography typography={"h4"} textAlign={"center"}>
                    Nothing found
                </Typography>
            }
            <Grid container>
                {searchedDiscoverItems.map((item, index) => {
                    return <CardItem
                        refreshData={addDiscoverItemToMyItems}
                        key={"cardItem" + item.id}
                        id={"cardItem" + item.id}
                        item={item}
                    />
                })}
                {loader && Array(20).fill(0).map(() => <Grid
                    key={uuidGenerator()}
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    lg={3}
                    xl={2}
                    sx={{
                        padding: isDesktop ? "20px" : "10px",
                        transition: "1s",
                    }}><Skeleton
                    variant={"rectangular"}
                    sx={{
                        height: "168px",
                        width: "100%"
                    }}/></Grid>)}
                <Grid item xs={12} sx={{
                    display: "flex",
                    justifyContent: "center"
                }}>
                    <LoadingButton
                        loading={loader}
                        loadingPosition="start"
                        onClick={async () => {
                        if (discoverItems.page + 1 < discoverItems.totalPage) {
                            discoverItems.setPage(discoverItems.page + 1)
                        }
                    }} variant={"contained"} color={"primary"}>
                        Load more
                    </LoadingButton>
                </Grid>
            </Grid>
        </AccordionDetails>

    </Accordion>

})


const KeepWatchingSection = observer(({
                                          store,
                                          drawerRef,
                                          drawerRefLeft,
                                      }) => {
    let {keepWatchingSectionOpen, loading, keepWatchingItems, searchedItems} = store
    let checkNothingFoundMyItems = keepWatchingItems.get.length === 0 || searchedItems.get.length > 0

    const refreshData = async () => {

        try {
            let res = await getLastSeenSeries()
            let tmp = res.map(x => {
                return {id: x.idShow, name: x.showName}
            })

            keepWatchingItems.set(tmp)
            checkNothingFoundMyItems = tmp.length === 0
        } catch (e) {
            console.error("Errore retrieving Series", e, e.response)
        } finally {
            loading.set(false)
        }
    }

    useEffect(() => {

        (async () => {
            if (keepWatchingItems.refresh) {
                keepWatchingSectionOpen.set(await getConf("keepWatchingSectionOpen"))
                await refreshData()
                keepWatchingItems.setRefresh(false)
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [keepWatchingItems.refresh])

    return !checkNothingFoundMyItems && <Accordion expanded={!!keepWatchingSectionOpen.get}
                                                   onChange={async (event, expanded) => {
                                                       keepWatchingSectionOpen.set(expanded)
                                                       await setConf("keepWatchingSectionOpen", expanded)
                                                   }}

    >
        <AccordionSummary
            sx={{
                pointerEvents: "none"
            }}
            expandIcon={<ExpandMore
                sx={{
                    pointerEvents: "auto"
                }}
            />}

            aria-controls="panel1a-content"
            id="panel1a-header"
        >

            <Stack sx={{padding: "10px"}} width={"100%"}
                   onClick={(event) => {
                       event.preventDefault()
                       event.stopPropagation()
                   }}
            >
                <Stack direction={"row"} width={"100%"} alignItems={"center"}
                >
                    <Stack width={"100%"}>
                        <Typography typography={"h5"}>
                            Keep watching ({keepWatchingItems.get.length})
                        </Typography>
                    </Stack>
                </Stack>

            </Stack>
        </AccordionSummary>
        <AccordionDetails>
            <Grid container>
                {keepWatchingSectionOpen.get && keepWatchingItems.get.map((item, index) => {
                    return <CardItem
                        refreshData={refreshData}
                        drawerRef={drawerRef}
                        drawerRefLeft={drawerRefLeft}
                        key={"cardItem" + item.id}
                        id={"cardItem" + item.id}
                        item={item}
                    />
                })}
            </Grid>
        </AccordionDetails>

    </Accordion>
})
