import {
    Button,
    Collapse,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Grid,
    IconButton,
    MenuItem,
    Skeleton,
    Stack,
    Tab,
    Tabs,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import React, {useEffect, useState} from "react";

import {ArrowForwardIos, Assignment, ContentCopy, ContentPaste, Info} from "@mui/icons-material";
import {getEpisodes, getImage} from "../../constants/CrawfishApi";
import FunctionalButton from "../../component/FunctionalButton/FunctionalButton";
import EpisodeCard from "../../component/EpisodeCard/EpisodeCard";
import useLoadingSystem from "../../component/LoadingPage/useLoadingSystem";
import {getConf, saveUrlEpisode, setConf} from "../../constants/StorageHelper";
import {copyToClipboard, uuidGenerator, validURL} from "../../constants/Utility";
import UploadButton from "../../component/UploadButton/UploadButton";
import ShareButton from "../../component/FunctionalButton/ShareButton";
import ImageTest from "../../asset/image/logo-quix-defaultNoImg.png";
import {useLocation} from "react-router-dom";
import queryString from "query-string";
import {useAppStore} from "../../component/MobxContext/AppContext";
import {observer} from "mobx-react";
import DeleteShowButton from "../../component/DeleteShowButton/DeleteShowButton";
import Queue from "../../component/Queue";

const DEFAULT_PADDING = 2;

let queue = new Queue(true)
const ShowDetail = (props) => {
    let
        {id, resetDrawerStateLocal, show, refreshData} = props
    let [seasonSelected, setSeasonSelected] = useState(
        (show && show.seasons && show.seasons.length > 0 && show.seasons[0].season_number)
    )

    let [key, setKey] = useState("1");
    let [loaderSeason, stopLoadingSeason] = useLoadingSystem(true, {enabled: true, height: "68px", width: "100%"})
    let [episodes, setEpisodes] = useState([])
    let [openDialogImportMassiveLinks, setOpenDialogImportMassiveLinks] = useState(false)
    let [image, setImage] = useState(
        // getConfLocal(id + "image") ||
        ImageTest);

    let location = useLocation();
    let searchValues = queryString.parse(location.search)
    let store = useAppStore()
    let {keepWatchingItems, showDetail, plyrProps, loadingImages} = store
    let {player} = plyrProps

    useEffect(() => {
        (async () => {
            loadingImages.set(show.backdrop_path ?? show.poster_path, false)
            try {
                player.resetEpisodeList()
                player.setSelectedEpisodeIndex(1)
                player.setSelectedEpisodeLink(null)
                let ss
                queue.add(async () => {
                    setImage(await getImage(show.backdrop_path ?? show.poster_path))
                    loadingImages.set(show.backdrop_path ?? show.poster_path, true)
                })
                if (
                    searchValues.seasonNumber
                ) {
                    await setConf("showSeasonSelected" + id, parseInt(searchValues.seasonNumber))
                    setSeasonSelected(parseInt(searchValues.seasonNumber))
                    showDetail.selectedSeasonToInsertLink.set(parseInt(searchValues.seasonNumber))
                    ss = searchValues.seasonNumber
                } else {
                    let s = await getConf("showSeasonSelected" + id)

                    if (s) {
                        ss = s
                        setSeasonSelected(s)
                        showDetail.selectedSeasonToInsertLink.set(s)
                    } else {
                        ss = seasonSelected
                        await setConf("showSeasonSelected" + id, seasonSelected)
                    }
                }

                await refreshEpisodes(ss)

            } catch (e) {
                console.error("errore recuper dettaglio serie", e, e.response)
            }

        })()
        return () => {
            queue.emptyQueue()
        }
    }, [])


    const refreshEpisodes = async (s = seasonSelected) => {
        let response = await getEpisodes(s, id);
        if (response?.data?.episodes) {
            setEpisodes(response.data.episodes)
        }

        stopLoadingSeason()
    }


    return (<Container maxWidth="false"
        >
            <AddMassiveLinksContent
                refreshEpisodes={async () => {
                    await refreshEpisodes()
                    setKey(uuidGenerator())
                }}
                show={show}
                seasonSelected={seasonSelected}
                episodes={episodes}
                open={openDialogImportMassiveLinks}
                handleClose={() => setOpenDialogImportMassiveLinks(false)}
            />

            <Grid container sx={{height: "100%"}} alignContent={"flex-start"}>
                <Grid item xs={12}>
                    <Stack
                        sx={{
                            backgroundColor: "rgba(0, 0, 0,0.6)", position: "relative",
                            minHeight: "200px"
                        }}
                        direction={"row"} justifyContent={"space-between"} alignItems={"flex-start"}
                        height={"100%"}
                    >
                        <Stack
                            id={"backgroundShowImage"}
                            sx={{
                                backgroundImage: "url(" + image + ")",
                                backgroundRepeat: "no-repeat",
                                backgroundPosition: "center",
                                backgroundSize: "cover",
                                height: "100%",
                                width: "100%",
                                position: "absolute",
                                zIndex: -1,
                                minHeight: "200px;"
                            }}
                        />

                        <Stack width={"100%"} justifyContent={"flex-start"}
                               height={"100%"}
                               padding={"10px"}
                        >
                            <Stack direction={"row"} alignItems={"center"}>
                                <Button color={"primary"}
                                        sx={{height: "50px", marginRight: "20px"}}
                                        variant={"contained"}
                                        onClick={async () => {
                                            await resetDrawerStateLocal()
                                            refreshData()
                                            keepWatchingItems.setRefresh(true)
                                        }}><ArrowForwardIos/></Button>
                                <Typography
                                    textAlign={"right"}
                                    sx={{marginRight: "10px"}}
                                    variant={"title"}>
                                    {show.name}</Typography>
                            </Stack>
                            <Stack direction={"row"} width={"100%"} justifyContent={"space-between"} height={"100%"}
                                   alignItems={"flex-end"}>
                                <Stack direction={"row"}>
                                    <Tooltip title={"Add episodes link to the show"}>
                                        <IconButton onClick={() => {
                                            setOpenDialogImportMassiveLinks(true)
                                        }}>
                                            <Assignment fontSize={"large"}/>
                                        </IconButton>
                                    </Tooltip>
                                    <ShareButton
                                        seasonSelected={seasonSelected}
                                        episodes={episodes}
                                        typeButton={"SHARE_SEASON"}
                                        show={show}
                                        id={"shareButton"}
                                    />
                                    <DeleteShowButton
                                        show={show}
                                        seasonSelected={seasonSelected}
                                        episodes={episodes}
                                        resetDrawerStateLocal={resetDrawerStateLocal}
                                        refreshData={refreshData}
                                    />
                                </Stack>
                                <Stack direction={"row"}>
                                    <FunctionalButton
                                        typeButton={"WATCHING"}
                                        typeItem={"show"}
                                        show={show}
                                    />

                                    <FunctionalButton
                                        typeButton={"FOLLOWING"}
                                        typeItem={"show"}
                                        show={show}
                                    />
                                </Stack>
                            </Stack>
                        </Stack>
                    </Stack>
                </Grid>
                <Grid item xs={12}
                >
                    <Tabs value={seasonSelected}
                          variant="scrollable"
                          scrollButtons={"auto"}

                    >
                        {loaderSeason ? Array(show.seasons).fill(0).map(() => <Tab
                                sx={{
                                    textTransform: "capitalize"
                                }}
                                key={uuidGenerator()}
                                label={<Skeleton
                                    variant={"rectangular"}
                                    sx={{
                                        height: "68px",
                                        width: "155px"
                                    }}/>
                                }/>)
                            :
                            show.seasons && show.seasons.map((season, index) => {
                                return <Tab
                                    onClick={async () => {
                                        loadingImages.set(show.backdrop_path ?? show.poster_path, false)
                                        setSeasonSelected(season.season_number)
                                        await setConf("showSeasonSelected" + id, season.season_number)
                                        showDetail.selectedSeasonToInsertLink.set(parseInt(season.season_number))
                                        loadingImages.set(show.backdrop_path ?? show.poster_path, true)
                                        await refreshEpisodes(season.season_number)
                                    }}
                                    sx={{
                                        textTransform: "capitalize"
                                    }} key={index} value={season.season_number}
                                    label={<Typography typography={"h6"}>
                                        {season.name.toLowerCase()}
                                    </Typography>}/>

                            })

                        }
                    </Tabs>
                </Grid>
                <Grid item xs={12} sx={{padding: "5px"}}
                >
                    {!loadingImages[show.backdrop_path ?? show.poster_path] ? <Skeleton
                        variant={"rectangular"}
                        sx={{

                            height: "58px",
                            width: "98%"
                        }}/> : <Grid container
                                     key={"episodeContainer" + key}
                                     id={"episodeContainer" + key}
                    >
                        {episodes.map((x, xIndex) => {

                            return <Grid item xs={12} sm={12} md={6}
                                         sx={{padding: "2px"}}
                                         key={"episode" + x.id}
                            ><EpisodeCard
                                show={show}
                                episode={x}
                                seasonNumber={seasonSelected}
                            />
                            </Grid>
                        })}
                    </Grid>}
                </Grid>
            </Grid>
        </Container>
    )

}

export default (ShowDetail)

const AddMassiveLinksContent = observer((props) => {
    let [tabMassiveValue, setTabMassiveValue] = useState(0)
    let [inputPadding, setInputPadding] = useState(DEFAULT_PADDING)
    let {episodes, open, handleClose, seasonSelected, show, refreshEpisodes} = props
    let [alertDialogProps, setAlertDialogProps] = useState({open: false, type: "S"})
    let store = useAppStore()
    let {showDetail, snackbarHandler} = store

    let rightButtonText = "Fetch & Save",
        handleSubmit = async () => {
            try {
                let inputLinkPattern = document.getElementById("linkPattern").value
                if (!inputLinkPattern || !inputLinkPattern.includes("[@EP]")) {
                    setAlertDialogProps({open: true, type: "MT"})
                } else {
                    for (let i of episodes) {
                        let temp = inputLinkPattern.includes("[@EP]") ? inputLinkPattern.replace("[@EP]", i.episode_number.toString().padStart(inputPadding, '0')) : inputLinkPattern;
                        if (validURL(temp)) {
                            let input = {
                                tvShowId: show.id,
                                episodeNumber: i.episode_number,
                                episodeImage: i.still_path,
                                seasonNumber: showDetail.selectedSeasonToInsertLink.get,
                                showName: show.name,
                                // showDetail: JSON.stringify(show),
                                episodeName: i.name,
                                link: temp
                            };
                            await saveUrlEpisode(input);
                        }
                    }
                    setAlertDialogProps({open: true, type: "S"})
                    handleClose();
                    setInputPadding(DEFAULT_PADDING)
                    await refreshEpisodes()
                }


            } catch (e) {
                console.error("Error saving the episodes", e)
            }

        }

    if (tabMassiveValue === 1) {
        handleSubmit = async () => {
            try {
                let multilineLink = document.getElementById("linkAreaText").value
                if (!multilineLink || multilineLink.length === 0) {
                    setAlertDialogProps({open: true, type: "NL"})
                } else {
                    let lines = multilineLink.split("\n").map(x => x.trim());
                    for (let index in episodes) {
                        let item = episodes[index];
                        //Se non ho elementi fermo il ciclo
                        if (lines.length < index) {
                            break;
                        }
                        let link = lines[index];
                        if (validURL(link)) {
                            let input = {
                                tvShowId: show.id,
                                episodeNumber: item.episode_number,
                                episodeImage: item.still_path,
                                seasonNumber: showDetail.selectedSeasonToInsertLink.get,
                                showName: show.name,
                                episodeName: item.name,
                                link
                            };
                            await saveUrlEpisode(input);
                        }
                    }
                    setAlertDialogProps({open: true, type: "S"})
                    setInputPadding(DEFAULT_PADDING)
                    handleClose();
                    await refreshEpisodes()
                }

            } catch (e) {

            }


        }
    }
    if (tabMassiveValue === 2) {
        handleSubmit = async () => {
            try {
                let jsonContent = document.getElementById("linkAreaTextJSON").value
                if (!jsonContent || jsonContent.length === 0) {
                    setAlertDialogProps({open: true, type: "NL"})
                } else {
                    let links = JSON.parse(document.getElementById("linkAreaTextJSON").value).episodes
                    for (let index in episodes) {
                        let item = episodes[index];
                        //Se non ho elementi fermo il ciclo
                        if (links.length < index) {
                            break;
                        }
                        let link = links[index];
                        if (validURL(link)) {
                            let input = {
                                tvShowId: show.id,
                                episodeNumber: item.episode_number,
                                episodeImage: item.still_path,
                                seasonNumber: showDetail.selectedSeasonToInsertLink.get,
                                showName: show.name,
                                episodeName: item.name,
                                link
                            };
                            await saveUrlEpisode(input);
                        }
                    }
                    setAlertDialogProps({open: true, type: "S"})
                    handleClose();
                    await refreshEpisodes()
                }
            } catch (e) {

                setAlertDialogProps({open: true, type: "JNV"})
                console.error("Error associating json", e)
            } finally {

            }


        }

    }

    return <>
        <AlertDialog
            alertDialogProps={alertDialogProps}
            handleClose={() => setAlertDialogProps(p => ({...p, open: false}))}
        />
        <Dialog onClose={handleClose} open={open} PaperProps={{sx: {overflow: "hidden"}}}>
            <DialogTitle>
                Upload link whole season
            </DialogTitle>
            <DialogContent>
                <Tabs
                    value={tabMassiveValue}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="scrollable"
                    scrollButtons={"auto"}
                    onChange={(event, value) => {
                        setTabMassiveValue(value)
                    }}
                    style={{alignSelf: "center", marginBottom: "5px"}}
                >
                    <Tab label="Pattern"/>
                    <Tab label="Link list"/>
                    <Tab label="JSON"/>
                </Tabs>
                <DialogContentText
                    sx={{cursor: "pointer"}}
                    onClick={() => {
                        showDetail.needHelp.set(!showDetail.needHelp.get)
                    }}>
                    Need help? <IconButton
                    color={"primary"}
                ><Info/></IconButton>

                </DialogContentText>
                <TextField
                    sx={{marginTop: "10px"}}
                    label={"Select the season"}
                    id="selectSeason"
                    defaultValue={showDetail.selectedSeasonToInsertLink.get}
                    onChange={(event) => {
                        let s = event.target.value
                        showDetail.selectedSeasonToInsertLink.set(s)
                    }}
                    fullWidth
                    required
                    select
                    color={"primary"}
                >
                    {show.seasons && show.seasons.map((s, sIndex) => {
                        return <MenuItem
                            value={s.season_number}
                            key={"seasonNumber" + sIndex}>
                            Season {s.season_number}
                        </MenuItem>

                    })}
                </TextField>
                {tabMassiveValue === 0 && <>
                    <DialogContentText>
                        <Collapse in={showDetail.needHelp.get}>
                            <br/>
                            Add links to the season episodes following a pattern:
                            <br/><br/>
                            1) Find the episode number in the link
                            <br/>
                            "ShowName_Ep_01_SUB_LANG.mp4"
                            <br/>
                            In this case is the "01" after "Ep_"
                            <br/>
                            3) Copy the identifier [@EP] clicking here --><IconButton
                            color={"primary"}
                            onClick={async () => {
                                await copyToClipboard("[@EP]")
                                snackbarHandler.set(
                                    "top",
                                    "right",
                                    true,
                                    3000,
                                    "Identifier [@EP] copied to the clipboard",
                                    "success",
                                );
                            }}><ContentCopy/></IconButton>
                            <br/>
                            4) Replace "01" with our identifier "[@EP]"
                            <br/>
                            Highlight the number to replace and then paste the identifier copied above (CTRL+V)
                            or click this button --><Button
                            onSelect={(event) => {
                                let sel = event.target.value.substring(event.target.selectionStart, event.target.selectionEnd)
                                if (sel && sel.length > 0) {
                                    showDetail.isSelectedLinkPart.set(true)
                                } else {
                                    showDetail.isSelectedLinkPart.set(false)
                                }
                            }}
                            onClick={async () => {

                                let txtarea = document.getElementById("linkPattern");
                                let start = txtarea.selectionStart;
                                let finish = txtarea.selectionEnd;
                                let allText = txtarea.value;
                                let sel = allText.substring(start, finish);

                                if (sel && sel.length > 0) {
                                    txtarea.value = allText.substring(0, start) + "[@EP]" + allText.substring(finish, allText.length);
                                    txtarea.focus();
                                    txtarea.setSelectionRange(txtarea.value.length, txtarea.value.length);
                                    snackbarHandler.set(
                                        "top",
                                        "right",
                                        true,
                                        3000,
                                        sel + " replaced with [@EP] in the link",
                                        "success",
                                    );
                                } else {
                                    await copyToClipboard("[@EP]")
                                    snackbarHandler.set(
                                        "top",
                                        "right",
                                        true,
                                        3000,
                                        "Identifier [@EP] copied to the clipboard",
                                        "success",
                                    );
                                }

                            }}
                            variant={"contained"} color={"primary"}
                            endIcon={showDetail.isSelectedLinkPart.get ? <ContentPaste/> :
                                <ContentCopy/>}>[@EP]</Button>
                            <br/>
                        </Collapse>
                    </DialogContentText>
                    <TextField
                        sx={{marginTop: "10px"}}
                        autoFocus
                        id="linkPattern"
                        fullWidth
                        required
                        color={"primary"}
                        label={"Paste the link"}
                        onSelect={(event) => {
                            let sel = event.target.value.substring(event.target.selectionStart, event.target.selectionEnd)
                            if (sel && sel.length > 0) {
                                showDetail.isSelectedLinkPart.set(true)
                            } else {
                                showDetail.isSelectedLinkPart.set(false)
                            }
                        }}
                        InputProps={{
                            endAdornment: <Button
                                onClick={async () => {

                                    let txtarea = document.getElementById("linkPattern");
                                    let start = txtarea.selectionStart;
                                    let finish = txtarea.selectionEnd;
                                    let allText = txtarea.value;
                                    let sel = allText.substring(start, finish);

                                    if (sel && sel.length > 0) {
                                        txtarea.value = allText.substring(0, start) + "[@EP]" + allText.substring(finish, allText.length);
                                        txtarea.focus();
                                        txtarea.setSelectionRange(txtarea.value.length, txtarea.value.length);
                                        snackbarHandler.set(
                                            "top",
                                            "right",
                                            true,
                                            3000,
                                            sel + " replaced with [@EP] in the link",
                                            "success",
                                        );
                                    } else {
                                        snackbarHandler.set(
                                            "top",
                                            "right",
                                            true,
                                            3000,
                                            "Identifier [@EP] copied in the clipboard",
                                            "success",
                                        );
                                        await copyToClipboard("[@EP]")
                                    }


                                }}
                                variant={"contained"} color={"primary"}
                                endIcon={showDetail.isSelectedLinkPart.get ? <ContentPaste/> :
                                    <ContentCopy/>}>[@EP]</Button>
                        }}
                    />
                    <DialogContentText>
                        <Collapse in={showDetail.needHelp.get}>
                            <br/>Insert the padding for the number to place inside the link
                            <br/>
                            (Es. "Ep_01" the padding is 2)
                            <br/>
                            (Es. "Ep_001" the padding is 3)
                            <br/>
                        </Collapse>
                    </DialogContentText>
                    <TextField
                        sx={{marginTop: "10px"}}
                        id="padding"
                        label={"Select the correct padding"}
                        InputProps={{inputProps: {min: 0, max: 10}}}
                        type={"number"}
                        fullWidth
                        value={inputPadding}
                        onChange={(e) => {
                            setInputPadding(e.target.value)
                        }}
                        color={"primary"}
                    />
                </>}
                {tabMassiveValue === 1 && <>
                    <DialogContentText>
                        <Collapse in={showDetail.needHelp.get}>
                            Insert the links of the episodes, the links must be in order and one per line. All
                            links
                            greater than the number of episodes will be ignored<br/>
                            <br/>- Link ep1<br/>
                            <br/>- Link ep2<br/><br/>
                        </Collapse>
                    </DialogContentText>
                    <TextField
                        id="linkAreaText"
                        multiline
                        rows={(episodes && episodes.length) || 4}
                        placeholder={"Insert the link here"}
                        variant="outlined"
                        style={{width: "100%", marginTop: "10px"}}
                    />
                </>}
                {tabMassiveValue === 2 && <>
                    <DialogContentText sx={{marginBottom: "10px"}}>
                        {/* eslint-disable-next-line no-useless-concat */}
                        <Collapse in={showDetail.needHelp.get}>
                            Upload the JSON file, it need to be like this:
                            <br/>
                            {"{" + '"episodes"' + ":[" + '"episode1Link"' + "," + '"episode2Link"' + "]}"}
                        </Collapse>
                    </DialogContentText>
                    <UploadButton
                        uploadButtonOnClick={(event) => {
                            let fr = new FileReader();
                            let output = ""
                            fr.onload = () => {
                                let obj = JSON.parse(fr.result);
                                output = JSON.stringify(obj, undefined, 4);
                                document.getElementById("linkAreaTextJSON").value = output

                            }
                            fr.readAsText(event.target.files[0]);

                        }}
                        removeButtonOnClick={(event) => {
                            console.error("delete aa", event.target.value)
                        }}
                    />
                    <DialogContentText>
                        <Collapse in={showDetail.needHelp.get}>
                            <br/>or copy paste the JSON body:<br/><br/>
                        </Collapse>
                    </DialogContentText>
                    <TextField
                        id="linkAreaTextJSON"
                        name="linkAreaTextJSON"
                        multiline
                        rows={(episodes && episodes.length) || 4}
                        placeholder={"Insert the JSON body here"}
                        variant="outlined"
                        style={{width: "100%", marginTop: "10px"}}
                    />
                </>}
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleClose}
                    color={"primary"}
                    variant={"text"}
                >
                    {"Close"}
                </Button>
                <Button
                    variant={"contained"}
                    color={"primary"}
                    onClick={handleSubmit}
                >
                    {rightButtonText}
                </Button>
            </DialogActions>


        </Dialog></>
})

const AlertDialog = (props) => {
    let {alertDialogProps, handleClose} = props
    let action = "Close", title, content

    switch (alertDialogProps.type) {
        case "NL":
            title = "Wait a minute"
            content = "No links inserted!"
            break
        case "JNV":
            title = "Oh no"
            content = "JSON file not valid!"
            break
        case "MT":
            title = "You had one job"
            content = "Missing tag [@EP]"
            break
        case "S":
        default:
            title = "Let's go"
            content = "Links associated with success!"
            break

    }

    return <Dialog
        onClose={handleClose}
        open={alertDialogProps.open}
        PaperProps={{sx: {overflow: "hidden"}}}
    >
        <DialogTitle>
            {title}
        </DialogTitle>
        <DialogContent>
            <DialogContentText color={"secondary"}>
                {content}
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button
                variant={"contained"}
                color={"primary"}
                onClick={handleClose}
            >
                {action}
            </Button>
        </DialogActions>


    </Dialog>
}


