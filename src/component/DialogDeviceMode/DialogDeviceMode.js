import {useEffect, useState} from "react";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";
import {getConf, setConf} from "../../constants/StorageHelper";
import {SimpleDialog} from "../SimpleDialog/SimpleDialog";
import {Button, DialogContent, Stack, Typography} from "@mui/material";
import QRCode from "qrcode.react";

const DEVICE_MODE_DISABLE_CHECK = "DEVICE_MODE_DISABLE_CHECK"

const DialogDeviceMode = (props) => {
    let [checkDevice, setCheckDevice] = useState(false)
    let [loading, stopLoading] = useLoadingSystem(true, {enabled: false, height: "40px", width: "100%"})

    let hostId = "asd"

    useEffect(() => {

        (async () => {
            //TODO per farlo ricomparire
            //await setConf(DEVICE_MODE_DISABLE_CHECK,false)
            setCheckDevice(await getConf(DEVICE_MODE_DISABLE_CHECK))
            stopLoading()
        })()

    }, [])

    return loading ? loading : <SimpleDialog
        isDialogOpen={!checkDevice}
        handleClose={() => setCheckDevice(true)}
        dialogTitle={<Stack direction={"row"} justifyContent={"center"} alignItems={"flex-start"}>
            <Typography variant={"title"} >Scan the QR to use this device as video player</Typography>
        </Stack>}
        dialogContent={<DialogContent>
            <Stack  justifyContent={"center"} alignItems={"center"}>

                    <QRCode
                        size={200}
                        includeMargin={true}
                        value={window.location.origin+"/?pathId=" + hostId}/>
            </Stack>
        </DialogContent>}
        dialogActions={<Stack width={"100%"} direction={"row"} justifyContent={"space-between"} alignItems={"flex-start"}>
            <Button variant={"contained"} color={"secondary"}
            onClick={async()=>{
                setCheckDevice(true)
                await setConf(DEVICE_MODE_DISABLE_CHECK,true)
            }}
            >
                Never again
            </Button>
            <Button onClick={() => setCheckDevice(true)} variant={"contained"} color={"primary"}>
                Not today
            </Button>
        </Stack>}
    />

}

export default DialogDeviceMode