import WebTorrent from "webtorrent";
import React, {useEffect, useState} from "react";
import {Box, Button, Container, Grid, Stack, Tooltip, Typography} from "@mui/material";
import {Download, DownloadForOffline, Upload} from "@mui/icons-material";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";
import {uuidGenerator} from "../../constants/Utility";
import '../VideoPlayer/VideoPlayer.css'
import {observer} from "mobx-react";
import {useAppStore} from "../MobxContext/AppContext";
import CF_Logo from "../../asset/image/CF_logo_only.svg"
import {TypographyEllipsed} from "../TypographyEllipsed";
import LinearProgress from "@mui/material/LinearProgress";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";


const WebtorrentFileList = ((props) => {
    let store = useAppStore()

    let [loader, stopLoading] = useLoadingSystem(true, null, null, "Our hamsters army is preparing your download, please wait they are doing their best")
    let {episodeProps} = props
    let {selectedMagnet} = episodeProps
    let [
        // errorMessage
        , setErrorMessage] = useState()
    let [openFilePicker, setOpenFilePicker] = useState(false)
    let {webTorrentProps, } = store

    useEffect(() => {


        (() => {
            webTorrentProps.client.set(new WebTorrent({tracker: trackerOpts}))
            navigator.serviceWorker
                .register("sw.min.js", {scope: "/"})
                .then((reg) => {
                    console.log("register the worker");
                    const worker = reg.active || reg.waiting || reg.installing;

                    function checkState(worker) {
                        return (
                            worker.state === "activated" && webTorrentProps.client.get.loadWorker(worker, play)
                        );
                    }

                    if (!checkState(worker)) {
                        worker.addEventListener("statechange", ({target}) =>
                            checkState(target)
                        );
                    }
                })
                .catch(console.error);

            webTorrentProps.client.get.on("warning", (warning) => {
                console.warn("Error torrent: ", warning)
            });
            webTorrentProps.client.get.on("error", (error) => {
                console.error("Error validating torrent: ", error)
                setErrorMessage("Invalid torrent/magnet/hash")
                stopLoading()
            })
            // play()

        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    const play = () => {
        console.log("worker registered, going to get the file from the torrent");
        webTorrentProps.client.get.add(selectedMagnet, torrentOpts, (torrent) => {
            // Torrents can contain many files. Let's use the .mp4 file
            webTorrentProps.torrent.set(torrent)
            let filesTmp = torrent.files
            //     .filter(function (file) {
            //     return (
            //         file.name.endsWith(".mp4") ||
            //         file.name.endsWith(".3g2") ||
            //         file.name.endsWith(".m4v") ||
            //         file.name.endsWith(".mov") ||
            //         file.name.endsWith(".ogm") ||
            //         file.name.endsWith(".ogv") ||
            //         file.name.endsWith(".webm")
            //
            //     );
            // });
            // filesTmp = [...filesTmp, ...filesTmp]
            webTorrentProps.filesCanBeStreamed.set(filesTmp)
            //TODO levare
            console.info("file ready", torrent, filesTmp);

            torrent.on('download', (bytes) => {
                webTorrentProps.progress.set(torrent.progress)
                webTorrentProps.download.set(humanFileSize(torrent.downloadSpeed))
                webTorrentProps.filesToDownload.set(torrent.files)
            })
            torrent.on('upload', (bytes) => {
                webTorrentProps.upload.set(humanFileSize(torrent.uploadSpeed))
            })

            stopLoading()
            setOpenFilePicker(true)


        });
    };

//TODO gestire eventuali errori
    //far scegliere il file da riprodurre
    console.info("asdasd", webTorrentProps.progress.get, webTorrentProps.download.getSpeed)
    return (<Container maxWidth="false" sx={{background: "#303030"}}>
            {loader}
            {openFilePicker && <Stack height={"100%"} width={"100%"}>
                <Stack width={"100%"} padding={"20px 30px"}>

                    <Stack direction={"row"} width={"100%"} justifyContent={"center"} alignItems={"center"}>
                        <Typography variant={"small-text"} sx={{
                            textAlign: "center !important"
                        }}>
                            A friend is sharing the following torrent with you, you can download each file in the
                            torrent individually.
                        </Typography>
                    </Stack>
                    <Stack direction={"row"} width={"100%"} justifyContent={"center"} alignItems={"center"} marginTop={"20px"}>
                        <Typography variant={"small-text"}
                                    // onClick={() => {
                                    //     window.open("https://github.com/drakonkat/Crawfish/releases", '_blank').focus();
                                    // }}
                                    sx={{
                                        // cursor: "pointer",
                                        color: "primary.main",
                                        textAlign: "center !important",
                                        marginLeft: "5px"
                                    }}>
                            Powered by RapTorrent
                        </Typography>
                        <Button
                            color={"primary"}
                            // onClick={() => {
                            //     window.open("https://github.com/drakonkat/Crawfish/releases", '_blank').focus();
                            // }}
                            variant={"contained"}
                            sx={{
                                minWidth: "45px",
                                padding: 0,
                                marginLeft: "5px"
                            }}
                        >
                            <img src={CF_Logo} style={{width: "30px"}} alt={"cf-logo"}/>
                        </Button>
                    </Stack>
                </Stack>
                <UploadDownloadStats/>
                <ProgressBar/>
                <Stack width={"100%"} height={"100%"} sx={{overflow: "auto"}} padding={"20px 30px"}>
                    {webTorrentProps.filesCanBeStreamed.get && webTorrentProps.filesCanBeStreamed.get.map(file => {
                        let title = file.name

                        return <Stack
                            id={file.name}
                            key={file.name + uuidGenerator()}
                            sx={{
                                padding: "5px",
                                justifyContent: "space-between",
                                flexDirection: "row",
                                alignItems: "center"
                            }}>
                            <TypographyEllipsed fontWeight={300} variant={"small-text"} tooltip={file.name}>
                                {title}
                            </TypographyEllipsed>
                            <DownloadButtonStats name={title}/>
                        </Stack>
                    })}
                </Stack>
            </Stack>}

        </Container>

    );

})

export default WebtorrentFileList

const ProgressBar = withCommonPropsLite((props) => {
    let {store} = props
    let {webTorrentProps} = store
    return <Box sx={{display: 'flex', alignItems: 'center', minWidth: "150px", width: props?.width}}>
        <Box sx={{width: '100%', ml:"30px"}}>
            <LinearProgress variant="determinate" value={webTorrentProps.progress.get}/>
        </Box>
        <Box sx={{minWidth: 35,mr:"20px",ml:"30px"}}>
            <Typography variant="body2" color="text.secondary">{`${Math.round(
                webTorrentProps.progress.get
            )}%`}</Typography>
        </Box>
    </Box>

})


const UploadDownloadStats = observer(() => {
    let store = useAppStore()
    let {webTorrentProps} = store

    return<Stack direction={"row"} alignItems={"center"} width={"100%"} justifyContent={"center"}>
        <Tooltip title={"Download speed"}><Stack sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            marginRight: "30px",
            width: "50px"
        }}>
            <Download color={"primary"}/>
            <Typography textAlign={"left"}>{webTorrentProps.download.getSpeed}</Typography>
            <Typography variant={"smallText"}>{webTorrentProps.download.getUnit}</Typography>
        </Stack>
        </Tooltip>
        <Tooltip title={"Upload speed"}>
        <Stack sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            width: "50px"
        }}>
            <Upload color={"success"}/>
            <Typography textAlign={"left"}>{webTorrentProps.upload.getSpeed}</Typography>
            <Typography variant={"smallText"}>{webTorrentProps.upload.getUnit}</Typography>
        </Stack>
        </Tooltip>
    </Stack>
})


const round = (input) => {
    return Math.round(input * 100) / 100
}
const humanFileSize = (bytes, si = false, dp = 1) => {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
        return {
            speed: bytes,
            unit: "B"
        }

    }

    const units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10 ** dp;

    do {
        bytes /= thresh;
        ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


    return {
        speed: round(bytes.toFixed(dp)),
        unit: units[u]
    };
}
const trackers = [    'wss://tracker.btorrent.xyz',
    'wss://tracker.openwebtorrent.com',
    'wss://tracker.crawfish.cf',
    "udp://explodie.org:6969",
    "udp://tracker.coppersurfer.tk:6969",
    "udp://tracker.empire-js.us:1337",
    "udp://tracker.leechers-paradise.org:6969",
    "udp://tracker.opentrackr.org:1337",
    "wss://tracker.fastcast.nz",]

const rtcConfig =
    {
        "iceServers": [
            {
                "urls": "stun:23.21.150.121"
            },
            {
                urls: [
                    'stun:stun.l.google.com:19302',
                    'stun:global.stun.twilio.com:3478'
                ]
            },
            {
                "username": "admin",
                "credential": "Password1!",
                "urls": "turn:23.94.202.235:3478"
            }
        ]
    }

const torrentOpts = {
    announce: trackers
}

const trackerOpts = {
    announce: trackers,
    rtcConfig: rtcConfig
}


const DownloadButtonStats = withCommonPropsLite(({store, name}) => {
    let {webTorrentProps} = store
    let file = webTorrentProps.filesToDownload.get.find(x => {
        return x.name === name
    })
    if (!file) {
        return
    }

    return <Grid
        xs={4}
        id={name}
        key={name} item
        sx={{
            padding: "5px",
            display: "flex",
            justifyContent: "flex-start",
            flexDirection: "row",
            alignItems: "center"
        }}>
        <Button
            disabled={file.progress < 1}
            size={"small"}
            sx={{minWidth: "100px"}}
            onClick={() => {
                file.getBlobURL((err, url) => {
                    if (err) {
                        console.error("Error downloading file: ", err)
                    }
                    let link = document.createElement("a");
                    link.href = url;
                    link.download = name;
                    document.body.appendChild(link);
                    link.dispatchEvent(
                        new MouseEvent('click', {
                            bubbles: true,
                            cancelable: true,
                            view: window
                        })
                    );
                    document.body.removeChild(link);
                })
            }} variant={"contained"} color={"primary"}
            endIcon={<DownloadForOffline/>}>
            {/*({Math.round(webTorrentProps.progress.get * 100)}%)*/}
            ({Math.max(Math.round(file.progress * 100), 0)}%)
        </Button>
    </Grid>
})