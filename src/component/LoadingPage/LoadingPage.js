import style from "../../index.module.css";
import QuixLogoMobile from "../../asset/image/logo-quix-mobile.png";
import {Stack, Typography, useMediaQuery} from "@mui/material";

const LoadingPage = ({customSentence}) => {
    let isMobile = useMediaQuery('(max-width:868px)');
    return <Stack maxWidth="false" sx={{
        height: "100%",
        width: "100%",
        position: "absolute",
        backdropFilter: "blur(4px) brightness(0.5)",
        zIndex: 101,
    }}>
        <Typography
            sx={{
                position: "absolute",
                top: "40%",
                left: 0,
                right: 0,
                marginLeft: "auto",
                marginRight: "auto",
            }}
            textAlign={"center"}
            variant={"title"}>{customSentence}</Typography>
        <img
            style={{
                zIndex: 102,
                position: "absolute",
                top: customSentence && isMobile ? "60%" : "50%",
                left: 0,
                right: 0,
                marginLeft: "auto",
                marginRight: "auto",
            }}
            className={style.rotate}
            alt={"quix-logo"} src={QuixLogoMobile}/>
    </Stack>
}

export default LoadingPage