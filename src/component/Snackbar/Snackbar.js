import React, {useState} from "react";
import {Close} from "@mui/icons-material";
import {IconButton, Snackbar} from "@mui/material";
import MuiAlert from '@mui/material/Alert';
import style from "./SnackBar.module.css";
import {uuidGenerator} from "../../constants/Utility";


const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const useSnackbar = () => {

    let [show, setShow] = useState()
    let [props, setProps] = useState({
        open: false,
        vertical: "top",
        horizontal: "right",
        customKey: uuidGenerator()
    })
    let color;
    switch (props.type) {
        case "success":
            color = "#43A047";
            break;
        case "warning":
            color = "#FFA000";
            break;
        case "info":
            color = "#0659BD";
            break;
        case "error":
            color = "#D32F2F";
            break;
        case "notification":
            if (props.customColor) {
                color = props.customColor;
            }
            break;
        default:
            color = "#000000"
            break;
    }

    return [

        <Snackbar
            sx={{
                opacity: show ? 1 : 0
            }}
            autoHideDuration={props.autoHideDuration}
            key={props.customKey}
            className={style.snackBar}
            anchorOrigin={{vertical: props.vertical, horizontal: props.horizontal}}
            open={props.open}
            onClose={
                () => setShow(false)
            }
            type={props.type}
            message={props.message}
            // TransitionComponent={(props)=><Slide {...props} direction="left" />}
            action={[
                <IconButton
                    key={"snackBar"}
                    id="close"
                    aria-label="close"
                    onClick={() => setShow(false)}
                    size="large">
                    <Close className={style.icon}/>
                </IconButton>
            ]}
        >
            <Alert onClose={() => setShow(false)}
                   severity={props.type}
                   sx={{
                       width: '100%',
                       backgroundColor: color
                   }}
                   className={style.message}
            >
                {props.message}
            </Alert>
        </Snackbar>
        ,
        (vertical,
         horizontal,
         open,
         autoHideDuration,
         message,
         type,
         customColor, customKey
        ) => {
            setProps({
                open,
                vertical,
                horizontal,
                autoHideDuration,
                message,
                type,
                customColor,
                customKey
            })
            setShow(true)
        },
        () => {
            setProps({open: false})
            setShow(false)
        },


    ]
}

export default useSnackbar