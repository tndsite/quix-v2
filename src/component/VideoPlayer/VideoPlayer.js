import React, {useEffect, useState} from "react";
import Plyr from "plyr";
import {Collapse, Stack, Typography} from "@mui/material";
import {episodeSeen, getTimeLink, saveTimeLink} from "../../constants/StorageHelper";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";
import {getTypeVideo, isDeviceMobile} from "../../constants/Utility";
import "./VideoPlayer.css"

const VideoPlayer = (props) => {
    let {
        episodeProps,
        idToRender,
        topbarContent,
        store,
        disableTouchEvents,
        // disableUi
    } = props;
    let {plyrProps} = store
    let {player} = plyrProps
    let {
        id,
        seasonNumber,
        episodeNumber,
        selectedLink,
        selectedMagnet
    } = episodeProps
    let [display, setDisplay] = useState({display: "none"});
    let videoJsOptions = {
        autoplay: false,
        controls: true,
        "data-setup": '{}',
        preload: "auto",
        sources: [
            {
                src: selectedLink + "#t=0.5",
                type: getTypeVideo(selectedLink)
            },

        ]
    }
    let timeout;

    const onPause = async ({
                               id,
                               episodeNumber,
                               seasonNumber,
                               selectedLink,

                           }) => {
        let input = {
            idShow: id,
            tvShowId: id,
            episodeNumber,
            seasonNumber,
            link: selectedLink,
            time: player.get.currentTime
        }

        await saveTimeLink(input);
        if (player.get.currentTime > player.get.duration * 0.8) {
            await episodeSeen(input, true);
        }
    }

    const onPlay = async ({
                              id,
                              episodeNumber,
                              seasonNumber,
                              selectedLink,

                          }) => {
        try {
            let input = {
                idShow: id,
                tvShowId: id,
                episodeNumber,
                seasonNumber,
                link: selectedLink,
            }
            getTimeLink(input).then((x) => {
                if (x) {
                    console.info("MA COME WORKA",x)
                    player.get.currentTime = x;
                    player.setOldCurrentTime()
                }
            });
            //TODO capire perché va in errore
            // if (player.forceFullScreen || (isDeviceMobile() && !player.disableForcedFullScreen)) {
            // player.get.fullscreen.toggle()
            // if (isDeviceMobile()) {
            //     await window.screen.orientation.lock("landscape")
            // }
            // }
        } catch (e) {
            console.error("onPlay error", e)
        }
    }

    useEffect(() => {
        (() => {
            player.setIsControlsShowed(true)
            player.set(
                new Plyr(idToRender
                    , {
                        controls: [
                            'play-large', // The large play button in the center
                            'restart', // Restart playback
                            'rewind', // Rewind by the seek time (default 10 seconds)
                            'play', // Play/pause playback
                            'fast-forward', // Fast forward by the seek time (default 10 seconds)
                            'progress', // The progress bar and scrubber for playback and buffering
                            'current-time', // The current time of playback
                            'duration', // The full duration of the media
                            'mute', // Toggle mute
                            'volume', // Volume control
                            'captions', // Toggle captions
                            'settings', // Settings menu
                            'pip', // Picture-in-picture (currently Safari only)
                            'airplay', // Airplay (currently Safari only)
                            // 'download', // Show a download button with a link to either the current source or a custom URL you specify in your options
                            'fullscreen', // Toggle fullscreen
                        ]
                    }))

            player.get.on("timeupdate", async () => {
                let input = {
                    idShow: id,
                    tvShowId: id,
                    episodeNumber,
                    seasonNumber,
                    link: selectedLink,
                    time: player.get.currentTime
                }


                if (player.get.currentTime > player.oldCurrentTime + 3) {
                    if (
                        player.get.currentTime !== 0 &&
                        player.get.currentTime !== 0.5
                    ) {
                        player.setOldCurrentTime()
                        await saveTimeLink(input);
                    }
                }

                if (player.get.currentTime > player.get.duration * 0.8) {
                    await episodeSeen(input, true);
                }

            })

            player.get.on("controlshidden", () => {
                player.setIsControlsShowed(false)
            })
            player.get.on("loadeddata", async() => {
                    await onPlay({
                        id,
                        episodeNumber,
                        seasonNumber,
                        selectedLink
                    })
            })
            player.get.on("controlsshown", () => {
                player.setIsControlsShowed(true)
            })
            // player.get.once("play",
            //     async () => {
            //         await onPlay({
            //             id,
            //             episodeNumber,
            //             seasonNumber,
            //             selectedLink
            //         })
            //     }
            // )
            player.get.on("pause",
                async () => {
                    await onPause({
                        id,
                        episodeNumber,
                        seasonNumber,
                        selectedLink
                    })
                }
            )


            if (!selectedMagnet) {
                setDisplay({display: "block"})
                player.get.source = {
                    type: 'video',
                    title: 'Example title',
                    sources: videoJsOptions.sources
                }
            } else {
                // eslint-disable-next-line
                selectedLink = selectedMagnet
            }

        })()
        return () => {
        }
    }, [])


    useEffect(() => {
        player.get.once("play",
            async () => {
                await onPlay({
                    id,
                    episodeNumber,
                    seasonNumber,
                    selectedLink
                })
            }
        )
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedLink])


    const togglePlayPause = async () => {
        if (!player.lockPlayPause) {
            if (player.get.paused) {
                player.setLockPlayPause(true)
                await player.get.play();
                player.setHideUIonPlay(false)
                player.setLockPlayPause(false)
            } else {
                player.setLockPlayPause(true)
                await player.get.pause();
                player.setHideUIonPlay(true)
                player.setLockPlayPause(false)
            }
        }
    }

    const onTouchEnd = async (event) => {
        let currentTime = new Date().getTime();
        let tapLength = currentTime - player.lastTap;
        clearTimeout(timeout);
        if (tapLength < 350 && tapLength > 0 && !player.touch2) {
            //Foward/Backward
            event.preventDefault();

            if (player.touch.clientX >= window.innerWidth / 2 && player.touch.clientY >= window.innerHeight / 2) {
                player.get.currentTime = player.get.currentTime + 10;
            } else if (player.touch.clientX < window.innerWidth / 2 && player.touch.clientY >= window.innerHeight / 2) {
                player.get.currentTime = player.get.currentTime - 10;
            } else if (player.touch.clientX >= window.innerWidth / 2 && player.touch.clientY < window.innerHeight / 2) {
                player.get.currentTime = player.get.currentTime + 80;
            } else if (player.touch.clientX < window.innerWidth / 2 && player.touch.clientY < window.innerHeight / 2) {
                player.get.currentTime = player.get.currentTime - 80;
            }
        } else {
            timeout = setTimeout(async () => {
                if ((isDeviceMobile() && player.touch2) || !isDeviceMobile()) {
                    await togglePlayPause()
                }
                //Play/Pause
                clearTimeout(timeout);
            }, 350);
        }
        player.setLastTap(currentTime);

    }


    const onTouchStart = (e) => {
        if (e.touches && e.touches[0]) {
            console.info("sono un player.touch ", e.touches && e.touches[0])
            player.setTouch(e)
        }
    }

    const renderUI = () => {
        return player.hideUIonPlay &&
            <div key={"videoUIcustom"} style={{zIndex: 1, position: "absolute"}}
                // onTouchStart={onTouchStart}
                // onTouchEnd={onTouchEnd}
            >

                <div style={{
                    opacity: 0.5,
                    position: "fixed",
                    height: "50%",
                    width: "50%",
                    borderBottom: "2px dotted",
                    borderRight: "2px dotted",
                    borderColor: "var(--second-color)",
                    top: 0,
                    left: 0,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "20px",
                    background: "rgba(0,0,0,0.5)"

                }}>
                    <Typography variant={"cardTitle"}
                                style={{}}>
                        Double tap here to skip 80s backward
                    </Typography>
                </div>
                <div style={{
                    opacity: 0.5,
                    position: "fixed",
                    height: "50%",
                    width: "50%",
                    borderBottom: "2px dotted",
                    borderLeft: "2px dotted",
                    borderColor: "var(--second-color)",
                    top: 0,
                    left: "50%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "20px",
                    background: "rgba(0,0,0,0.5)"
                }}>
                    <Typography variant={"cardTitle"}>
                        Double tap here to skip 80s forward
                    </Typography>
                </div>
                <div style={{
                    opacity: 0.5,
                    position: "fixed",
                    height: "50%",
                    width: "50%",
                    borderTop: "2px dotted",
                    borderRight: "2px dotted",
                    borderColor: "var(--second-color)",
                    top: "50%",
                    left: 0,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "20px",
                    background: "rgba(0,0,0,0.5)"
                }}>
                    <Typography variant={"cardTitle"}>
                        Double tap here to skip 10s backward
                    </Typography>
                </div>
                <div style={{
                    opacity: 0.5,
                    position: "fixed",
                    height: "50%",
                    width: "50%",
                    borderTop: "2px dotted",
                    borderLeft: "2px dotted",
                    borderColor: "var(--second-color)",
                    top: "50%",
                    left: "50%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    padding: "20px",
                    background: "rgba(0,0,0,0.5)"
                }}>
                    <Typography variant={"cardTitle"}>
                        Double tap here to skip 10s forward
                    </Typography>
                </div>

            </div>
    }

    return (<>
            <Collapse
                easing={{enter: "ease-in-out", exit: "ease-in-out"}}
                in={player.isControlsShowed}
                onMouseEnter={() => {
                    player.setIsControlsShowed(true)
                }}
                onMouseLeave={() => {
                    player.setIsControlsShowed(true)
                }}
                sx={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    marginTop: "0 !important",
                    width: "100%",
                    background: "rgba(0,0,0,.5)",
                    zIndex: 2
                }}
            >
                {topbarContent}
            </Collapse>
            <Stack
                width={"100%"}
                sx={display}
                id={"videoContainer"}
                onTouchStart={!disableTouchEvents ? onTouchStart : () => {
                }}
                onTouchEnd={!disableTouchEvents ? onTouchEnd : () => {
                }}
            >
                {!player.disableTutorialMobile &&
                    isDeviceMobile() &&
                    renderUI()}
                <style>
                    {".plyr {" +
                        "position: fixed !important;" +
                        "top: 0 !important;" +
                        "bottom: 0 !important;" +
                        "right: 0 !important;" +
                        "left: 0 !important;" +
                        "width: 100% !important;" +
                        "height: 100% !important;" +
                        "max-width: initial !important;" +
                        "max-height: initial !important;" +
                        "}"}
                </style>
                <video

                    controls
                    style={{
                        width: "100%", flex: 1, height: "100%",

                    }}
                    className="player"
                    id={idToRender}

                />
            </Stack></>
    )


}


export default withCommonPropsLite(VideoPlayer)