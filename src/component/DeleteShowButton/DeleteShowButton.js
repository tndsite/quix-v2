import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    IconButton,
    MenuItem,
    Radio,
    RadioGroup,
    Stack,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {Close, DeleteForever} from "@mui/icons-material";
import {deleteSerie, deleteSeriesAndLink} from "../../constants/StorageHelper";
import React, {useState} from "react";
import {observer} from "mobx-react";

const DeleteShowButton = (props) => {
    let [openDialog, setOpenDialog] = useState(false);
    let store = window.TNDglobalStoreMobx
    let {showDetail, snackbarHandler} = store
    let {
        refreshData,
        resetDrawerStateLocal,
        show,
        seasonSelected,
    } = props

    return <>
        <Dialog onClose={() => setOpenDialog(false)}
                open={openDialog} PaperProps={{sx: {overflow: "hidden", maxWidth: "100%"}}}>
            <DialogTitle><Stack direction={"row"} justifyContent={"space-between"}
                                alignItems={"center"}><Typography
                variant={"title"}>Delete show and/or episodes link</Typography>

                <IconButton
                    onClick={() => setOpenDialog(false)}>
                    <Close/>
                </IconButton>
            </Stack></DialogTitle>
            <DialogContent>
                <FormControl>
                    <Typography variant={"semiTitle"}>Oh so you want to delete
                        something :c</Typography>
                    <RadioGroup
                        onChange={(event) => {
                            let value = event.target.value
                            showDetail.deleteSelectedOption.set(value)
                        }}
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={showDetail.deleteSelectedOption.get}
                        name="radio-buttons-group"
                    >
                        <FormControlLabel value="show" control={<Radio/>}
                                          label="Delete the show but keep the episodes"/>
                        <FormControlLabel value="showAndEpisodes" control={<Radio/>}
                                          label="Delete both the show and all the episodes from all the seasons"/>
                        <FormControlLabel value="episodesFromSelectedSeason"
                                          control={<Radio/>}
                                          label={"Delete all the links from the selected season (" + showDetail.deleteSelectedOption.season + ") but keep the show"}/>
                        <FormControlLabel value="episodesFromSelectedSeasonAndShow"
                                          control={<Radio/>}
                                          label={"Delete all the links from the selected season (" + showDetail.deleteSelectedOption.season + ") and the show"}/>
                    </RadioGroup>
                </FormControl>

            </DialogContent>
            <DialogActions>
                <Stack direction={"row"} justifyContent={"space-between"} width={"100%"} alignItems={"center"}>
                    <TextField
                        sx={{marginTop: "10px", width: "100px"}}
                        label={"Season"}
                        id="selectSeason"
                        disabled={showDetail.deleteSelectedOption.get === "show" || showDetail.deleteSelectedOption.get === "showAndEpisodes"}
                        defaultValue={showDetail.deleteSelectedOption.season}
                        onChange={(event) => {
                            let s = event.target.value
                            showDetail.deleteSelectedOption.setSeason(s)
                        }}
                        required
                        select
                        color={"primary"}
                    >
                        {show.seasons && show.seasons.map((s, sIndex) => {
                            return <MenuItem
                                value={s.season_number}
                                key={"seasonNumber" + sIndex}>
                                {s.season_number}
                            </MenuItem>

                        })}
                    </TextField>
                    <Button variant={"contained"} color={"secondary"}
                            onClick={async (e) => {
                                e.preventDefault();
                                switch (showDetail.deleteSelectedOption.get) {
                                    case "show":
                                    default:
                                        await deleteSerie(show.id)
                                        break
                                    case "showAndEpisodes":
                                        for (let sNumber in show.seasons) {
                                            let s = show.seasons[sNumber].season_number
                                            await deleteSeriesAndLink(show.id, s)
                                        }
                                        await deleteSerie(show.id)
                                        break
                                    case "episodesFromSelectedSeason":
                                        await deleteSeriesAndLink(show.id, seasonSelected)
                                        break
                                    case "episodesFromSelectedSeasonAndShow":
                                        await deleteSeriesAndLink(show.id, seasonSelected, true)
                                        break
                                }

                                await resetDrawerStateLocal()
                                showDetail.deleteSelectedOption.set("show")
                                setOpenDialog(false)
                                refreshData()
                                snackbarHandler.set(
                                    "top",
                                    "right",
                                    true,
                                    3000,
                                    "Deleted with success",
                                    "success",
                                );
                            }}>Delete</Button>
                </Stack>
            </DialogActions>
        </Dialog>
        <Tooltip title={"Delete show and/or episodes link"}>
            <IconButton onClick={() => {
                showDetail.deleteSelectedOption.setSeason(seasonSelected)
                setOpenDialog(true)
            }}>
                <DeleteForever fontSize={"large"}/>
            </IconButton>
        </Tooltip>
    </>

}

export default observer(DeleteShowButton)