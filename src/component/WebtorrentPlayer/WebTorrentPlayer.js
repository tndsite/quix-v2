import WebTorrent from "webtorrent";
import React, {useEffect, useState} from "react";
import {Button, Chip, Container, DialogContent, Grid, Stack, Typography, useMediaQuery} from "@mui/material";
import {ContentCopy, Download, DownloadForOffline, Favorite, Share, SmartDisplay, Upload} from "@mui/icons-material";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";
import {copyToClipboard, uuidGenerator} from "../../constants/Utility";
import '../VideoPlayer/VideoPlayer.css'
import {observer} from "mobx-react";
import {useAppStore} from "../MobxContext/AppContext";
import {SimpleDialog} from "../SimpleDialog/SimpleDialog";
import CF_Logo from "../../asset/image/CF_logo_only.svg"
import {TypographyEllipsed} from "../TypographyEllipsed";
import VideoPlayer from "../VideoPlayer/VideoPlayer";


const WebTorrentPlayer = ((props) => {
    let store = useAppStore()

    let [loader, stopLoading] = useLoadingSystem(true, null, null, "Our hamsters army is preparing your streaming, please wait they are doing their best")
    let {episodeProps} = props
    let {selectedMagnet, selectedLink} = episodeProps
    let [
        // errorMessage
        , setErrorMessage] = useState()
    let [openFilePicker, setOpenFilePicker] = useState(false)
    let [fallbackElement, setFallbackElement] = useState()
    let [readyToPlay, setReadyToPlay] = useState(false)
    let {webTorrentProps,snackbarHandler} = store

    useEffect(() => {

        (() => {
            webTorrentProps.client.set(new WebTorrent({tracker: trackerOpts}))
            navigator.serviceWorker
                .register("sw.min.js", {scope: "/"})
                .then((reg) => {
                    console.log("register the worker");
                    const worker = reg.active || reg.waiting || reg.installing;

                    function checkState(worker) {
                        return (
                            worker.state === "activated" && webTorrentProps.client.get.loadWorker(worker, play)
                        );
                    }

                    if (!checkState(worker)) {
                        worker.addEventListener("statechange", ({target}) =>
                            checkState(target)
                        );
                    }
                })
                .catch(console.error);

            webTorrentProps.client.get.on("warning", (warning) => {
                console.warn("Error torrent: ", warning)
            });
            webTorrentProps.client.get.on("error", (error) => {
                console.error("Error validating torrent: ", error)
                setErrorMessage("Invalid torrent/magnet/hash")
                handleFallback()
                stopLoading()
            })
            // play()

        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleFallback = () => {
        if (
            selectedLink.endsWith('.mp4') ||
            selectedLink.endsWith(".3g2") ||
            selectedLink.endsWith(".m4v") ||
            selectedLink.endsWith(".mov") ||
            selectedLink.endsWith(".ogm") ||
            selectedLink.endsWith(".ogv") ||
            selectedLink.endsWith(".webm")

        ) {

            setFallbackElement(<><VideoPlayer
                    key={uuidGenerator()}
                    idToRender={"video"}
                    episodeProps={{...episodeProps, selectedMagnet: false}}
                />
                    <TopbarPlayer
                        isFallback={true}
                    />
                </>
            )
            setReadyToPlay(true)
        } else {
            setFallbackElement(<iframe
                src={selectedLink} style={{
                height: "100%",
                width: "100%",
                border: "none",
                marginTop: 0
            }}
                title={"fallback"}
                allowFullScreen/>)
        }
    }

    const play = () => {
        console.log("worker registered, going to get the file from the torrent");
        let video = document.getElementById("video");
        setTimeout(() => {
            if ((video && !video.src) && selectedLink) {
                handleFallback()
                stopLoading()
                if (webTorrentProps.client.get.destroyed) {
                    webTorrentProps.client.get.destroy()//remove(selectedMagnet);
                }
            }
        }, 20000)
        webTorrentProps.client.get.add(selectedMagnet, torrentOpts, (torrent) => {
            // Torrents can contain many files. Let's use the .mp4 file
            webTorrentProps.torrent.set(torrent)
            let filesTmp = torrent.files.filter(function (file) {
                return (
                    file.name.endsWith(".mp4") ||
                    file.name.endsWith(".3g2") ||
                    file.name.endsWith(".m4v") ||
                    file.name.endsWith(".mov") ||
                    file.name.endsWith(".ogm") ||
                    file.name.endsWith(".ogv") ||
                    file.name.endsWith(".webm")

                );
            });
            // filesTmp = [...filesTmp, ...filesTmp]
            webTorrentProps.filesCanBeStreamed.set(filesTmp)
            //TODO levare
            console.info("file ready", torrent, filesTmp);

            torrent.on('download', (bytes) => {
                webTorrentProps.progress.set(torrent.progress)
                webTorrentProps.download.set(humanFileSize(torrent.downloadSpeed))
            })
            torrent.on('upload', (bytes) => {
                webTorrentProps.upload.set(humanFileSize(torrent.uploadSpeed))
            })
            if (filesTmp.length === 1) {
                webTorrentProps.selectedFile.set(filesTmp[0])
                filesTmp[0].on("stream", ({stream, file, req}) => {
                    console.info("stream on going", stream, file, req);
                });
                filesTmp[0].streamTo(video, (err) => {                    let videoContainer = document.getElementById("videoContainer");
                    videoContainer.style.display = "block"
                    if (err) {
                        setErrorMessage("Error reproducing file " + filesTmp[0].name)
                        stopLoading()
                    } else {
                        setReadyToPlay(true)
                        stopLoading()
                    }
                });
            } else {
                setOpenFilePicker(true)
            }


        });
    };

//TODO gestire eventuali errori
    //far scegliere il file da riprodurre
    return (
        fallbackElement ? fallbackElement : <Container maxWidth="false" sx={{background: "#303030"}}>
            {loader}
            <SimpleDialog
                isDialogOpen={openFilePicker}
                dialogTitle={webTorrentProps.filesCanBeStreamed.get && webTorrentProps.filesCanBeStreamed.get.length === 0 ?
                    <Stack width={"100%"}
                           alignItems={"center"}
                    >
                        <Typography variant={"small-text"} textAlign={"center"}>
                            Non sono stati trovati file supportati per lo streaming :C
                            <br/>
                            <br/>
                            Ma non disperarti, installa il nostro client
                        </Typography>
                        <Stack direction={"row"} width={"100%"} justifyContent={"center"} alignItems={"center"}>
                            <Typography variant={"small-text"}
                                        onClick={() => {
                                            window.open("https://github.com/drakonkat/Crawfish/releases", '_blank').focus();
                                        }}
                                        sx={{
                                            cursor: "pointer",
                                            color: "primary.main",
                                            textAlign: "center !important",
                                            marginLeft: "5px"
                                        }}>
                                Crawfish
                            </Typography>
                            <Button
                                color={"primary"}
                                onClick={() => {
                                    window.open("https://github.com/drakonkat/Crawfish/releases", '_blank').focus();
                                }}
                                variant={"contained"}
                                sx={{
                                    minWidth: "45px",
                                    padding: 0,
                                    marginLeft: "5px"
                                }}
                            >
                                <img src={CF_Logo} style={{width: "30px"}} alt={"cf-logo"}/>
                            </Button>
                        </Stack>
                        <Stack direction={"row"} width={"100%"} justifyContent={"center"} alignItems={"center"}>
                            <Typography variant={"small-text"}>
                                e scarica questo
                            </Typography>
                            <Typography variant={"small-text"}
                                        onClick={async () => {
                                            await copyToClipboard(selectedMagnet)
                                            snackbarHandler.set(
                                                "top",
                                                "right",
                                                true,
                                                3000,
                                                "Magnet copied in the clipboard!",
                                                "success",
                                            );
                                        }}
                                        sx={{
                                            cursor: "pointer",
                                            color: "primary.main",
                                            textAlign: "center !important",
                                            marginLeft: "5px"
                                        }}>
                                Torrent
                            </Typography>
                            !
                            <Button
                                color={"primary"}
                                onClick={async () => {
                                    await copyToClipboard(selectedMagnet)
                                    snackbarHandler.set(
                                        "top",
                                        "right",
                                        true,
                                        3000,
                                        "Magnet copied in the clipboard!",
                                        "success",
                                    );
                                }}
                                variant={"contained"}
                                sx={{
                                    marginLeft: "5px"
                                }}
                            >
                                <ContentCopy/>
                            </Button>
                        </Stack>

                    </Stack>
                    :
                    <Stack>
                        <Typography variant={"small-text"} sx={{
                            textAlign: "center !important"
                        }}>
                            Scegli quale file riprodurre,
                        </Typography>
                        <Stack direction={"row"} width={"100%"} justifyContent={"center"} alignItems={"center"}>
                            <Typography variant={"small-text"} sx={{
                                textAlign: "center !important"
                            }}>
                                oppure scaricalo con
                            </Typography>
                            <Typography variant={"small-text"}
                                        onClick={() => {
                                            window.open("https://github.com/drakonkat/Crawfish/releases", '_blank').focus();
                                        }}
                                        sx={{
                                            cursor: "pointer",
                                            color: "primary.main",
                                            textAlign: "center !important",
                                            marginLeft: "5px"
                                        }}>
                                Crawfish
                            </Typography>
                            <Button
                                color={"primary"}
                                onClick={() => {
                                    window.open("https://github.com/drakonkat/Crawfish/releases", '_blank').focus();
                                }}
                                variant={"contained"}
                                sx={{
                                    minWidth: "45px",
                                    padding: 0,
                                    marginLeft: "5px"
                                }}
                            >
                                <img src={CF_Logo} style={{width: "30px"}} alt={"cf-logo"}/>
                            </Button>
                        </Stack>
                    </Stack>}
                dialogContent={
                    <DialogContent>

                        {webTorrentProps.filesCanBeStreamed.get && webTorrentProps.filesCanBeStreamed.get.map(file => {
                            let title = file.name
                            if (
                                title.length > 15
                            ) {
                                title = file.name.substring(0, 10) +
                                    "..." +
                                    file.name.slice(-5)

                            }
                            return <Stack
                                id={file.name}
                                key={file.name + uuidGenerator()}
                                sx={{
                                    padding: "5px",
                                    justifyContent: "space-between",
                                    flexDirection: "row",
                                    alignItems: "center"
                                }}>
                                <TypographyEllipsed fontWeight={300} variant={"small-text"} tooltip={file.name}>
                                    {title}
                                </TypographyEllipsed>
                                <Button size={"small"}
                                        sx={{marginLeft: "5px"}}
                                        onClick={() => {
                                            let video = document.getElementById("video");
                                            webTorrentProps.selectedFile.set(file)
                                            file.on("stream", ({stream, fileStream, req}) => {
                                                console.info("stream on going", stream, fileStream, req, file);
                                            });

                                            file.streamTo(video, (err) => {
                                                if (err) {
                                                    setErrorMessage("Error reproducing file " + file.name)
                                                    stopLoading()
                                                } else {
                                                    setReadyToPlay(true)
                                                    stopLoading()
                                                }
                                            })
                                            setOpenFilePicker(false)
                                        }}
                                        variant={"contained"} color={"primary"}
                                >
                                    <SmartDisplay/>
                                </Button>
                            </Stack>
                        })}
                    </DialogContent>
                }


            />
            <Stack
                sx={{
                    height: "100%"
                }}
                key={"FIRST-ELEMENT"} justifyContent={"center"} spacing={2}>
                <Stack
                    sx={{
                        height: "100%"
                    }} key={"THIRD-ELEMENT"} alignItems={"center"} spacing={2}>
                    <VideoPlayer
                        idToRender={"video"}
                        episodeProps={{...episodeProps}}
                        topbarContent={readyToPlay && <TopbarPlayer/>}
                    />

                </Stack>
            </Stack>
        </Container>

    );

})

export default WebTorrentPlayer


const TopbarPlayer = observer((props) => {

    let {isFallback} = props
    // let store = useAppStore()
    // let {plyrProps} = store
    // let {player} = plyrProps
    let isMobile = useMediaQuery('(max-width:868px)');

    return <Grid container
                 justifyContent={isFallback ? "flex-end" : "space-between"}
                 alignItems="center"

    >
        {!isFallback && <DownloadButtonStats/>}
        {!isFallback && <UploadDownloadStats/>}
        <Grid
            xs={4}
            id={"Star github"} item sx={{
            padding: isFallback ? "25px 10px 25px 0px" : "5px",
            display: "flex",
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "center"
        }}>
            <Chip
                variant={"filled"}
                icon={<Favorite/>} color={"primary"} onClick={() => {
                window.open("https://github.com/drakonkat/Crawfish", '_blank').focus();
            }}
                label={
                    isMobile ? "CrawFish" : "Se ti piace il player torrent, segui CrawFish"
                }
            />
        </Grid>
    </Grid>
})

const UploadDownloadStats = observer(() => {
    let store = useAppStore()
    let {webTorrentProps} = store

    return <Grid id={"Speed"}
                 xs={4}
                 item sx={{
        padding: "5px",
        display: "flex",
        justifyContent: "center",
        flexDirection: "row",
        alignItems: "center"
    }}>
        <Stack sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            marginRight: "50px",
            width: "50px"
        }}>
            <Download/>
            <Typography textAlign={"left"}>{webTorrentProps.download.getSpeed}</Typography>
            <Typography variant={"smallText"}>{webTorrentProps.download.getUnit}</Typography>
        </Stack>
        <Stack sx={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            width: "50px"
        }}>
            <Upload/>
            <Typography textAlign={"left"}>{webTorrentProps.upload.getSpeed}</Typography>
            <Typography variant={"smallText"}>{webTorrentProps.upload.getUnit}</Typography>
        </Stack>
    </Grid>
})


const round = (input) => {
    return Math.round(input * 100) / 100
}
const humanFileSize = (bytes, si = false, dp = 1) => {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
        return {
            speed: bytes,
            unit: "B"
        }

    }

    const units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10 ** dp;

    do {
        bytes /= thresh;
        ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


    return {
        speed: round(bytes.toFixed(dp)),
        unit: units[u]
    };
}
const trackers = ['wss://tracker.crawfish.cf']

const rtcConfig = {
    "iceServers": [
        {
            "urls": "stun:23.21.150.121"
        },
        {
            urls: [
                'stun:stun.l.google.com:19302',
                'stun:global.stun.twilio.com:3478'
            ]
        },
        {
            "username": "admin",
            "credential": "Password1!",
            "urls": "turn:185.149.22.163:3478"
        },
        {
            "username": "admin",
            "credential": "Password1!",
            "urls": "turn:23.94.202.235:3478"
        }
    ]
}

const torrentOpts = {
    announce: trackers
}

const trackerOpts = {
    announce: trackers,
    rtcConfig: rtcConfig
}


const DownloadButtonStats = observer(() => {
    let store = useAppStore()
    let {webTorrentProps,snackbarHandler} = store
    let isMobile = useMediaQuery('(max-width:868px)');
    let title = webTorrentProps.selectedFile.get.name
  
    return webTorrentProps.selectedFile.get &&
        <Grid
            xs={4}
            id={webTorrentProps.selectedFile.get.name}
            key={webTorrentProps.selectedFile.get.name} item
            sx={{
                padding: "5px",
                display: "flex",
                justifyContent: "flex-start",
                flexDirection: "row",
                alignItems: "center"
            }}>
            <Stack direction={"row"} alignItems={"flex-start"}
                   width={"100%"}
                   sx={{cursor: "pointer", padding: "10px"}}
                   onClick={async () => {
                       await copyToClipboard(window.location.href
                       )
                       snackbarHandler.set(
                           "top",
                           "right",
                           true,
                           3000,
                           "Magnet copied in the clipboard!",
                           "success",
                       );
                   }}>
                <Share/>
                {!isMobile && <TypographyEllipsed
                    fontWeight={300}
                    variant={"small-text"}
                >
                    { title}
                </TypographyEllipsed>}
            </Stack>

            <Button disabled={webTorrentProps.progress.get < 100} size={"small"}
                    sx={{minWidth: "100px"}}
                    onClick={() => {
                        webTorrentProps.selectedFile.get.getBlobURL((err, url) => {
                            if (err) {
                                console.error("Error downloading file: ", err)
                            }
                            let link = document.createElement("a");
                            link.href = url;
                            link.download = webTorrentProps.selectedFile.get.name;
                            document.body.appendChild(link);
                            link.dispatchEvent(
                                new MouseEvent('click', {
                                    bubbles: true,
                                    cancelable: true,
                                    view: window
                                })
                            );
                            document.body.removeChild(link);
                        })
                    }} variant={"contained"} color={"primary"}
                    endIcon={<DownloadForOffline/>}>
                {/*({Math.round(webTorrentProps.progress.get * 100)}%)*/}
                ({webTorrentProps.progress.get}%)
            </Button>
        </Grid>
})