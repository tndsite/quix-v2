import {getConfLocal} from "../../constants/DbLocal";
import {commonHandledRequest, uuidGenerator} from "../../constants/Utility";
import translationEN from "../../translation/translationEN.json"
import translationIT from "../../translation/translationIT.json"
import {createTheme} from "@mui/material";

export const createAppStore = () => {


    return {
        isDisconnected:
            {
                get: false,
                set(data) {
                    this.get = data;
                },
            },
        snackbarHandler:
            {
                get: {
                    open: false,
                    vertical: "top",
                    horizontal: "right",
                    autoHideDuration: null,
                    message: "",
                    type: "warning",
                    customColor: null,
                    customKey: null
                },
                success(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "success"
                    };
                },
                warning(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "warning"
                    };
                },
                error(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "error"
                    };
                }, info(message = "", autoHideDuration = 2000) {
                    this.get = {
                        vertical: "top",
                        horizontal: "right",
                        open: true,
                        autoHideDuration,
                        message,
                        type: "info"
                    };
                },
                set(vertical = "top",
                    horizontal = "right",
                    open = false,
                    autoHideDuration = null,
                    message = "",
                    type = "warning",
                    customColor = null,
                    customKey = null
                ) {
                    this.get = {
                        vertical,
                        horizontal,
                        open,
                        autoHideDuration,
                        message,
                        type,
                        customColor,
                        customKey
                    };
                },
                reset() {
                    this.get.open = false
                },
            },
        language: {
            get: localStorage.getItem("websiteLanguage") || "en",
            temp: localStorage.getItem("websiteLanguage") || "en",
            locales: [],
            list: {
                en: JSON.parse(localStorage.getItem("translations_en")) || translationEN,
                it: JSON.parse(localStorage.getItem("translations_it")) || translationIT,
            },
            set(data) {
                this.get = data
            },
            setLocales(data) {
                this.locales = data
            },
            setList(data) {

                for (let i in data) {
                    let locale = data[i]
                    for (let x in locale) {
                        if (locale[x] === "" || !locale[x]) {
                        } else {
                            this.list[i][x] = locale[x]
                        }
                    }

                    localStorage.setItem("translations_" + i, JSON.stringify(this.list[i]))


                }
            },
            setTemp(data) {
                this.temp = data
            }
        },
        theme: {
            get: createTheme(options),
            options: options,
            getDefaultTheme() {
                return createTheme(options)
            },
            set() {

                let palette, overrides
                switch (this.styleTmp) {
                    case "light":
                        palette = paletteLight
                        overrides = overridesLight
                        break
                    case "dark":
                    default:
                        palette = paletteDark
                        overrides = overridesDark
                        break
                }
                this.get = createTheme({
                    ...this.options,
                    components: {...this.options.components, ...overrides},
                    palette: palette
                })
            },
            style: localStorage.getItem("theme") || "dark",
            setStyle(data) {
                this.style = data
            },
            styleTmp: localStorage.getItem("theme") || "dark",
            setStyleTmp(data) {
                this.styleTmp = data
            }
        },
        loading: {
            get: true,
            set(data) {
                if (data !== this.get) {
                console.debug("loading.set",data)
                    this.get = data;
                }
            },
        },
        localLoading: {
            get: true,
            set(data) {
                if (data !== this.get) {
                    this.get = data;
                }
            },
        },
        searchedValue:
            {
                get: "",
                set(data) {
                    this.get = data;
                },
            },
        accessToken: {
            get: null,
            set(data) {
                this.get = data;
            },
        }, profileData: {
            get: {},
            set(data) {
                this.get = data;
            },
            reset() {
                this.get = {}
            }
        }, toSync: {
            get: false,
            set(data) {
                this.get = data;
            },
        },
        dialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },
        episodeDialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },
        descriptionDialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },
        changelogDialogHandler:
            {
                get: false,
                set(data) {
                    this.get = data;
                }
            },
        showDialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },
        smallDialogHandler:
            {
                get: {open: false},
                show(isOpen) {
                    this.get.open = isOpen;
                },
                set(data) {
                    this.get = data;
                    this.get.key = uuidGenerator();
                },
                reset() {
                    this.get.open = false
                },
            },
        discoverSectionOpen:
            {
                get: getConfLocal("discoverSectionOpen"),
                set(data) {
                    this.get = data;
                }
            },
        myItemsSectionOpen:
            {
                get: getConfLocal("myItemsSectionOpen"),
                set(data) {
                    this.get = data;
                }
            },
        keepWatchingSectionOpen:
            {
                get: getConfLocal("keepWatchingSectionOpen"),
                set(data) {
                    this.get = data;
                }
            },
        items:
            {
                get: [],
                set(data) {
                    this.get = data;
                }
            },
        keepWatchingItems:
            {
                get: [],
                refresh: true,
                setRefresh(data) {
                    this.refresh = data;
                },
                set(data) {
                    this.get = data;
                }
            },
        searchedItems:
            {
                get: [],
                set(data) {
                    this.get = data;
                }
            },
        myItems:
            {
                get: [],
                refresh: true,
                setRefresh(data) {
                    this.refresh = data;
                },
                set(data) {
                    this.get = data;
                }
            },
        searchedMyItems:
            {
                get: [],
                set(data) {
                    this.get = data;
                }
            },
        checkFullHome:
            {
                get: true,
                set(data) {
                    this.get = data;
                }
            },
        checkBadge:
            {
                get: false,
                set(data) {
                    this.get = data;
                }
            },

        ...plyrProps(),
        ...showDetail(),
        ...webTorrentProps(),
        ...discoverItems(),
        ...images(),
        ...loadingImages()
    }
}
const loadingImages = () => {

    return {
        loadingImages: {
            set(imagePath,loading){
                this[imagePath]=loading
            }
        }
    }
}
const images = () => {

    return {
        images: {
            set(imagePath,image){
                this[imagePath]=image
            }
        }
    }
}

const discoverItems = () => {

    return {
        discoverItems: {
            get: [],
            push(data) {
                let t = this.get
                data.forEach(x=>{
                    if(!this.get.some(y=>y.id===x.id)){
                        this.get.push(x)
                    }
                })
                // this.get = [...t, ...data]
            },
            set(data) {
                this.get = data
            },
            providerSelected: 8,
            setProviderSelected(data) {
                this.providerSelected = data
            },
            oldProviderSelected: 8,
            setOldProviderSelected(data) {
                this.oldProviderSelected = data
            },
            providerList: [],
            setProviderList(data) {
                this.providerList = data
            },
            countrySelected: "US",
            setCountrySelected(data) {
                this.countrySelected = data
            },
            oldCountrySelected: "US",
            setOldCountrySelected(data) {
                this.oldCountrySelected = data
            },
            countryList: [],
            setCountryList(data) {
                this.countryList = data
            },
            page: 1,
            setPage(data) {
                this.page = data
            },
            oldPage: 1,
            setOldPage(data) {
                this.oldPage = data
            },
            totalPage: 100,
            setTotalPage(data) {
                this.totalPage = data
            }
        }
    }
}

const showDetail = () => {
    return {
        showDetail: {
            redirectToSeason: {
                redirect: false,
                setRedirect(data) {
                    this.redirect = data;
                },
                get: 0,
                set(data) {
                    this.get = data;
                },
            },
            shareSelectedOption: {
                season: 0,
                setSeason(data) {
                    this.season = data;
                },
                get: "show",
                set(data) {
                    this.get = data;
                },
            },
            deleteSelectedOption: {
                season: 0,
                setSeason(data) {
                    this.season = data;
                },
                get: "show",
                set(data) {
                    this.get = data;
                },
            },
            selectedSeasonToInsertLink: {
                get: 0,
                set(data) {
                    this.get = data;
                },
            },
            needHelp: {
                get: false,
                set(data) {
                    this.get = data;
                },
            },
            isSelectedLinkPart: {
                get: false,
                set(data) {
                    this.get = data;
                },
            }

        },
    }
}

const plyrProps = () => {
    return {
        plyrProps: {
            player:
                {
                    get: false,
                    set(data) {
                        this.get = data;
                    },
                    hideUIonPlay: true,
                    setHideUIonPlay(data) {
                        this.hideUIonPlay = data;
                    },
                    selectedEpisodeLink: null,
                    setSelectedEpisodeLink(data) {
                        this.selectedEpisodeLink = data
                    },
                    selectedEpisodeIndex: 1,
                    setSelectedEpisodeIndex(data) {
                        this.selectedEpisodeIndex = data
                    },
                    episodeList: {},
                    setEpisodeList(data) {
                        this.episodeList[data.episode_number] = data
                    },
                    resetEpisodeList() {
                        this.episodeList = {}
                    },
                    forceFullScreen: true,
                    setForceFullScreen(data) {
                        this.forceFullScreen = data;
                    },
                    disableForcedFullScreen: false,
                    setDisableForcedFullScreen(data) {
                        this.disableForcedFullScreen = data;
                    },
                    disableTutorialMobile: true,
                    setDisableTutorialMobile(data) {
                        this.disableTutorialMobile = data;
                    },
                    oldCurrentTime: 0,
                    setOldCurrentTime() {
                        this.oldCurrentTime = this.get.currentTime;
                    },
                    isControlsShowed: true,
                    setIsControlsShowed(data) {
                        if (data !== this.isControlsShowed) {
                            this.isControlsShowed = data;
                        }
                    },
                    touch: null,
                    touch2: null,
                    lastTap: new Date().getTime(),
                    setLastTap(data) {
                        this.lastTap = data

                    },
                    setTouch(data) {
                        this.touch = data.touches[0]
                        this.touch2 = data.touches.length > 1
                    },
                    lockPlayPause: false,
                    setLockPlayPause(data) {
                        this.lockPlayPause = data

                    },
                },
        }
    }
}
const webTorrentProps = () => {
    return {
        webTorrentProps: {
            client: {
                get: {},
                set(data) {
                    this.get = data;
                },
            },
            selectedFile: {
                get: {},
                set(data) {
                    this.get = data;
                },
            },
            torrent: {
                get: {},
                set(data) {
                    this.get = data;
                },
            },
            filesCanBeStreamed: {
                get: [],
                set(data) {
                    this.get = data;
                },
            },
            progress: {
                get: 0,
                set(data) {
                    this.get = Math.round(data * 100);
                },
            },
            download: {
                getSpeed: 0,
                getUnit: "",
                set(data) {
                    this.getSpeed = data.speed;
                    this.getUnit = data.unit;
                },
            },
            upload: {
                getSpeed: 0,
                getUnit: "",
                set(data) {
                    this.getSpeed = data.speed;
                    this.getUnit = data.unit;
                },
            },
        }
    }
}


const defaultTheme = createTheme();

export const paletteDark = {
    mode: 'dark',
    background: {
        default: "#2b2d31",
        mainSection: "#232428",
        torrentCard: "#1D1D1D",
        paper: "#1e1f22",
        paper2: "#292929",
        settingsItem: "#2A2A2A"
    },
    text: {
        primary: "#FFF",
        secondary: "#A1A1A1",
        darkContrast: "#000"
    },
    primary: {
        light: '#757ce8',
        main: '#3f7ec0',
        dark: '#3f7ec0',
        contrastText: '#fff',
        goldenStar: "#e5b92c"
    },
    secondary: {
        main: "#c12d36"
    },
    tertiary: {
        main: "#99A1EC",
        contrastText: "#1D1D1D",
    },
    success: {
        main: "#A6F1C9",
        contrastText: "#1D1D1D",
    },
    info: {
        main: "#A6C0F1",
        contrastText: "#fff",
    },
    warning: {
        main: "#FEEACC",
        contrastText: "#fff",
    },
    error: {
        main: "#FC7785",
        contrastText: "#fff",
    },
    button: {
        main: "#fffff1",
        hover: "#4a4a4b"
    },
    disabled: {
        main: "#707577",
        contrastText: "#a1a7ac"
    },
    border: {main: "#262626"}
}
export const paletteLight = {
    mode: 'light',
    background: {
        default: "#c1cee3",
        mainSection: "#fff",
        torrentCard: "#E5EEFF",
        paper: "#FFF",
        paper2: "#f8fafe",
        settingsItem: "#6C77DA",
    },
    text: {
        primary: "#010101",
        secondary: "#A1A1A1",
        contrast: "#fff",
        darkContrast: "#000"
    },
    primary: {
        light: '#757ce8',
        main: '#3f7ec0',
        dark: '#3f7ec0',
        contrastText: '#fff',
        goldenStar: "#e5b92c"
    },
    secondary: {
        main: "#c12d36"
    },
    tertiary: {
        main: "#99A1EC",
        contrastText: "#fff",
    },
    success: {
        main: "#A6F1C9",
        contrastText: "#fff",
    },
    info: {
        main: "#A6C0F1",
        contrastText: "#fff",
    },
    warning: {
        main: "#FEEACC",
        contrastText: "#fff",
    },
    button: {
        main: "#010101",
        hover: "#afafb0"
    },
    disabled: {
        main: "#707577",
        contrastText: "#012244"
    },

    border: {main: "#c7cdd2"}
}
export const overridesDark = {
    MuiTab: {
        root: {
            padding: "0px"
        }
    },

    MuiLinearProgress: {
        styleOverrides: {
            root: {
                borderRadius: "10px"
            }
        }
    },
    MuiListItemText: {
        styleOverrides: {
            root: {
                marginTop: "7px",
            },
            primary: {
                fontSize: "14px",
                fontWeight: 100,
            }
        }
    },

    MuiTableContainer: {
        styleOverrides: {
            root: {
                height: "100%",
            },
        },
    },

    MuiContainer: {
        styleOverrides: {
            root: {
                overflow: "hidden",
                paddingLeft: "0px",
                paddingRight: "0px",
                height: "100%",
                [defaultTheme.breakpoints.up('xs')]: {
                    paddingLeft: "0px",
                    paddingRight: "0px",
                    paddingTop: "5px",
                }
            },
        },
    },
    MuiFilledInput: {
        styleOverrides: {
            root: {
                // backgroundColor: paletteDark.background.paper2,
                padding: 0,
                borderRadius: "16px",
                '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button':
                    {
                        display: 'none',
                    },
                '& input[type=number]': {
                    MozAppearance: 'textfield',
                },

            },
            input: {
                height: "70%!important",
                paddingLeft: "32px",
                paddingRight: "32px",
            },
            underline: {
                "&&&:before": {
                    borderBottom: "none"
                },
                "&&:after": {
                    borderBottom: "none"
                }
            }
        }
    },
    MuiDialogTitle: {
        styleOverrides: {
            root: {
                padding: "40px"
            },
        },
    },
    MuiDialogContent: {
        styleOverrides: {
            root: {
                minWidth: "100%",
                // padding: "0 20px"
            },
        },
    },
    MuiDialogActions: {
        styleOverrides: {
            root: {
                padding: "40px",
                justifyContent: "center",
                gap: "8px"
            },
        },
    },
    MuiButton: {
        styleOverrides: {
            root: {
                textTransform: "unset",
                fontSize: "14px",
                fontWeight: 300,
                boxShadow: "none"
            },
        },
    },
    MuiPaper: {
        styleOverrides: {
            root: {
                // borderRadius: "24px"
            }
        }
    },
    MuiTooltip: {
        styleOverrides: {
            tooltip: {
                background: "#161818",
                padding: "10px 10px",
                fontSize: "14px",
                fontWeight: 100
            },
        },
    }, MuiPopover: {
        styleOverrides: {
            paper: {
                backgroundColor: paletteDark.background.default + "!important",
                // borderRadius: "10px"
                background: paletteDark.background.default + "!important",
            },
        },
    },
    MuiMenuItem: {
        styleOverrides: {
            root: {
                "&.Mui-selected": {
                    // backgroundColor: "transparent!important"
                },

            }
        },
    },
    MuiListItemButton: {
        styleOverrides: {
            root: {
                "&.Mui-selected": {
                    // backgroundColor: "transparent!important"
                },
                "&:hover": {
                    backgroundColor: "#555!important"
                }
            },

        }
    },
    MuiOutlinedInput: {
        styleOverrides: {
            notchedOutline: {
                border: "1px solid #262626",
                borderColor: "#262626!important",
            },
            root: {
                "&.Mui-disabled": {
                    border: "1px solid #262626",

                },
                // padding: 0,
                // paddingRight: "30px",
                borderRadius: "16px!important",
                height: "54px",
                '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button':
                    {
                        display: 'none',
                    },
                '& input[type=number]': {
                    MozAppearance: 'textfield',
                },

            },
            multiline: {
                // paddingTop: "10px!important",
            },
            input: {
                // padding: 0,

                height: "100%!important",
                alignItems: "center",
                paddingLeft: "32px",
                display: "flex"
            },
        }
    },
    MuiDivider: {
        styleOverrides: {
            root: {
                borderColor: "#F8FAFE"
            }
        }
    }
}
export const overridesLight = {

    MuiTab: {
        styleOverrides: {
            textColorPrimary: {
                // padding: "0px",
                color: "white!important"
            },

        }
    },
    MuiLinearProgress: {
        styleOverrides: {
            root: {
                borderRadius: "10px"
            }
        }
    },
    MuiListItemText: {
        styleOverrides: {
            root: {
                marginTop: "7px",
            },
            primary: {
                fontSize: "14px",
                fontWeight: 100,
            }
        }
    },

    MuiTableContainer: {
        styleOverrides: {
            root: {
                height: "100%",
            },
        },
    },

    MuiContainer: {
        styleOverrides: {
            root: {
                overflow: "hidden",
                paddingLeft: "0px",
                paddingRight: "0px",
                height: "100%",
                [defaultTheme.breakpoints.up('xs')]: {
                    paddingLeft: "0px",
                    paddingRight: "0px",
                    paddingTop: "5px",
                }
            },
        },
    },
    MuiDialogTitle: {
        styleOverrides: {
            root: {
                padding: "40px"
            },
        },
    },
    MuiDialogContent: {
        styleOverrides: {
            root: {
                minWidth: "100%",
                // padding: "0 20px"
            },
        },
    },
    MuiDialogActions: {
        styleOverrides: {
            root: {
                padding: "40px",
                justifyContent: "center",
                gap: "8px"
            },
        },
    },
    MuiButton: {
        styleOverrides: {
            root: {
                textTransform: "unset",
                fontSize: "14px",
                fontWeight: 300,
                boxShadow: "none"
            },
        },
    },
    MuiPaper: {
        styleOverrides: {
            root: {
                // borderRadius: "24px"
            }
        }
    },

    MuiDivider: {
        styleOverrides: {
            root: {
                borderColor: "#292929"
            }
        }
    },

    MuiTooltip: {
        styleOverrides: {
            tooltip: {
                background: "#867979",
                padding: "10px 10px",
                fontSize: "14px",
                fontWeight: 100

            },
        },
    }, MuiPopover: {
        styleOverrides: {
            paper: {
                background: "#fff"
            },
        },
    },
    MuiMenuItem: {
        styleOverrides: {
            root: {
                "&.Mui-selected": {
                    backgroundColor: paletteLight.background.default + "!important"
                },
                "&:hover": {
                    backgroundColor: paletteLight.background.default + "!important"
                },
                backgroundColor: paletteLight.background.paper2 + "!important"
            }
        },
    },
    MuiListItemButton: {
        styleOverrides: {
            root: {
                // backgroundColor: paletteLight.background.paper2,
                "&.Mui-selected": {
                    // backgroundColor: paletteLight.background.default
                },
                "&:hover": {
                    backgroundColor: "#6e82a6!important",
                    color: "#fff"
                }
            },

        }
    },
    MuiOutlinedInput: {
        styleOverrides: {
            notchedOutline: {
                border: "1px solid #E4ECF6",
                borderColor: "#E4ECF6!important",
            },
            root: {
                backgroundColor: paletteLight.background.mainSection,
                "&.Mui-disabled": {
                    border: "1px solid #E4ECF6",
                },
                color: "black",
                padding: 0,
                paddingRight: "30px",
                borderRadius: "16px!important",
                height: "54px",
                border: "1px solid #E4ECF6",
                '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button':
                    {
                        display: 'none',
                    },
                '& input[type=number]': {
                    MozAppearance: 'textfield',
                },
            },
            multiline: {
                paddingTop: "10px!important",
            },
            input: {
                padding: 0,
                height: "100%!important",
                alignItems: "center",
                paddingLeft: "32px",
                display: "flex"
            },
        }
    },
    MuiFilledInput: {
        styleOverrides: {
            root: {
                backgroundColor: paletteLight.background.mainSection,
                padding: 0,
                borderRadius: "16px",
                '& input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button':
                    {
                        display: 'none',
                    },
                '& input[type=number]': {
                    MozAppearance: 'textfield',
                },

            },
            input: {
                height: "70%!important",
                paddingLeft: "32px",
                paddingRight: "32px",
            },
            underline: {
                "&&&:before": {
                    borderBottom: "none"
                },
                "&&:after": {
                    borderBottom: "none"
                }
            }
        }
    },
}


const options = {
    typography: {
        fontFamily: "Euclid Circular A",
        fontSize: 16,
        menuLabel: {
            fontStyle: "normal",
            fontWeight: 600,
            fontSize: "12px",
            lineHeight: "140%",
            textAlign: "center",
            textTransform: "uppercase",
        },
        h3: {
            fontSize: '5em',
        },
        h4: {
            fontSize: '4em',
        }, h5: {
            fontSize: '2em',
        }, h6: {
            fontSize: '1.5em',
        },
        title30: {
            fontSize: "30px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        title30Black: {
            fontSize: "30px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px",
            color: "black"
        },
        title: {
            fontSize: "26px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        title24: {
            fontSize: "24px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        cardTitle: {
            fontSize: "16px",
            fontWeight: 300,
            lineHeight: "120%",
            letterSpacing: "1px"
        },
        semiTitle: {

            fontSize: "20px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        }, semiTitleBlack: {
            color: "black",
            fontSize: "20px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        },
        subTitle18: {

            fontSize: "18px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        },
        subTitle: {

            fontSize: "17px",
            fontWeight: 500,
            lineHeight: "133%",
            letterSpacing: "0.4px"
        },
        smallText16: {

            fontSize: "16px",
            fontWeight: 200,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        }, smallText16Black: {

            fontSize: "16px",
            fontWeight: 200,
            lineHeight: "150%",
            letterSpacing: "0.4px",
            color: "black"
        }, smallText: {

            fontSize: "15px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        smallText14: {

            fontSize: "14px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        miniText:
            {

                fontSize: "13px",
                fontWeight: 500,
                lineHeight: "150%",
                letterSpacing: "0.4px"
            }
        ,
        microText: {
            color: "#9f9f9f",
            fontSize: "12px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        microTextW: {
            fontSize: "12px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        microText10: {
            fontSize: "10px",
            fontWeight: 500,
            lineHeight: "150%",
            letterSpacing: "0.4px"
        },
        noData: {

            fontSize: "30px",
            fontWeight: 500,
            lineHeight: "120%",
            letterSpacing: "0.4px"
        },
        upperCase14: {

            fontSize: "14px",
            fontWeight: "500",
            textAlign: "center",
            textTransform: "uppercase",
            letterSpacing: "1.5px"
        }
    },
    palette: ( localStorage.getItem("theme") === "dark" || !localStorage.getItem("theme")) ? paletteDark : paletteLight,
    components: {

        ...((!localStorage.getItem("theme") || localStorage.getItem("theme") === "dark")) ? overridesDark : overridesLight,
    },
};
