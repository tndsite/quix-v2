import {toJS} from "mobx";
import {observer} from "mobx-react";
import CardItemExpanded from "../CardItem/CardItemExpanded";
import {uuidGenerator} from "../../constants/Utility";
import {Drawer} from "@mui/material";
import React from "react";

const DescriptionDialogDrawerHandler = (props) => {
    let store = window.TNDglobalStoreMobx
    let {descriptionDialogHandler} = store
    let {
        open=false,
        item={},
    } = toJS(descriptionDialogHandler.get)

    return <Drawer
        // key={uuidGenerator()}
        anchor={"left"}
        variant={"persistent"}
        open={open}
        PaperProps={{sx: {width: "50%"}}}
    >
        <CardItemExpanded
            key={uuidGenerator()}
            item={item}
        />
    </Drawer>

}
export default observer(DescriptionDialogDrawerHandler)