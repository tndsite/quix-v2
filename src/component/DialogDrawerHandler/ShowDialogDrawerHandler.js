import CommonDialog, {CommonDrawer} from "../CommonDialog/CommonDialog";
import {toJS} from "mobx";
import {observer} from "mobx-react";

const ShowDialogDrawerHandler = (props) => {
    let store = window.TNDglobalStoreMobx
    let {showDialogHandler} = store
    let {
        open,
        handleClose,
        title,
        customContent,
        customActions,
        leftButtonText,
        rightButtonText,
        leftButtonOnClick,
        rightButtonOnClick,
        leftButtonColor,
        leftButtonVariant,
        leftStartIcon,
        rightButtonColor,
        rightButtonVariant,
        rightStartIcon,
        rightButtonDisabled,
        handleSubmit,
        content,
        anchor,
        variant,
        customStyle,
        isDialog,
        paperSizeIsSmallest,
        key
    } = toJS(showDialogHandler.get)
    if (!isDialog) {
        return <CommonDrawer
            {...{
                customKey: key,
                open,
                handleClose,
                title,
                customContent,
                customActions,
                leftButtonText,
                rightButtonText,
                leftButtonOnClick,
                rightButtonOnClick,
                leftButtonColor,
                leftButtonVariant,
                leftStartIcon,
                rightButtonColor,
                rightButtonVariant,
                rightStartIcon,
                rightButtonDisabled,
                handleSubmit,
                content,
                anchor,
                variant,
                customStyle
            }}


        />
    } else {
        return <CommonDialog
            {...{
                customKey: key,
                open,
                handleClose,
                title,
                customContent,
                customActions,
                leftButtonText,
                rightButtonText,
                leftButtonOnClick,
                rightButtonOnClick,
                leftButtonColor,
                leftButtonVariant,
                leftStartIcon,
                rightButtonColor,
                rightButtonVariant,
                rightStartIcon,
                rightButtonDisabled,
                handleSubmit,
                content,
                anchor,
                variant,
                customStyle,
                paperSizeIsSmallest
            }}

        />
    }

}
export default observer(ShowDialogDrawerHandler)