import {SimpleDialog} from "../SimpleDialog/SimpleDialog";
import {useEffect, useState} from "react";
import packageJson from "../../../package.json"
import {Button, DialogContent, Stack, Typography} from "@mui/material";
import axios from "axios";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";
import {getConfLocal, setConfLocal} from "../../constants/DbLocal";
import {uuidGenerator} from "../../constants/Utility";

let version = packageJson.version

const ChangelogDialogDrawerHandler = (props) => {
    let {store}=props
    let{changelogDialogHandler,checkBadge}=store
    let [text, setText] = useState([])

    useEffect(() => {

        (async () => {
            try {
                // let check = await getConfLocal("changelog" + version)
                // if (!check) {
                //     let res = await axios.get("https://quix.altervista.org/changelog.php?key=" + version)
                //     setText(res.data.data)
                //
                //     // Se vogliamo forzare l'apertura
                //     // changelogDialogHandler.set(true)
                // }
            } catch (e) {

            }
        })()

    }, [])


    return <SimpleDialog
        isDialogOpen={changelogDialogHandler.get}
        dialogTitle={"Changelog v" + packageJson.version}
        dialogContent={<DialogContent sx={{flexDirection: "column", display: "flex"}}>{
            text.map((x, xIndex) => {
                return <Stack
                    key={uuidGenerator()}
                    direction={"row"} padding={"10px"} alignItems={"center"}>- <Typography
                    key={"change" + xIndex} variant={"smallText"}>{x}</Typography></Stack>
            })}</DialogContent>}
        handleClose={() => {
            changelogDialogHandler.set(false)
        }}
        dialogActions={<Button
            color={"secondary"}
            onClick={async () => {
                await setConfLocal("changelog" + version, true)
                checkBadge.set(true)
                changelogDialogHandler.set(false)
            }}>Close</Button>}
    />

}
export default withCommonPropsLite(ChangelogDialogDrawerHandler)
