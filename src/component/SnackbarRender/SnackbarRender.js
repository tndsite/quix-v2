import {toJS} from "mobx";
import {IconButton, Snackbar} from "@mui/material";
import style from "./SnackBar.module.css";
import {Close} from "@mui/icons-material";
import React from "react";
import MuiAlert from "@mui/material/Alert";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});
const SnackbarRender=(props)=>{
    let{store}=props
    let {snackbarHandler} = store
    let {
        vertical,
        horizontal,
        open,
        autoHideDuration,
        message,
        type,
        customColor,
        customKey
    } = toJS(snackbarHandler.get)
    let color;
    switch (type) {
        case "success":
            color = "#43A047";
            break;
        case "warning":
            color = "#FFA000";
            break;
        case "info":
            color = "#0659BD";
            break;
        case "error":
            color = "#D32F2F";
            break;
        case "notification":
            if(customColor){
                color = customColor;
            }
            break;
        default:
            color = "#000000"
            break;
    }
    return  <Snackbar
        autoHideDuration={autoHideDuration}
        key={customKey}
        className={style.snackBar}
        anchorOrigin={{vertical: vertical, horizontal: horizontal}}
        open={open}
        onClose={
            () =>snackbarHandler.reset()
        }
        type={type}
        message={message}
        action={[
            <IconButton
                key={"snackBar"}
                id="close"
                aria-label="close"
                onClick={() => snackbarHandler.reset()}
                size="large">
                <Close className={style.icon} />
            </IconButton>
        ]}
    >
        <Alert onClose={() => snackbarHandler.reset()}
               severity={type}
               sx={{ width: '100%' ,
                   backgroundColor: color}}
               className={style.message}
        >
            {message}
        </Alert>
    </Snackbar>
}

export default withCommonPropsLite(SnackbarRender)