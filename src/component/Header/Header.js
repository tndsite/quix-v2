import {useEffect, useState} from "react";
import {IconButton, Stack, Typography} from "@mui/material";
import QuixLogo from "../../asset/image/logo-quix.png"
import QuixLogoMobile from "../../asset/image/logo-quix-mobile.png"
import packageJson from "../../../package.json"
import style from "./Header.module.css"
import LoginGoogle from "../GoogleLogin/LoginGoogle";
import {withCommonProps} from "../interfacehook/InterfaceHook";
import {getConfLocal} from "../../constants/DbLocal";
import {NotificationsNoneOutlined} from "@mui/icons-material";
import ThemePicker from "../ThemePicker";
import PrivacyPolicy from "../PrivacyPolicy";

let version = packageJson.version
const Header = (props) => {
    // let [state, setState] = useState({})
    // let {} = state
    let {customElement, history, isDesktop, isMobile, containerScroller, store} = props
    let [lastScrollTop, setLastScrollTop] = useState(0);
    // let [isScrolling, setIsScrolling] = useState(false);
    let {dialogHandler, changelogDialogHandler, checkBadge} = store

    // ,cooldownTimeout
    useEffect(() => {
        (async () => {
            checkBadge.set(await getConfLocal("changelog" + version))
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        let searchTimeout
        // const handleHeaderOnScroll = (e) => {
        //     if(dialogHandler.get){
        //         return
        //     }
        //     let {loadMore} = props;
        //     let header = document.getElementById("hidableHeader")
        //
        //     let container = e.target
        //     let scrollTop = container.scrollTop;
        //
        //
        //
        //     if (scrollTop > lastScrollTop) {
        //         header.style.top = "-" + header.offsetHeight + "px"
        //     console.info("(scrollTop > lastScrollTop)",scrollTop , lastScrollTop)
        //     } else {
        //         header.style.top = "0"
        //     console.info("else",scrollTop , lastScrollTop)
        //     }
        //     if (loadMore && scrollTop + container.clientHeight > container.scrollHeight / 2) {
        //         loadMore()
        //     }
        //     setLastScrollTop(scrollTop)
        // }


        const doSomething = (e) => {
            // Do something with the scroll position
            let header = document.getElementById("hidableHeader")
            let container = e.target
            let scrollTop = container.scrollTop;
            if (scrollTop > lastScrollTop) {
                header.style.top = "-" + header.offsetHeight + "px"
            } else {
                header.style.top = "0"
            }
            setLastScrollTop(scrollTop)
        }


        const wrap = (e) => {

            clearTimeout(searchTimeout);
            searchTimeout = setTimeout(() => {
                doSomething(e);
            }, 200)
        }

        if (containerScroller && containerScroller.current && containerScroller.current.getAttribute('listener') !== 'true') {
            containerScroller.current.addEventListener("scroll", wrap)
            containerScroller.current.setAttribute('listener', 'true');
        }

        return () => {
            if (containerScroller && containerScroller.current && containerScroller.current.getAttribute('listener') === 'true') {
                containerScroller.current.removeEventListener("scroll", wrap)
                // eslint-disable-next-line
                containerScroller.current.setAttribute('listener', 'false');
            }
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [lastScrollTop])

    let enviroment = ""
    if (process.env.REACT_APP_BETA === "true") {
        enviroment = "-beta"
    } else if (process.env.REACT_APP_ALPHA === "true") {
        enviroment = "-alpha"
    }


    return (<nav className={style.header} id={"hidableHeader"}>
            {dialogHandler.get.open ? null : <Stack
                justifyContent={"space-between"}
                alignItems={"center"}
                direction={"row"} width={"100%"} height={"100%"} sx={{
                background: "linear-gradient(90deg, rgba(0, 0, 0, 0.5) 0%, rgba(0, 0, 0, 0.5) 100%)"
            }}><Stack direction={"row"} alignItems={"center"}
                      justifyContent={"flex-start"}
                      paddingLeft={isMobile ? "10px" : undefined}
                      sx={{

                          zIndex: 100,
                      }}>
                <Stack
                    className={style.logoLink}
                    onClick={() => {
                        history.push("/")
                    }}
                >
                    <img
                        alt={"quix-logo"} src={!isMobile ? QuixLogo : QuixLogoMobile} className={style.logo}
                    />
                </Stack>
                <IconButton
                    onClick={() => {
                        changelogDialogHandler.set(true)
                    }}>
                    {/*<Badge invisible={checkBadge.get} color="secondary" badgeContent={"1"} overlap="circular">*/}
                    <Stack alignItems={"center"} justifyContent={"center"}>
                        <NotificationsNoneOutlined/>
                        <Typography variant={"smallText"}
                                    sx={{color: "white"}}
                        >v{packageJson.version + enviroment}</Typography>
                    </Stack>
                    {/*</Badge>*/}
                </IconButton>

            </Stack>
                {customElement(isDesktop) && <Stack sx={{
                    height: "100%"
                }}
                                                    alignItems={"center"}
                                                    justifyContent={"center"}
                >
                    {customElement(isDesktop)}
                </Stack>}
                <Stack direction={"row"} alignItems={"center"} sx={{
                    zIndex: 100,
                }}>
                    {/*<LanguagePicker/>*/}
                    {/*<PrivacyPolicy/>*/}
                    <ThemePicker/>
                    <LoginGoogle/>
                </Stack>
            </Stack>}
        </nav>
    );


}


export default withCommonProps(Header)
