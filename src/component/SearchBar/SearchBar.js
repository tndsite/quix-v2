import React, {forwardRef, useEffect, useImperativeHandle, useRef, useState} from 'react';
import {Close, Search} from "@mui/icons-material";
import {Button, CircularProgress, Fade, IconButton, OutlinedInput} from "@mui/material";
import style from "./SearchBar.module.css"
import {SimpleDialog} from "../SimpleDialog/SimpleDialog";
import LinearProgress, {linearProgressClasses} from '@mui/material/LinearProgress';
import {styled} from '@mui/material/styles';
import {withCommonProps} from "../interfacehook/InterfaceHook";


const SearchBar = (props) => {
    let {enableButton, onChange, isMobile, store = {searchedValue: ""}, inHeader} = props
    let [isInputDialogOpen, setInputDialogOpen] = useState(false)
    let searchTimeout, cooldownTimeout
    let {searchedValue} = store

    const refCooldownSearch = useRef();

    let Input = (propsComponent) => {
        let buttonOption = {
            autoFocus: propsComponent.autoFocus,
            className: propsComponent.customStyle ? propsComponent.customStyle : enableButton ?
                style.inputWithButton
                :
                style.inputWithoutButton,
            onChange: (event) => {
                if (!isMobile) {
                    let value = event.target.value
                    clearTimeout(searchTimeout);
                    clearTimeout(cooldownTimeout);
                    if (refCooldownSearch.current) {
                        refCooldownSearch.current.setShowParent(false);
                    }
                    if (value) {
                        searchTimeout = setTimeout(() => {
                            if (refCooldownSearch.current) {
                                refCooldownSearch.current.setShowParent(true);
                            }
                            cooldownTimeout = setTimeout(() => {
                                if (propsComponent.onChange) {
                                    propsComponent.onChange(value)
                                }
                            }, 2000)
                        }, 1000)
                    }
                }
            },
            defaultValue: searchedValue.get,
            onKeyDown: (event) => {
                if (event.code === "Enter" || event.code === "NumpadEnter") {
                    clearTimeout(searchTimeout);
                    clearTimeout(cooldownTimeout);
                    let value = document.getElementById("searchbarInput").value
                    if (propsComponent.onChange) {
                        propsComponent.onChange(value)
                    }
                }
            },
            endAdornment:
                propsComponent.enableButton ?
                    <>
                        <SearchCooldown ref={refCooldownSearch}/>
                        {searchedValue.get.length > 0 && <Button
                            sx={{height: "100%", width: "50px", borderRadius: "0 4px 4px 0"}}
                            variant={"text"}

                            onClick={() => {
                                store.searchedValue.set("")
                                if (propsComponent.onChange) {
                                    clearTimeout(searchTimeout);
                                    clearTimeout(cooldownTimeout);
                                    propsComponent.onChange("")
                                }
                            }}
                        >
                            <Close/>
                        </Button>}
                        <Button
                            id={"searchButton"}
                            sx={{height: "100%", width: "50px", borderRadius: "0 16px 16px 0"}}
                            variant={"contained"}

                            onClick={() => {
                                if (propsComponent.onChange) {
                                    clearTimeout(searchTimeout);
                                    clearTimeout(cooldownTimeout);
                                    propsComponent.onChange(document.getElementById("searchbarInput").value)
                                }
                            }}
                        >
                            <Search/>
                        </Button></>
                    :
                    <>
                        {searchedValue.get.length > 0 && <Button
                            sx={{height: "100%", width: "50px", borderRadius: "0"}}
                            variant={"text"}
                            onClick={() => {
                                store.searchedValue.set("")
                                if (propsComponent.onChange) {
                                    clearTimeout(searchTimeout);
                                    clearTimeout(cooldownTimeout);
                                    propsComponent.onChange("")
                                }
                            }}
                        >
                            <Close/>
                        </Button>}
                        <Button
                            id={"searchButton"}
                            sx={{height: "100%", width: "50px", borderRadius: "0 4px 4px 0", padding: 0}}
                            variant={"text"}
                            onClick={() => {
                                if (propsComponent.onChange) {
                                    clearTimeout(searchTimeout);
                                    clearTimeout(cooldownTimeout);
                                    propsComponent.onChange(document.getElementById("searchbarInput").value)
                                }
                            }}
                        >
                            <Search/>
                        </Button>
                    </>,
            placeholder: "Search here"
        }

        if (propsComponent.inHeader) {
            buttonOption = {
                ...buttonOption,
                autoFocus: true,
                className: style.inputWithButtonInHeader,
                endAdornment:
                    <>
                        <SearchCooldown ref={refCooldownSearch} inHeader={inHeader}/>
                        {searchedValue.get.length > 0 && <Button
                            sx={{height: "100%", width: "50px", borderRadius: "0 4px 4px 0", color: "white"}}
                            variant={"text"}
                            onClick={() => {
                                store.searchedValue.set("")
                                if (propsComponent.onChange) {
                                    clearTimeout(searchTimeout);
                                    clearTimeout(cooldownTimeout);
                                    propsComponent.onChange("")
                                }
                            }}
                        >
                            <Close/>
                        </Button>}
                        <Button
                            sx={{height: "100%", width: "50px", borderRadius: "0 16px 16px 0"}}
                            variant={"contained"}
                            onClick={() => {
                                if (propsComponent.onChange) {
                                    clearTimeout(searchTimeout);
                                    clearTimeout(cooldownTimeout);
                                    propsComponent.onChange(document.getElementById("searchbarInput").value)
                                }
                            }}
                        >
                            <Search/>
                        </Button></>
            }
        }

        return <>
            <OutlinedInput


                id={"searchbarInput"}
                key={"searchbarInput"}
                {...buttonOption}
            />

        </>
    }


    return isMobile && inHeader ? <>
            <SimpleDialog
                isDialogOpen={isInputDialogOpen}
                dialogContent={<Input
                    autoFocus={true}
                    enableButton={true}
                    customStyle={style.inputWithButtonNoMargin}
                    onChange={(value) => {
                        onChange(value)
                        setTimeout(() => {
                            setInputDialogOpen(false)
                        }, 500)
                    }}/>}
                handleClose={
                    () => {
                        setInputDialogOpen(false)
                    }
                }
            />
            <IconButton
                onClick={() => {
                    setInputDialogOpen(true)
                }

                }><Search/></IconButton></>
        :
        <Input onChange={onChange} enableButton={enableButton} inHeader={inHeader}/>
}

export default withCommonProps(SearchBar)


const BorderLinearProgress = styled(LinearProgress)(({theme}) => ({
    height: "100%",
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
        backgroundColor: "transparent",
    },
    [`& .${linearProgressClasses.bar}`]: {
        borderRadius: 5,
        backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
    },
}));
const BorderLinearProgressLite = styled(LinearProgress)(({theme}) => ({
    height: "100%",
    [`&.${linearProgressClasses.colorPrimary}`]: {
        backgroundColor: "transparent",
    },
    [`& .${linearProgressClasses.bar}`]: {
        backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
    },
}));
const SearchCooldown = forwardRef((props, ref) => {
    let [progress, setProgress] = useState(0);
    let [show, setShow] = useState(false);
    let {inHeader} = props
    let [random, setRandom] = useState(Math.round(Math.random() * 11))

    useImperativeHandle(ref, () => ({
        setShowParent(check) {
            setShow(check)
            if (!check) {
                setProgress(0)
                //Levare se si vuole cambiare solo ad ogni nuova ricerca
                setRandom(Math.round(Math.random() * 11))
            }
        }
    }));

    useEffect(() => {
        // clearInterval(timerInterval);
        let timerInterval = setInterval(() => {
            if (show) {
                setProgress((oldProgress) => {
                    if (oldProgress === 100) {
                        setTimeout(() => {
                            clearInterval(timerInterval);
                            setShow(false)
                            return 0;
                        }, 250)
                        // return 0;
                    }

                    const diff = Math.random() * 15;
                    return Math.min(oldProgress + diff, 100);
                });
            } else {
                clearInterval(timerInterval);
            }
        }, 100);

        return () => {
            clearInterval(timerInterval);
        };
    }, [show])

    let RandomLoading = forwardRef((props, ref) => <BorderLinearProgress
        ref={ref}
        variant="determinate" value={progress} sx={{
        width: "100%",
        position: "absolute",
        // "-moz-transform": "scale(-1, 1)",
        "-webkit-transform": "scale(-1, 1)",
        "-o-transform": "scale(-1, 1)",
        "-ms-transform": "scale(-1, 1)",
        transform: "scale(-1, 1)",

    }}/>)


    switch (random) {
        case 0:
        case 1:
        case 2:
            RandomLoading = forwardRef((props, ref) => <CircularProgress ref={ref} size={30} variant="determinate"
                                                                         value={progress}
                                                                         sx={{
                                                                             width: "100%",
                                                                             position: "absolute",
                                                                             right: inHeader ? 120 : 70
                                                                         }}/>)
            break;
        case 3:
        case 4:
        case 5:

            RandomLoading = forwardRef((props, ref) => <BorderLinearProgressLite ref={ref} value={progress}
                                                                                 variant="determinate"
                                                                                 sx={{
                                                                                     width: "100%",
                                                                                     position: "absolute",
                                                                                     top: 33,
                                                                                     right: -1,
                                                                                     height: "7px",
                                                                                 }}/>)
            break
        case 6:
        case 7:
        case 8:
            RandomLoading = forwardRef((props, ref) => <BorderLinearProgressLite ref={ref} value={progress}
                                                                                 variant="determinate"
                                                                                 sx={{
                                                                                     width: "100%",
                                                                                     height: "7px",
                                                                                     position: "absolute",
                                                                                     top: 0
                                                                                 }}/>)
            break
        case 9:
        case 10:
        case 11:
        default:
            break
    }


    return <Fade
        onClick={() => {
            document.getElementById("searchbarInput").focus()
        }}
        in={show}
        unmountOnExit
    >
        <RandomLoading/>

    </Fade>
})