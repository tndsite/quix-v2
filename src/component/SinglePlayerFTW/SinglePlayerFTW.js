import {uuidGenerator} from "../../constants/Utility";
import React, {useEffect, useState} from "react";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";
import WebTorrentPlayer from "../WebtorrentPlayer/WebTorrentPlayer";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";

const SinglePlayerFTW = (props) => {

    let {store, searchValues} = props
    let {loading} = store
    let [episodeProps, setEpisodeProps] = useState({
        id: null,
        seasonNumber: null,
        episodeNumber: null,
        selectedLink: null,
        selectedMagnet: null,
    });
    let [loader, stopLoading] = useLoadingSystem(true)
    useEffect(() => {
        (async () => {
            try {
                let link=searchValues.magnet
                if(link.startsWith("magnet")){
                    let infoHash =link.replace("magnet:?xt=urn:btih:","")
                    infoHash=infoHash.substring(infoHash.indexOf("&"),-infoHash.indexOf("&"))
                    setEpisodeProps({
                        id: infoHash,
                        seasonNumber: infoHash,
                        episodeNumber: infoHash,
                        selectedLink: searchValues.magnet,
                        selectedMagnet: searchValues.magnet,
                    })
                }else{
                    setEpisodeProps({
                        id: searchValues.idShow,
                        seasonNumber: searchValues.seasonNumber,
                        episodeNumber: searchValues.episodeNumber,
                        selectedLink: searchValues.episodeLink,
                        selectedMagnet: searchValues.magnet,
                    })
                }

                stopLoading()

                loading.set(false)
            } catch (e) {
                console.error("errore recuper dettaglio serie", e, e.response)
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return loader ? loader :
        <WebTorrentPlayer
            key={uuidGenerator()}
            episodeProps={episodeProps}
        />

}

export default withCommonPropsLite(SinglePlayerFTW)
