import {uuidGenerator} from "../../constants/Utility";
import React, {useEffect, useState} from "react";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";
import WebtorrentFileList from "../WebtorrentFileList/WebtorrentFileList";

const TorrentDownloadList = (props) => {

    let {store, searchValues} = props
    let {loading} = store
    let [episodeProps, setEpisodeProps] = useState({
        selectedMagnet: null,
    });
    let [loader, stopLoading] = useLoadingSystem(true)
    useEffect(() => {
        (async () => {
            try {

                let link=searchValues.magnet
                if(link.startsWith("magnet")){
                    let infoHash =link.replace("magnet:?xt=urn:btih:","")
                    infoHash=infoHash.substring(infoHash.indexOf("&"),-infoHash.indexOf("&"))
                    console.info("Ecco l'infohash",infoHash)
                    setEpisodeProps({
                        id: infoHash,
                        seasonNumber: infoHash,
                        episodeNumber: infoHash,
                        selectedLink: searchValues.magnet,
                        selectedMagnet: searchValues.magnet,
                    })
                }else{
                    setEpisodeProps({
                        id: searchValues.magnet,
                        seasonNumber: searchValues.magnet,
                        episodeNumber: searchValues.magnet,
                        selectedLink: searchValues.magnet,
                        selectedMagnet: searchValues.magnet,
                    })
                }
                stopLoading()

                loading.set(false)
            } catch (e) {
                console.error("errore recuper dettaglio serie", e, e.response)
            }
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return loader ? loader :
        <WebtorrentFileList
            key={uuidGenerator()}
            episodeProps={episodeProps}
        />

}

export default withCommonPropsLite(TorrentDownloadList)
