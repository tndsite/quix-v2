import React, {useState} from 'react';
import {Button, IconButton, ListItemIcon, Stack} from "@mui/material";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import {GOOGLE_ACCESS_TOKEN, GOOGLE_REFRESH_TOKEN} from "../../constants/Config";
import {databasePull, databasePush} from "../../constants/GoogleApi";
import PopupState, {bindMenu, bindTrigger} from 'material-ui-popup-state';
import {GetApp, Publish, SyncDisabled} from "@mui/icons-material";
import LoginButton from "./LoginButton";
import {withCommonProps} from "../interfacehook/InterfaceHook";
import {getConfLocal, setConfLocal} from "../../constants/DbLocal";


const LoginGoogle = (props) => {

    let [menuOpen, setMenuOpen] = useState(false)
    let [anchorEl, setAnchorEl] = useState()
    let {store,} = props
    let {toSync, profileData,loading} = store
    let {disableLogout} = props;

    const handleClose = () => {
        setMenuOpen(false)
        setAnchorEl(null)
    };


    const handleClick = (e) => {
        setMenuOpen(true)
        setAnchorEl(e.currentTarget)
    };



    let imageUrl = profileData.get && (profileData.get.imageUrl || profileData.get.picture)

    return imageUrl ? <PopupState variant="popover" popupId="demo-popup-menu">
        {(popupState) => (<Stack
            direction={"row"}
        >
            <IconButton style={{flex: 1}} aria-controls="simple-menu" aria-haspopup="true"
                        {...bindTrigger(popupState)}
                        onClick={disableLogout ? disableLogout : handleClick}>
                <img alt={"profileImage"} style={{
                    verticalAlign: "middle",
                    width: "50px",
                    height: "50px",
                    borderRadius: "50%"

                }}
                     src={imageUrl}/>
            </IconButton>
            <Menu
                id="simple-menu"
                keepMounted
                {...bindMenu(popupState)}
                anchorEl={anchorEl}
                open={menuOpen}
                onClose={handleClose}

                anchorOrigin={{
                    horizontal: 'center',
                    vertical: "bottom"
                }}

            >
                <MenuItem
                    onClick={async () => {
                        await setConfLocal(GOOGLE_ACCESS_TOKEN,null);
                        await setConfLocal(GOOGLE_REFRESH_TOKEN,null);
                        window.google.accounts.id.disableAutoSelect()
                        profileData.reset()
                        handleClose();
                    }}
                    >
                    <Button
                        sx={{textTransform:"capitalize"}}
                        variant={"contained"}
                        color={"secondary"}
                        fullWidth
                      >
                        Sign Out
                    </Button>
                </MenuItem>
                <MenuItem onClick={() => {
                    if (toSync) {
                        handleClose();
                        loading.set(true)
                        databasePush().then(() => {
                            loading.set(false)
                            // window.location.reload()
                        });
                    }
                }}>
                    <ListItemIcon>
                        {toSync ? <Publish color={"secondary"} fontSize={"large"}/> :
                            <SyncDisabled color={"secondary"} fontSize={"large"}/>}
                    </ListItemIcon>
                    {toSync ? <p>Push local to remote</p> :
                        <p>Database synced</p>}

                </MenuItem>
                {toSync ? <MenuItem onClick={async() => {
                    handleClose();
                    loading.set(true)
                    databasePull(true).then(() => {
                        loading.set(false)
                         // window.location.reload()
                    })

                }}>
                    <ListItemIcon>
                        <GetApp color={"secondary"} fontSize={"large"}/>
                    </ListItemIcon>
                    <p>Pull remote to local and override local</p>
                </MenuItem> : null}
            </Menu>
        </Stack>)}
    </PopupState> : <
    >
        <LoginButton
            onClick={disableLogout ? (event) => {
                disableLogout();
            } : false}
        />
    </>

}

export default withCommonProps(LoginGoogle);
