import React, {useEffect} from 'react';

import {GOOGLE_ACCESS_TOKEN, GOOGLE_REFRESH_TOKEN} from "../../constants/Config";
import {Button} from "@mui/material";
import {databasePull, getAccessToken, getRefreshToken} from "../../constants/GoogleApi";
import axios from "axios";
import {withCommonProps} from "../interfacehook/InterfaceHook";
import {getConfLocal, setConfLocal} from "../../constants/DbLocal";
import {commonHandledRequest} from "../../constants/Utility";
import BaseAxios from "../../constants/BaseAxios";
import moment from "moment/moment";
import {LOADING, NO_LOADING} from "../../constants/Constants";


const Login = (props) => {
    let {store} = props
    let {profileData, loading, myItems, checkFullHome} = store

    useEffect(() => {
        (async () => {
            let refreshToken = await getConfLocal(GOOGLE_REFRESH_TOKEN)
            if (refreshToken) {

                await commonHandledRequest({
                    action: async () => {
                        let accessToken = await getRefreshToken(refreshToken);
                        await setConfLocal(GOOGLE_ACCESS_TOKEN, accessToken)
                        await getProfileData()
                        console.info("sono loggato, pullo dal server")
                        await databasePull(true)
                    },
                    loadingType: LOADING,
                    translationKeyError: "googleLogin",
                })

            }

        })()
    }, [])

    const getProfileData = async () => {
        let response = await axios.get("https://www.googleapis.com/oauth2/v2/userinfo", {
            headers: {
                Authorization: "Bearer " + await getConfLocal(GOOGLE_ACCESS_TOKEN)
            }
        });
        profileData.set(response.data);

    }
    return <Button
        sx={{textTransform: "capitalize"}}
        variant={"contained"}
        color={"primary"}
        onClick={() => {
            const client = window.google.accounts.oauth2.initCodeClient({
                client_id: '12998500978-9rk2hki7jntah53m20m9nag4bgk1kr1o.apps.googleusercontent.com',
                scope: 'profile email https://www.googleapis.com/auth/drive.appdata',
                ux_mode: 'popup',
                callback: async (response) => {
                    loading.set(true)
                    let accessToken = await getAccessToken(response.code);
                    await setConfLocal(GOOGLE_ACCESS_TOKEN, accessToken);
                    await getProfileData();
                    try {
                        await databasePull();
                    } catch (e) {

                        loading.set(false);
                    }
                    console.info("Settings il refresh della home", myItems.refresh)
                    checkFullHome.set(true)
                    myItems.setRefresh(true)
                    console.info("settato il refresh della home", myItems.refresh)
                    loading.set(false);
                }
            });
            client.requestCode()
        }}>
        Sign in
    </Button>


}

export default withCommonProps(Login);
