import {Button, ClickAwayListener, Grow, Stack, Tooltip} from "@mui/material";
import {ArrowLeft, ArrowRight} from "@mui/icons-material";
import moment from "moment";
import ReactCountryFlag from "react-country-flag";
import React, {useEffect, useState} from "react";
import {FormattedMessage} from "react-intl";
import {commonHandledRequest} from "../constants/Utility";
import {APP_LOADING} from "../constants/Constants";
import {withCommonPropsLite} from "./interfacehook/InterfaceHook";

const mapperCountryCode = (lang) => {
    switch (lang) {
        case "en":
            return "gb"
        default:
            return lang
    }
}
const langToName = (lang) => {
    return <FormattedMessage id={lang}/>
    // switch (lang) {
    //     case "en":
    //         return "English"
    //     case "it":
    //         return "Italiano"
    //     case "fr":
    //         return "Français"
    //     default:
    //         return lang
    // }
}
const mapperFlagToCountryCode = (lang) => {
    switch (lang) {
        case "gb":
            return "en"
        default:
            return lang
    }
}

const ButtonRow = withCommonPropsLite((props) => {

    let languagePickerRef = React.createRef()
    let [checkScrollState, setCheckScrollState] = useState("S")
    let {disableMargin, store, isMobile} = props
    let {language} = store


    useEffect(() => {
        const handleCheckScrollStateChange = () => {
            if (languagePickerRef.current.scrollLeft === 0) {
                setCheckScrollState("S")
            } else if (Math.abs(languagePickerRef.current.scrollWidth - languagePickerRef.current.clientWidth - languagePickerRef.current.scrollLeft) < 1) {
                setCheckScrollState("F")
            } else {
                setCheckScrollState("D")
            }
        }
        switch (checkScrollState) {
            case "M":
                if (languagePickerRef &&
                    languagePickerRef.current
                ) {
                    languagePickerRef.current.scrollLeft -= 40
                }
                handleCheckScrollStateChange()
                break;
            case "P":
                if (languagePickerRef &&
                    languagePickerRef.current
                ) {
                    languagePickerRef.current.scrollLeft += 40
                }
                handleCheckScrollStateChange()
                break;
            case "F":
            case "S":
            case "D":
            default:
                break;
        }

    }, [checkScrollState, languagePickerRef])
    useEffect(() => {
        return () => {
            if (language.get !== language.temp) {
                language.setTemp(language.get)
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    if (Object.keys(language.list).length < 2) {
        return null
    }

    let checkEnableButtons = Object.keys(language.list).length > 4


    // if (isMobile) {
    //
    //     return <Stack
    //         width={"100%"}
    //         ref={languagePickerRef}
    //         justifyContent={checkEnableButtons ? undefined : "flex-end"}
    //         sx={{
    //             overflowY: "hidden",
    //             overflowX: "clip",
    //         }}>
    //
    //         {
    //             Object.keys(language.list)
    //                 .map((option, index) => {
    //                     let lang = option
    //                     lang = mapperCountryCode(lang)
    //                     let checkSameLang = lang === mapperCountryCode(language.temp)
    //
    //
    //                     return <ButtonFlag key={"flag-button" + index}
    //                                        lang={lang}
    //                                        checkSameLang={checkSameLang}
    //                     />
    //
    //
    //                 })
    //         }
    //
    //     </Stack>
    // }

    return <Stack direction={"row"}
                  sx={{
                      width: checkEnableButtons ? "220px" : "100%",
                      marginTop: disableMargin ? 0 : "10px",
                      backgroundColor: "background.paper",
                      borderRadius: "8px"
                  }}
                  justifyContent={"space-between"}
                  alignItems={"center"}
    >
        {checkEnableButtons && <Button
            disabled={checkScrollState === "S"}
            onClick={() => {
                setCheckScrollState("M")
            }}

            sx={{
                minWidth: "30px",
                height: "30px"
            }}
        >
            <ArrowLeft/>
        </Button>}
        <Stack
            ref={languagePickerRef}
            direction={"row"}
            sx={{
                overflowY: "hidden",
                overflowX: "clip",
                maxWidth: "160px",
                minWidth: "160px",
            }}>

            {
                Object.keys(language.list)
                    .map((option, index) => {
                        let lang = option
                        lang = mapperCountryCode(lang)
                        let checkSameLang = lang === mapperCountryCode(language.temp)

                        if (checkSameLang) {
                            return <ButtonFlag key={"flag-button" + index}
                                               lang={lang}
                                               checkSameLang={checkSameLang}
                            />
                        } else {
                            return <Tooltip key={"flag-button" + index}
                                            title={langToName(option)}
                            >
                                <Stack>
                                    <ButtonFlag
                                        lang={lang}
                                        checkSameLang={checkSameLang}
                                    />
                                </Stack>
                            </Tooltip>
                        }

                    })
            }

        </Stack>
        {checkEnableButtons && <Button
            disabled={checkScrollState === "F"}
            onClick={() => {
                setCheckScrollState("P")
            }}
            sx={{
                minWidth: "30px",
                height: "30px"
            }}
        >
            <ArrowRight/>
        </Button>}
    </Stack>


})

let LanguagePicker = withCommonPropsLite(({store, isMobile, viewOnly}) => {
    let {language} = store
    let [o, sO] = useState(false)

    // if (isMobile) {
    //     return <>
    //         <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} width={"100%"}
    //                gap={"16px"} padding={"16px 0"}>
    //             <Stack direction={"row"} alignItems={"center"} gap={"16px"}>
    //                 <ButtonFlag key={"flag-button"}
    //                             viewOnly={viewOnly}
    //                             lang={mapperCountryCode(language.get)}
    //                             checkSameLang={false}
    //                 />
    //                 <Typography variant={"smallText16"}>
    //                     {langToName(language.get)}
    //                 </Typography>
    //             </Stack>
    //             <IconButton
    //                 sx={{
    //                     padding: 0
    //                 }}
    //                 onClick={() => {
    //                     sO(!o)
    //                 }}
    //             >
    //                 {o ? <ArrowUpIcon/> : <ArrowDownIcon/>}
    //             </IconButton>
    //         </Stack>
    //         <Collapse
    //             in={o}
    //             unmountOnExit={false}
    //         >
    //             <ButtonRow/>
    //
    //         </Collapse>
    //     </>
    // }

    return <Stack sx={{position: "relative"}}>

        <ClickAwayListener onClickAway={() => sO(false)}>
            <Stack>
                <Tooltip title={
                    <FormattedMessage id={"changeLanguage"}/>
                }>
                    <Stack>
                        <ButtonFlag
                            inSidebar={false}
                            lang={mapperCountryCode(language.get)}
                            checkSameLang={false}
                            onClick={() => {
                                sO(!o)
                            }}
                        />
                    </Stack>
                </Tooltip>
                <Grow
                    in={o}
                    sx={{
                        position: "absolute",
                        zIndex: 1000000,
                        top: "-10px",
                        right: "-70px",
                        width: "100%"
                    }}
                >
                    <Stack>
                        <ButtonRow/>
                    </Stack>
                </Grow>
            </Stack>
        </ClickAwayListener>
    </Stack>
})

export default withCommonPropsLite(LanguagePicker)


let ButtonFlag = withCommonPropsLite(({
                                          lang,
                                          checkSameLang,
                                          store,
                                          onClick, isMobile, viewOnly, inSidebar
                                      }) => {

    let {language, theme, client} = store


    // if (isMobile) {
    //     if (viewOnly) {
    //         return <ReactCountryFlag
    //             className="emojiFlag"
    //             countryCode={lang}
    //             style={{
    //                 width: '24px',
    //                 maxWidth: '24px',
    //                 minWidth: '24px',
    //                 height: '24px',
    //                 maxHeight: '24px',
    //                 minHeight: '24px',
    //                 borderRadius: "50px",
    //                 objectFit: "cover"
    //             }}
    //             svg/>
    //     }
    //     return <MenuItem
    //         onClick={onClick ? onClick : async () => {
    //
    //             await commonHandledRequest({
    //                 action: async () => {
    //
    //                     lang = mapperFlagToCountryCode(lang)
    //                     if (lang) {
    //                         moment.locale(lang);
    //                     } else {
    //                         moment.locale("en");
    //                     }
    //
    //                     language.setTemp(lang)
    //
    //
    //                     localStorage.setItem("websiteLanguage", lang)
    //                     language.set(lang)
    //
    //                 },
    //                 loadingType: APP_LOADING,
    //                 translationKeyError: "language",
    //                 translationKeySuccess: "language"
    //             })
    //         }}
    //
    //     >
    //         <ReactCountryFlag
    //             className="emojiFlag"
    //             countryCode={lang}
    //             style={{
    //                 width: '24px',
    //                 maxWidth: '24px',
    //                 minWidth: '24px',
    //                 height: '24px',
    //                 maxHeight: '24px',
    //                 minHeight: '24px',
    //                 borderRadius: "50px",
    //                 objectFit: "cover",
    //                 marginRight:"16px"
    //             }}
    //             svg/>
    //         <Typography variant={"smallText16"}>
    //             {langToName(mapperFlagToCountryCode(lang))}
    //         </Typography>
    //     </MenuItem>
    //
    // }

    return <Stack
        sx={{
            maxHeight: inSidebar ? "70px" : "50px",
            height: inSidebar ? "70px" : "50px",
            padding: 0,
            minWidth: inSidebar ? "98px" : "50px",
            width: inSidebar ? "98px" : "50px",
            maxWidth: inSidebar ? "98px" : "50px",
            transition: "width 1s",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            cursor: "pointer"
        }}

        id={"language-button-" + lang}
        // sx={{
        //     minWidth: "40px",
        //     height: "40px",
        //     padding: 0,
        // }}
        disabled={checkSameLang}
        onClick={onClick ? onClick : async () => {

            await commonHandledRequest({
                action: async () => {

                    lang = mapperFlagToCountryCode(lang)
                    if (lang) {
                        moment.locale(lang);
                    } else {
                        moment.locale("en");
                    }

                    language.setTemp(lang)


                    localStorage.setItem("websiteLanguage", lang)
                    language.set(lang)

                },
                loadingType: APP_LOADING,
                translationKeyError: "language",
                translationKeySuccess: "language"
            })


        }}
    ><ReactCountryFlag
        className="emojiFlag"
        countryCode={lang}
        style={{
            width: '24px',
            maxWidth: '24px',
            minWidth: '24px',
            height: '24px',
            maxHeight: '24px',
            minHeight: '24px',
            borderRadius: "50px",
            objectFit: "cover"
        }}
        svg/></Stack>

})

