import {Button, DialogContent, IconButton, Stack, Tooltip, Typography} from "@mui/material";
import React, {useEffect} from "react";
import {withCommonPropsLite} from "./interfacehook/InterfaceHook";
import {FormattedMessage} from "react-intl";
import {commonHandledRequest} from "../constants/Utility";
import {APP_LOADING} from "../constants/Constants";
import DarkThemeIcon from "../asset/icons/DarkThemeIcon";
import LightThemeIcon from "../asset/icons/LightThemeIcon";
import {Close, PrivacyTip} from "@mui/icons-material";
import {TypographyEllipsed} from "./TypographyEllipsed";
import {deleteLink} from "../constants/StorageHelper";


const PrivacyPolicy = (props) => {
    let {store, variant} = props
    let {
        theme,
        smallDialogHandler
    } = store;
    let onClick = () => {
        smallDialogHandler.set({
            isDialog: true,
            paperSizeIsSmallest: true,
            open: true,
            title: <Stack direction={"row"} justifyContent={"center"} alignItems={"flex-start"}>
                <Typography>
                    Privacy Policy & Terms and Conditions
                </Typography>
            </Stack>,
            customContent: <DialogContent>
                <Typography variant="body2">Effective Date: March 4, 2024</Typography>
                <Typography variant="body1">
                    This Privacy Policy describes how Quix.tnl.one ("Quix", "we", "us", or
                    "our") collects, uses, and discloses your information when you use our
                    website.
                </Typography>

                <Typography variant="h2">Information We Collect</Typography>
                <ul>
                    <li>
                        <Typography variant="body1">
                            <b>No data is automatically collected or stored on any database when
                                you visit Quix.</b>
                        </Typography>
                    </li>
                    <li>
                        <Typography variant="body1">
                            <b>Optional Information:</b> If you choose to sign in using Google
                            Drive API, we will access the following information with your explicit
                            consent:
                        </Typography>
                        <ul>
                            <li>
                                <Typography variant="body1">
                                    Your Google Drive files and folders (read-only access)
                                </Typography>
                            </li>
                            <li>
                                <Typography variant="body1">
                                    Your Google account information (name and email address)
                                </Typography>
                            </li>
                        </ul>
                    </li>
                </ul>

                <Typography variant="h2">Use of Information</Typography>
                <ul>
                    <li>
                        <Typography variant="body1">
                            <b>Non-signed-in users:</b> We do not use any information collected
                            from non-signed-in users.
                        </Typography>
                    </li>
                    <li>
                        <Typography variant="body1">
                            <b>Signed-in users:</b> We only use the information collected through
                            Google Drive API to access your files and folders to offer the
                            functionalities of Quix. We do not use this information for any other
                            purpose, including marketing, advertising, or sharing with third
                            parties.
                        </Typography>
                    </li>
                </ul>

                <Typography variant="h2">Data Deletion</Typography>
                <ul>
                    <li>
                        <Typography variant="body1">
                            We do not store any user data on our servers, so there is no need for
                            a specific data deletion process.
                        </Typography>
                    </li>
                    <li>
                        <Typography variant="body1">
                            If you revoke access to Google Drive API, we will no longer be able to
                            access your files and folders.
                        </Typography>
                    </li>
                </ul>

                <Typography variant="h2">Security</Typography>
                <Typography variant="body1">
                    We take reasonable measures to protect your information from unauthorized
                    access, disclosure, alteration, or destruction. However, no website or
                    internet transmission is completely secure, so we cannot guarantee the
                    absolute security of your information.
                </Typography>

                <Typography variant="h2">Changes to this Policy</Typography>
                <Typography variant="body1">
                    We may update this Privacy Policy from time to time. We will notify you
                    of any changes by posting the new Privacy Policy on this page.
                </Typography>

                <Typography variant="h2">Contact Us</Typography>
                <Typography variant="body1">
                    If you have any questions about this Privacy Policy, please contact us
                    at service@tnl.one
                </Typography>

                <Typography variant="h2">Additional Notes</Typography>
                <ul>
                    <li>
                        <Typography variant="body1">
                            Quix does not track users across the web and does not use cookies.
                        </Typography>
                    </li>
                    <li>
                        <Typography variant="body1">
                            Quix does not target children and does not knowingly collect personal
                            information from children under the age of 13.
                        </Typography>
                    </li>
                </ul>
                <div className="terms-and-conditions">
                    <Typography variant="h1">Quix.tnl.one Terms and Conditions</Typography>
                    <Typography variant="body2">Effective Date: March 4, 2024</Typography>
                    <Typography variant="body1">
                        These Terms and Conditions ("Terms") govern the use of the Quix.tnl.one website ("Website") by
                        the user ("User"). By accessing or using the Website, the User agrees to be bound by these
                        Terms.
                    </Typography>

                    <Typography variant="h2">Services Offered</Typography>
                    <ul>
                        <li>
                            <Typography variant="body1">
                                <b>Manage and organize your favorite TV shows.</b>
                            </Typography>
                        </li>
                        <li>
                            <Typography variant="body1">
                                <b>Discover new shows based on your interests.</b>
                            </Typography>
                        </li>
                    </ul>

                    <Typography variant="h2">Access and Use</Typography>
                    <ul>
                        <li>
                            <Typography variant="body1">
                                Access to the Website is free. The User is responsible for Internet access and any
                                associated costs.
                            </Typography>
                        </li>
                        <li>
                            <Typography variant="body1">
                                The Website may contain links to third-party websites. The User acknowledges and agrees
                                that Quix.tnl.one is not responsible for the content or availability of such third-party
                                websites.
                            </Typography>
                        </li>
                    </ul>

                    <Typography variant="h2">Personal Data</Typography>
                    <Typography variant="body1">
                        The Website **does not collect or store any personal data of the User on its servers**. The User
                        can choose to save their data, such as favorite TV shows, in two ways:
                    </Typography>
                    <ul>
                        <li>
                            <Typography variant="body1">
                                <b>Local storage:</b> Data is saved on the User's browser through local storage. This
                                means that the data is only accessible on that specific device and could be lost if the
                                User clears their browsing history or cookies.
                            </Typography>
                        </li>
                        <li>
                            <Typography variant="body1">
                                <b>Google Drive:</b> The User can choose to connect their Google Drive account to sync
                                their data. In this case, the data will be saved on Google Drive and will be accessible
                                on any device where the User has logged into their Google account.
                            </Typography>
                        </li>
                    </ul>
                    <Typography variant="body1">
                        The User is solely responsible for backing up their data. It is recommended that the User
                        regularly backs up their data to a secure location.
                    </Typography>

                    <Typography variant="h2">Content</Typography>
                    <Typography variant="body1">
                        The Website contains content owned by Quix.tnl.one and its licensors. The User may not copy,
                        distribute, modify, transmit, or create derivative works from such content without the prior
                        written consent of Quix.tnl.one.
                    </Typography>

                    <Typography variant="h2">Intellectual Property</Typography>
                    <Typography variant="body1">
                        All trademarks, logos, and trade names used on the Website are owned by Quix.tnl.one or its
                        licensors. The User may not use such trademarks, logos, or trade names without the prior written
                        consent of Quix.tnl.one.
                    </Typography>

                    <Typography variant="h2">Limitation of Liability</Typography>
                    <Typography variant="body1">
                        Quix.tnl.one is not liable for any damages resulting from the use of the Website, including but
                        not limited to direct, indirect, consequential, incidental, or special damages.
                    </Typography>
                    <Typography variant="body1">
                        In no event shall Quix.tnl.one's total liability to the User for all damages, losses, and causes
                        of action (whether in contract, tort, or otherwise) exceed the amount paid by the User to
                        Quix.tnl.one for access to the Website.
                    </Typography>

                    <Typography variant="h2">Indemnity</Typography>
                    <Typography variant="body1">
                        The User agrees to indemnify and hold harmless Quix.tnl.one, its employees, contractors, agents,
                        and licensors from and against any claims, losses, damages, liabilities, and expenses (including
                        attorneys' fees) arising out of or relating to the User's use of the Website.
                    </Typography>

                    <Typography variant="h2">Changes to these Terms</Typography>
                    <Typography variant="body1">
                        Quix.tnl.one may modify these Terms at any time. The User is responsible for periodically
                        reviewing these Terms. Continued use of the Website by the User after the modification of these
                        Terms constitutes acceptance of such modifications.
                    </Typography>
                    <Typography variant="h2">Governing Law and Dispute Resolution</Typography>
                    <Typography variant="body1">
                        These Terms shall be governed by and construed in accordance with the laws of the State of
                        Italy, without regard to its conflict of laws principles
                    </Typography>

                    <Typography variant="h2">Entire Agreement</Typography>
                    <Typography variant="body1">
                        These Terms constitute the entire agreement between Quix.tnl.one and the User with respect to
                        the use of the Website. Any waiver of any provision of these Terms shall not be effective unless
                        in writing and signed by both parties.
                    </Typography>

                    <Typography variant="h2">Contact Us</Typography>
                    <Typography variant="body1">
                        For any questions or comments about these Terms, please contact Quix.tnl.one at service@tnl.one.
                    </Typography>

                    <Typography variant="h2">Effective Date</Typography>
                    <Typography variant="body1">
                        These Terms are effective as of March 4, 2024.
                    </Typography>
                </div>
            </DialogContent>,
            customActions: <Button variant={"contained"} color={"secondary"} onClick={() => {
                smallDialogHandler.reset()
            }}>Close</Button>,
            handleClose: () => smallDialogHandler.reset()
        })
    }

    useEffect(() => {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        //If query param contain parameter "privacy" then open privacy policy dialog
        if (window.location.search.includes("privacy")) {
            onClick();
        }
    }, [])

    if(variant){
        return <Stack width={"100%"} direction={"row"} justifyContent={"space-around"}>
            <Button variant={"text"} onClick={onClick}>
                Privacy Policy
            </Button>
            <Button variant={"text"} onClick={onClick}>
                Terms and Conditions
            </Button>
        </Stack>
    }
    return <Tooltip title={
        <FormattedMessage id={"privacyPolicy"}/>
    }>
        <Stack
            sx={{
                height: "70px",
                maxHeight: "70px",
                padding: "5px",
                width: "50px",
                transition: "width 1s",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                cursor: "pointer"
            }}

            onClick={onClick}
        >
            <PrivacyTip/>
        </Stack>
    </Tooltip>

}

export default withCommonPropsLite(PrivacyPolicy)
