import React, {useEffect, useState} from 'react';
import style from "./CardItem.module.css"
import {Collapse, Fade, Grid, IconButton, Skeleton, Stack, Tooltip, Typography} from "@mui/material";
import FunctionalButton from "../FunctionalButton/FunctionalButton";
import {getImage, getSerieTVDetail} from "../../constants/CrawfishApi";
import ShowDetail from "../../screen/ShowDetail/ShowDetail";
import {withCommonProps} from "../interfacehook/InterfaceHook";
import ImageTest from "../../asset/image/logo-quix-defaultNoImg.png";
import CardItemExpanded from "./CardItemExpanded";
import {TypographyEllipsed} from "../TypographyEllipsed";
import {HistoryEdu} from "@mui/icons-material";
import {uuidGenerator} from "../../constants/Utility";
import {getConfLocal, removeConfLocal, setConfLocal,} from "../../constants/DbLocal";
import {useHistory, useLocation} from "react-router-dom";
import queryString from "query-string";
import Queue from "../Queue";

let queue = new Queue(true)
const CardItem = (props) => {
    let {
        isDesktop,
        store,
        isMobile,
        refreshData
    } = props
    let [onHover, setHover] = useState(false);
    let [item, setItem] = useState(props.item);

    let {showDialogHandler, descriptionDialogHandler, loadingImages} = store
    let name = item.name,
        id = item.id, first_air_date = item.first_air_date
    let [image, setImage] = useState(
        // getConfLocal(id + "image") || //TODO capire se vogliamo riprovare ad usare l'archiviazione locale delle immagini
        ImageTest);
    let onHoverInTimeout, onHoverOutTimeout
    let location = useLocation();
    let searchValues = queryString.parse(location.search)
    let history = useHistory();
    useEffect(() => {
        (async () => {
            loadingImages.set(item.backdrop_path ?? item.poster_path, false)
            let res = await getSerieTVDetail(props.item.id)
            if (res) {
                setItem(res.data)
                queue.add(async () => {
                    setImage(await getImage(res.data.backdrop_path ?? res.data.poster_path))
                    loadingImages.set(res.data.backdrop_path ?? res.data.poster_path, true)
                })
                //TODO sta roba va sempre in errore
                // await updateShowName(res.data.id, res.data.name)
            }


        })()
        return () => {
            queue.emptyQueue()
        }

    }, [])

    //TODO capire che problema ha
    useEffect(() => {

        (async () => {
            if (
                (searchValues.idShow == id && Object.keys(item).length > 2)
                ||
                (await getConfLocal("openSeries") == id && Object.keys(item).length > 2)
            ) {

                await handleNavigateToDetail()
            }

        })()

    }, [item])


    const handleNavigateToDetail = async () => {
        await setConfLocal("openSeries", id)
        showDialogHandler.set({
            open: true,
            customContent: <Stack>
                <ShowDetail id={id}
                            show={item}
                            refreshData={refreshData}
                            resetDrawerStateLocal={async () => {
                                await removeConfLocal("openSeries")
                                history.push("/")
                                showDialogHandler.reset()
                                descriptionDialogHandler.reset()
                            }}/>
                {isMobile && <CardItemExpanded
                    item={item}
                />}
            </Stack>,
            variant: "persistent",
            customActions: <></>,
            noTitle: true,
            name,
            customStyle: isMobile ? {width: "100%", title: {padding: 0}} : {title: {padding: 0}},
            key: uuidGenerator()
        })
        if (!isMobile) {
            descriptionDialogHandler.set({open: true, item})
        }
        setHover(false)
    }


    let onMouseEnter = () => {
        clearTimeout(onHoverOutTimeout);
        onHoverInTimeout = setTimeout(() => {
            if (!onHover) {
                setHover(true)
            }
        }, 500)
    }

    let onMouseLeave = () => {
        clearTimeout(onHoverInTimeout);
        onHoverOutTimeout = setTimeout(() => {
            if (onHover) {
                setHover(false)
            }

        }, 500)
    }

    return <Grid item
                 xs={12}
                 sm={6}
                 md={4}
                 lg={3}
                 xl={2}
                 sx={{
                     paddingBottom: isMobile ? "10px" : "30px",
                     transition: "1s",
                 }}>

        <Fade
            in={true}
            timeout={1000}
        >
            <Stack
                className={style.container}
                onMouseEnter={isMobile ? () => {
                } : onMouseEnter}
                onMouseLeave={isMobile ? () => {
                } : onMouseLeave}
            >
                <Stack
                    sx={{minHeight: "100%", cursor: "pointer", width: "100%"}}
                    onClick={handleNavigateToDetail}>
                    {!loadingImages[(item.backdrop_path ?? item.poster_path)] ? <Skeleton
                        variant={"rectangular"}
                        sx={{
                            height: "160px",
                            width: "98%"
                        }}/> : <img alt={"show-img"}
                        // src={ImageTest}
                                    src={image}
                                    className={style.img}/>}
                </Stack>
                <Stack className={style.typoContainer}
                       sx={{
                           background: "rgba(0,0,0,0.5)"
                       }}
                       direction={"column"}
                >
                    <Collapse
                        in={!onHover}
                    >
                        <TypographyEllipsed variant={"cardTitle"} fontWeight={"bold"}>{name}</TypographyEllipsed>
                        <Typography variant={"cardTitle"}
                        >
                            {first_air_date && first_air_date.substring(0, 4)}
                        </Typography>
                    </Collapse>

                    <Collapse
                        in={onHover}
                    ><Grid container
                    >
                        <Grid item
                              xs={isMobile ? 12 : 7}
                              sx={{
                                  display: "flex",
                                  justifyContent: isMobile ? "center" : "flex-end",
                                  alignItems: "center"
                              }}
                        >
                            <IconButton
                                color={"primary"}
                                sx={{
                                    padding: "1px"
                                }}
                                onClick={handleNavigateToDetail}
                            >
                                <Tooltip title={"Go to detail"}
                                >
                                    <HistoryEdu
                                        color={"primary"}
                                        sx={{
                                            height: "60px",
                                            width: "60px"
                                        }}
                                        fontSize={"large"}
                                    />
                                </Tooltip>
                            </IconButton>
                        </Grid>
                        <Grid item xs={3} sx={{display: "flex", justifyContent: "flex-end"}}>
                            {!isMobile && onHover && <FunctionalButton
                                key={"watchingButton" + item.id}
                                id={"watchingButton" + item.id}
                                show={item}
                                typeButton={"WATCHING"}
                                typeItem={"show"}
                                refreshData={refreshData}
                            />}
                        </Grid>
                        <Grid item xs={2} sx={{display: "flex", justifyContent: "center"}}>
                            {!isMobile && onHover && <FunctionalButton
                                key={"followingButton" + item.id}
                                id={"followingButton" + item.id}
                                show={item}
                                typeButton={"FOLLOWING"}
                                typeItem={"show"}
                                refreshData={refreshData}
                            />}
                        </Grid>
                    </Grid>
                    </Collapse>

                </Stack>
            </Stack>
        </Fade>
    </Grid>
}

export default withCommonProps(CardItem)