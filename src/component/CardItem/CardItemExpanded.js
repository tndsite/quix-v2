import React from 'react';
import style from "./CardItem.module.css"
import {Grid, Rating, Stack, Typography} from "@mui/material";


const CardItemExpanded = (props) => {
    let {item} = props

    return  <Stack
            className={style.container2}
        >
            <Stack className={style.typoContainer2}
                   direction={"column"}
            >
                <Stack
                    height={"100%"}
                >
                    <Stack direction={"row"} sx={{margin: "20px 0"}}>
                        <Rating name="half-rating-read" value={item.vote_average || 0} precision={0.1}
                                max={10}
                                readOnly/>
                    </Stack>
                    <Typography typography={"h5"} marginBottom={"20px"}>
                        Type: {item.type}
                    </Typography>
                    <Typography typography={"h5"} marginBottom={"20px"}>
                        Watch now: {item.watchNow && item.watchNow.length > 0 && item.watchNow[0]}
                    </Typography>
                    <Typography typography={"h5"} marginBottom={"20px"}>
                        Episodes: {item.number_of_episodes}
                    </Typography>
                    <Typography typography={"h5"} marginBottom={"20px"}>
                        Seasons: {item.number_of_seasons}
                    </Typography>
                    <Typography typography={"h5"} marginBottom={"20px"}>
                        First air date: {item.first_air_date}
                    </Typography>
                    <Typography typography={"h5"} marginBottom={"20px"}>
                        Last air date: {item.last_air_date}
                    </Typography>
                    <Typography variant={"h6"}
                    >
                        {item.overview}
                    </Typography>
                </Stack>
            </Stack>
        </Stack>
}

export default CardItemExpanded