import {Stack, Tooltip} from "@mui/material";
import React, {useEffect} from "react";
import {withCommonPropsLite} from "./interfacehook/InterfaceHook";
import {FormattedMessage} from "react-intl";
import {commonHandledRequest} from "../constants/Utility";
import {APP_LOADING} from "../constants/Constants";
import DarkThemeIcon from "../asset/icons/DarkThemeIcon";
import LightThemeIcon from "../asset/icons/LightThemeIcon";

const translationKey = "settings."
let objLight = {
    "--scrollbar-color": "#6e82a6"
}
let objDark = {
    "--scrollbar-color": "#45474a"
}

export const changeColorBasedOnTheme = (theme) => {

    for (let i of ["--scrollbar-color"]) {
        document.documentElement.style.setProperty(i, theme === "dark" ? objDark[i] : objLight[i]);
    }
}

const ThemePicker = (props) => {
    let {store} = props
    let {
        theme,
    } = store;

    useEffect(() => {
        changeColorBasedOnTheme(theme.style)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    const change = async () => {
        await commonHandledRequest({
            action: async () => {

                let mode = theme.style === "dark" ? "light" : "dark"
                theme.setStyleTmp(mode)
                theme.set()
                theme.setStyle(theme.styleTmp)
                localStorage.setItem("theme", theme.styleTmp)
                changeColorBasedOnTheme(theme.style)

            },
            loadingType: APP_LOADING,
            translationKeyError: "theme",
            translationKeySuccess: "theme"
        })
    }




    return <Tooltip title={
        <FormattedMessage id={"changeTheme"}/>
    }>
        <Stack
            sx={{
                height: "70px",
                maxHeight: "70px",
                padding: "5px",
                width: "50px",
                transition: "width 1s",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                cursor: "pointer"
            }}

            onClick={change}
        >
            {theme.style === "dark" ?
                <DarkThemeIcon/> :
                <LightThemeIcon/>
            }
        </Stack>
    </Tooltip>

}

export default withCommonPropsLite(ThemePicker)