import {CircularProgress, IconButton, Stack, Tooltip} from "@mui/material";
import {AddTask, Favorite, FavoriteBorder} from "@mui/icons-material";
import style from "./FunctionalButton.module.css";
import AddCircle from "../../asset/image/Buttons/AddCircle.png";
import React, {useEffect, useState} from "react";
import {withCommonProps} from "../interfacehook/InterfaceHook";
import {
    movieIsFollowed,
    movieIsToWatch,
    saveFollowedToShowShow,
    saveMovieFollow,
    showIsFollowed,
    showIsToWatch
} from "../../constants/StorageHelper";

const FunctionalButton = (props) => {

    let {store,typeButton, typeItem, isMovie, show,id,key,refreshData} = props
    let Icon, IconActive, title, titleActive, className, alt, apiCall
    let [isActive, setActive] = useState(false);
    let [isPendingActive, setPendingActive] = useState(true);
let {myItems}=store

    useEffect(() => {

        (async () => {
            switch (typeButton) {

                case "FOLLOWING":
                    setActive(isMovie ? await movieIsFollowed(show.id) : await showIsFollowed(show.id))
                    break
                default:
                case "WATCHING":
                    setActive(isMovie ? await movieIsToWatch(show.id) : await showIsToWatch(show.id))
                    break
            }
            setPendingActive(false)
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    switch (typeButton) {
        case "FOLLOWING":
            title = "You are following this " + typeItem + "!"
            titleActive = "Do you want to follow this " + typeItem + "?"
            alt = "addToWatch"
            Icon = () => <FavoriteBorder
                // className={style.followIcon}
                fontSize={"large"}
            />
            IconActive = Favorite
            className = style.followIcon
            apiCall = async (e) => {
                let isFollowedNew = !isActive
                if (isMovie) {
                    await saveMovieFollow(show, isFollowedNew, "follow")
                } else {
                    await saveFollowedToShowShow(show, isFollowedNew, "follow")
                }
                myItems.setRefresh(true)
                setActive(isMovie ? await movieIsFollowed(show.id) : await showIsFollowed(show.id))
            }
            break;
        default:
        case "WATCHING":
            title = "You have this " + typeItem + " in your watch list!"
            titleActive = "Do you want to watch this " + typeItem + "?"
            alt = "addToWatch"
            className = style.favoriteIcon
            Icon = () => <img
                className={className}
                alt={alt}
                src={AddCircle}
            />
            IconActive = AddTask
            apiCall = async (e) => {
                let ToWatchNew = !isActive
                let tmp = "T"
                if (ToWatchNew) {
                    tmp = "W"
                }

                if (isMovie) {
                    await saveMovieFollow(show, tmp, "toWatch")
                } else {
                    await saveFollowedToShowShow(show, tmp, "toWatch")
                }
                myItems.setRefresh(true)
                setActive(isMovie ? await movieIsToWatch(show.id) : await showIsToWatch(show.id))
            }
            break;
    }

    const handleChangeStatus = async () => {
        try {


            setPendingActive(true)

            await apiCall()

            // if(refreshData){
            //     refreshData(show.id)
            // }

        } catch (e) {
            console.error("Errore nel cambio follo/watch", e, e.response)
        } finally {

            setPendingActive(false)
        }
    }


    return <IconButton
        id={id}
        key={key}
        disabled={isPendingActive}
        onClick={async () => {
            await handleChangeStatus()
        }}
    >
        <Tooltip
            title={isActive ? title : titleActive}>
            <Stack>
                {isPendingActive ? <CircularProgress size={40}/> : isActive ?
                    <IconActive
                        className={className}
                        color={"primary"}
                        fontSize={"large"}
                    /> :
                    <Icon
                    />}
            </Stack>
        </Tooltip>
    </IconButton>
}

export default withCommonProps(FunctionalButton)