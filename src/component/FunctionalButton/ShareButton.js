import {
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    FormGroup,
    IconButton,
    MenuItem,
    Radio,
    RadioGroup,
    Stack,
    TextField,
    Tooltip,
    Typography
} from "@mui/material";
import {Close, ContentCopy, Share} from "@mui/icons-material";
import React, {useState} from "react";
import {copyToClipboard} from "../../constants/Utility";
import {observer} from "mobx-react";
import {shareableLinkDataShow, shareableLinkDataSingleSeason} from "../../constants/StorageHelper";

const ShareButton = (props) => {

    let {
        typeButton,
        show,
        seasonSelected,
        episodes,
    } = props
    let Icon, title
    let [shareableLink, setShareableLink] = useState()
    let [openDialog, setOpenDialog] = useState(false);
    let store = window.TNDglobalStoreMobx
    let {showDetail,snackbarHandler} = store

    switch (typeButton) {
        default:
        case "SHARE_SEASON":
            title = "Share links from this season"
            Icon = () => <Share fontSize={"large"}/>
            break;
    }


    const handleChangeStatus = async (ancor) => {
        try {
            showDetail.shareSelectedOption.setSeason(seasonSelected)
            showDetail.redirectToSeason.set(seasonSelected)
            setOpenDialog(true)

        } catch (e) {
            console.error("Error sharing", e, e.response)
        }
    }


    const action = (
        <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={async () => {
                await copyToClipboard(shareableLink)
                snackbarHandler.reset()
            }}
        >
            <ContentCopy/>
        </IconButton>

    );

    return <>
        <Dialog onClose={() => setOpenDialog(false)}
                open={openDialog} PaperProps={{sx: {overflow: "hidden", maxWidth: "100%"}}}>
            <DialogTitle><Stack direction={"row"} justifyContent={"space-between"}
                                alignItems={"center"}><Typography
                variant={"title"}>Share show, seasons and links</Typography>
                <IconButton
                    onClick={() => setOpenDialog(false)}>
                    <Close/>
                </IconButton>
            </Stack></DialogTitle>
            <DialogContent>
                <FormControl>
                    <Typography variant={"semiTitle"}>Oh so you want to share
                        something c:</Typography>
                    <RadioGroup
                        onChange={(event) => {
                            let value = event.target.value
                            showDetail.shareSelectedOption.set(value)
                        }}
                        aria-labelledby="demo-radio-buttons-group-label"
                        defaultValue={showDetail.shareSelectedOption.get}
                        name="radio-buttons-group"
                    >
                        <FormControlLabel value="show" control={<Radio/>}
                                          label="Share all the show's seasons"/>
                        <FormControlLabel value="episodesFromSelectedSeason"
                                          control={<Radio/>}
                                          label={"Share all the links from the selected season (" + showDetail.shareSelectedOption.season + ")"}/>
                    </RadioGroup>
                    <Stack direction={"row"} alignItems={"center"}>
                        <FormGroup

                        >
                            <FormControlLabel
                                control={<Checkbox onChange={(event) => {
                                    let value = event.target.checked
                                    showDetail.redirectToSeason.setRedirect(value)
                                }}
                                />}
                                label={"Redirect to season:"}/>
                        </FormGroup>
                        <TextField
                            sx={{marginTop: "10px", width: "100px"}}
                            label={"Season"}
                            id="selectSeason"
                            disabled={!showDetail.redirectToSeason.redirect}
                            defaultValue={showDetail.redirectToSeason.get}
                            onChange={(event) => {
                                let s = event.target.value
                                showDetail.redirectToSeason.set(s)
                            }}
                            fullWidth
                            required
                            select
                            color={"primary"}
                        >
                            {show.seasons && show.seasons.map((s, sIndex) => {
                                return <MenuItem
                                    value={s.season_number}
                                    key={"seasonNumber" + sIndex}>
                                    {s.season_number}
                                </MenuItem>

                            })}
                        </TextField>
                    </Stack>
                </FormControl>

            </DialogContent>
            <DialogActions><Stack direction={"row"} justifyContent={"space-between"} width={"100%"}
                                  alignItems={"center"}>
                <TextField
                    sx={{marginTop: "10px", width: "100px"}}
                    label={"Season"}
                    id="selectSeason"
                    disabled={showDetail.shareSelectedOption.get === "show"}
                    defaultValue={showDetail.shareSelectedOption.season}
                    onChange={(event) => {
                        let s = event.target.value
                        showDetail.shareSelectedOption.setSeason(s)
                    }}
                    required
                    select
                    color={"primary"}
                >
                    {show.seasons && show.seasons.map((s, sIndex) => {
                        return <MenuItem
                            value={s.season_number}
                            key={"seasonNumber" + sIndex}>
                            {s.season_number}
                        </MenuItem>

                    })}
                </TextField>
                <Button variant={"contained"} color={"secondary"}
                        onClick={async (e) => {
                            e.preventDefault();
                            let tmp
                            switch (showDetail.shareSelectedOption.get) {
                                case "show":
                                default:
                                    tmp = await shareableLinkDataShow(show.id, show.seasons.map(x=>(x.season_number)), episodes.map(x => x.episode_number), showDetail.redirectToSeason.redirect ? showDetail.redirectToSeason.get : false)
                                    break
                                case "episodesFromSelectedSeason":
                                    tmp = await shareableLinkDataSingleSeason(show.id, showDetail.shareSelectedOption.season, episodes.map(x => x.episode_number), showDetail.redirectToSeason.redirect ? showDetail.redirectToSeason.get : false)
                                    break
                            }
                            await copyToClipboard(tmp)
                            setShareableLink(tmp)
                            snackbarHandler.set(
                                "top",
                                "right",
                                true,
                                3000,
                                <>Import link copied to the clipboard {action}</>,
                                "success",
                            );
                            setOpenDialog(false)
                            showDetail.shareSelectedOption.set("show")
                        }}>Share</Button>
            </Stack>
            </DialogActions>
        </Dialog>
        <Tooltip
            title={title}>
                <span>
                <IconButton
                    id={"shareButtonSexy"}
                    key={"shareButton"}
                    onClick={async () => {
                        await handleChangeStatus()
                    }}
                >
                        <Icon/>
                </IconButton>
                </span>
        </Tooltip>
    </>
}

export default observer(ShareButton)