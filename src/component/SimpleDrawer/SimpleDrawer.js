import {DialogActions, DialogTitle, Drawer} from "@mui/material";
import React from "react";


export function SimpleDrawer(props) {
    const {
        isDialogOpen, dialogTitle, dialogContent, handleClose, dialogActions,
        anchor,
        variant,
        customStyle
    } = props;


    return (
        <Drawer
            anchor={anchor || "top"}
            variant={variant || "persistent"}
            open={isDialogOpen}
            onClose={handleClose}
            PaperProps={{sx: {height: "80%", ...customStyle}}}
        >
            {dialogTitle && <DialogTitle sx={{zIndex:1}}>{dialogTitle}</DialogTitle>}
            {dialogContent}
            {dialogActions && <DialogActions>{dialogActions}</DialogActions>}
        </Drawer>
    );
}
