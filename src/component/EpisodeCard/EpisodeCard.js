import {
    deleteLink,
    episodeSeen,
    getEpisodeLink,
    haveSeen,
    haveSeenRawEpisode,
    rawEpisodeSeen,
    saveUrlEpisode
} from "../../constants/StorageHelper";
import React, {forwardRef, useEffect, useState} from "react"
import {
    Avatar,
    Button,
    Checkbox,
    ClickAwayListener,
    DialogContent,
    DialogContentText,
    Fab,
    Grid,
    IconButton,
    OutlinedInput,
    Popper,
    Skeleton,
    Stack,
    Switch,
    Tooltip,
    Typography,
    useMediaQuery,
    Zoom
} from "@mui/material";
import {getEpisodes, getImage} from "../../constants/CrawfishApi";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";
import {
    Add,
    AddLink,
    ArrowForwardIos,
    Close,
    ContentCopy,
    Delete,
    DeleteForever,
    Help,
    MoreVert,
    OpenInNew,
    PlayArrow,
    PlaylistPlay,
    ScreenShare,
    Share,
    SkipNext,
    Visibility
} from "@mui/icons-material";
import {TypographyEllipsed} from "../TypographyEllipsed";
import {copyToClipboard, isDeviceMobile, uuidGenerator, validURL} from "../../constants/Utility";
import {getConfLocal, removeConfLocal, setConfLocal} from "../../constants/DbLocal";
import ImageTest from "../../asset/image/logo-quix-defaultNoImg.png";
import VideoPlayer from "../VideoPlayer/VideoPlayer";
import {withCommonPropsLite} from "../interfacehook/InterfaceHook";
import {useHistory} from "react-router-dom";
import Queue from "../Queue";


let queue = new Queue(true)
const EpisodeCard = (props) => {

    let {
        seasonNumber, episode, rawEpisode, show, isLite,
        isOpened,
    } = props;

    let [seen, setSeen] = useState(false)
    let [episodeLinks, setEpisodeLinks] = useState([])

    let id = episode.id
    let episodeName = episode.name || episode.episodeName;


    let [openMore, setOpenMore] = useState(false);
    let [anchorElMore, setAnchorElMore] = useState(null);
    let canBeOpen = openMore && Boolean(anchorElMore);
    let popperId = canBeOpen ? 'transition-popper' + id : undefined;
    let isMobile = useMediaQuery('(max-width:868px)');
    let [image, setImage] = useState(
        // getConfLocal(episode.still_path + "image") ||
        ImageTest);
    let history = useHistory();
    let searchValues = window.TNDsearchValues && window.TNDsearchValues(history.location.search);

    let store = window.TNDglobalStoreMobx;
    let {keepWatchingItems, dialogHandler, episodeDialogHandler, smallDialogHandler, plyrProps, loadingImages} = store
    let {player} = plyrProps


    useEffect(() => {

        (async () => {
            loadingImages.set?.(episode.still_path, false)
            await refreshData()
            queue.add(async () => {
                setImage(await getImage(episode.still_path))
                loadingImages.set(episode.still_path, true)
            })
            if (
                searchValues.episodeId == id
                &&
                searchValues.episodeLink &&
                !dialogHandler.get.open

            ) {
                player.setSelectedEpisodeLink(searchValues.episodeLink)
                player.setSelectedEpisodeIndex(episode.episode_number)
                await onPlaySingle()
                return
            }
            let tmpLink = await getConfLocal("openLink")
            if (
                tmpLink
                &&
                await getConfLocal("openEpisode") == id
                &&
                !dialogHandler.get.open
            ) {
                player.setSelectedEpisodeLink(tmpLink)
                player.setSelectedEpisodeIndex(episode.episode_number)
                await onPlaySingle()
            }
        })()
        return () => {
            queue.emptyQueue()
        }
    }, [])

    useEffect(() => {


        (async () => {
            if (keepWatchingItems.refresh) {
                await refreshSeen()
                keepWatchingItems.setRefresh(false)
            }

        })()
    }, [keepWatchingItems.refresh])


    const refreshData = async () => {
        try {
            await refreshSeen()
            await refreshEpisodeLink()


        } catch (e) {
            console.error("Error while reloading data", e, e.response)
        } finally {
        }
    }


    const refreshEpisodeLink = async () => {
        let episodeLinks;
        if (rawEpisode) {
            episodeLinks = [episode];
        } else {
            episodeLinks = await getEpisodeLink(show.id, seasonNumber, episode.episode_number);
        }
        setEpisodeLinks(episodeLinks)

        player.setEpisodeList({...episode, links: episodeLinks})

    }

    const refreshSeen = async () => {
        let result;
        if (rawEpisode) {
            result = await haveSeenRawEpisode(show.id, episode.link);
        } else {
            result = await haveSeen(show.id, seasonNumber, episode.episode_number);
        }
        let seen = result > 0;
        setSeen(seen)

    }

    const handlerSeenCheckbox = async (event) => {
        try {
            let checked = event.target.checked;
            if (rawEpisode) {
                let input = {
                    idShow: show.id,
                    episodeName: episodeName,
                    link: episode.link,
                }
                await rawEpisodeSeen(input, checked);
            } else {
                let input = {
                    tvShowId: show.id,
                    seasonNumber,
                    showName: show.name,
                    showDetail: JSON.stringify(show),
                    episodeNumber: episode.episode_number,
                    episodeName: episode.name,
                    episodeImage: episode.still_path,
                }
                await episodeSeen(input, checked);
            }


            await refreshSeen()
        } catch (e) {
            console.error("Failed to store data:", e)
        }
    }

    const EpisodeAvatar = ({sx}) => {


        return !loadingImages[episode.still_path] ? <Skeleton
            variant={"circular"}
            sx={{
                height: "40px",
                width: "40px"
            }}/> : <Avatar alt={"item" + episode.name}
                           src={image}
                           sx={{cursor: "pointer", ...sx}}
        />
    }

    const EpisodeAvatarSeen = () => {
        return <Stack sx={{position: "relative"}}>
            <EpisodeAvatar/>
            <Stack
                justifyContent={"center"}
                alignItems={"center"}
                sx={{
                    position: "absolute",
                    backgroundColor: "rgba(0, 0, 0,0.6)",
                    margin: "auto",
                    top: 0,
                    bottom: 0,
                    right: 0,
                    left: 0
                }}>
                <Visibility fontSize={"small"}/>
            </Stack>

        </Stack>
    }

    const DeleteLinksButton = ({isPopover}) => {

        let CustomButton = forwardRef((props, ref) => <IconButton
            ref={ref}
            {...props}/>)
        if (isPopover) {
            CustomButton = forwardRef((props, ref) => <Button
                ref={ref}
                variant={"contained"}
                {...props}/>)
        }
        return <Tooltip title={"Delete links to the episode N°° " + episode.episode_number}
                        id={"tooltipDeleteLinksEpisode"}><CustomButton
            onClick={() => {
                smallDialogHandler.set({
                    isDialog: true,
                    paperSizeIsSmallest: true,
                    open: true,
                    title: <Stack direction={"row"} justifyContent={"space-between"} alignItems={"flex-start"}>
                        <Stack>
                            <Typography variant={"title"}>Delete links from the episode </Typography>
                            <Typography
                                variant={"cardTitle"}>{"N°" + episode.episode_number + " - " + (episode.name && episode.name)}</Typography>
                        </Stack>
                        <IconButton onClick={() => smallDialogHandler.reset()}>
                            <Close/>
                        </IconButton>
                    </Stack>,
                    customContent: <DialogContent><Stack>Are you sure to delete all the episode links?
                        ({episodeLinks.length})

                        {episodeLinks.map((x, xIndex) => {
                            return <Stack key={"episodeLink" + xIndex} direction={"row"} justifyContent={"flex-start"}
                                          marginTop={"10px"} alignItems={"center"}>
                                {(xIndex + 1)})
                                <TypographyEllipsed
                                    tooltip={x.link}
                                >
                                    {(new URL(x.link)).hostname.replace('www.', '')}
                                </TypographyEllipsed>

                            </Stack>
                        })}
                    </Stack></DialogContent>,
                    customActions: <Button variant={"contained"} color={"secondary"} onClick={async () => {
                        for (let index in episodeLinks) {
                            let x = episodeLinks[index]
                            await deleteLink(show.id, x.link, seasonNumber, episode.episode_number);
                        }
                        await refreshEpisodeLink()
                        smallDialogHandler.reset()
                    }}>Delete</Button>,
                    handleClose: () => smallDialogHandler.reset()
                })
            }}
        >
            <Delete/>
        </CustomButton></Tooltip>
    }

    const WatchWithFriendsButton = () => {
        return <Tooltip title={"Watch the episode N° " + episode.episode_number + " with your friends!"}
                        id={"tooltipWatchWithFriends"}><Button
            variant={"contained"}
            onClick={() => {

                smallDialogHandler.set({
                    isDialog: true,
                    paperSizeIsSmallest: true,
                    open: true,
                    title: <Stack direction={"row"} justifyContent={"space-between"} alignItems={"flex-start"}>
                        <Stack>
                            <Typography variant={"title"}>Room setup</Typography>
                            <Typography
                                variant={"cardTitle"}>{"N°" + episode.episode_number + " - " + (episode.name && episode.name)}</Typography>
                        </Stack>
                        <IconButton onClick={() => smallDialogHandler.reset()}>
                            <Close/>
                        </IconButton>
                    </Stack>,
                    customContent: <DialogContent><Stack>

                    </Stack></DialogContent>,
                    customActions: <Button variant={"contained"} color={"secondary"} onClick={async () => {

                        smallDialogHandler.reset()
                    }}>Create the room</Button>,
                    handleClose: () => smallDialogHandler.reset()
                })
            }}
        >
            <ScreenShare/>
        </Button></Tooltip>
    }

    const AddLinksButton = ({isPopover}) => {

        let CustomButton = forwardRef((props, ref) => <IconButton
            ref={ref}
            {...props}/>)
        if (isPopover) {
            CustomButton = forwardRef((props, ref) => <Button
                ref={ref}
                variant={"contained"}
                {...props}/>)
        }

        return <Tooltip title={"Add links to the episode N°" + episode.episode_number}
                        id={"tooltipAddLinksEpisode"}><CustomButton
            onClick={() => {
                smallDialogHandler.set({
                    isDialog: true,
                    paperSizeIsSmallest: true,
                    customActions: <></>,
                    open: true,
                    title: <Stack direction={"row"} justifyContent={"space-between"}
                                  alignItems={"flex-start"}><Stack><Typography variant={"title"}>Add links to
                        the episode </Typography>
                        <Typography
                            variant={"cardTitle"}>{"N°" + episode.episode_number + " - " + (episode.name && episode.name)}</Typography>
                    </Stack>
                        <IconButton onClick={() => smallDialogHandler.reset()}>
                            <Close/>
                        </IconButton>
                    </Stack>,
                    customContent: <AddLinksContent
                        {...{id, show, refreshEpisodeLink, seasonNumber, episodeName, episode, rawEpisode}}
                    />,
                    handleClose: () => smallDialogHandler.reset(),
                    customStyle: {
                        paper: {
                            maxWidth: "unset!important",
                            width: "unset",
                        },
                        actions: {
                            padding: "0!important"
                        }
                    }
                })
            }}
        >
            <AddLink/>
        </CustomButton></Tooltip>
    }

    const onPlaySingle = async () => {
        await setConfLocal("openLink", player.selectedEpisodeLink)
        await setConfLocal("openEpisode", player.episodeList[player.selectedEpisodeIndex].id)
        dialogHandler.set({
            open: true,
            customContent: <>
                <VideoPlayerWrapper
                    key={uuidGenerator()}
                    {...{
                        onPlaySingle,
                        refreshSeen,
                        seasonNumber,
                        rawEpisode,
                        episode: player.episodeList[player.selectedEpisodeIndex],
                        show
                    }}
                /></>,
            customActions: <></>,
            anchor: "top",
            handleClose: async () => {
                await removeConfLocal("openLink")
                await removeConfLocal("openEpisode")
                dialogHandler.reset()
                episodeDialogHandler.reset()
            },
            customStyle: {height: "100%", width: "100%"}
        })
        episodeDialogHandler.reset()
    }

    return <Stack
        id={"EpisodeCard" + id + (isLite ? "lite" : "full")}


        direction="row" justifyContent={"space-between"}
        sx={{
            transition: "0.5s",
            background: isOpened ? "rgba(63, 126, 192,0.5)" : "unset"
        }}
        alignItems={"center"}
    >
        <Checkbox
            size={"small"}
            checkedIcon={<EpisodeAvatarSeen/>}
            icon={<EpisodeAvatar/>}
            checked={seen}

            onChange={async (event) => {
                await handlerSeenCheckbox(event)
            }}
        />
        <Stack width={"100%"}>
            <TypographyEllipsed variant={"cardTitle"}>
                {episode.episode_number + ". " + (episode.name && episode.name)}
            </TypographyEllipsed>
        </Stack>
        <Stack direction={"row"}

        >
            {!isLite && (seen || (!seen && episodeLinks.length > 0)) &&

                <Stack>
                    <Tooltip title={"More options, delete, assing links to the episode"}>
                        <IconButton sx={{marginRight: seen ? 0 : "10px"}}
                                    onClick={(event) => {
                                        setAnchorElMore(event.currentTarget);
                                        setOpenMore((previousOpen) => !previousOpen);
                                    }}
                        >
                            <MoreVert/>
                        </IconButton>
                    </Tooltip>
                </Stack>

            }

            <Popper
                sx={{zIndex: 1200}}
                id={popperId} open={openMore} anchorEl={anchorElMore}
            >
                <ClickAwayListener onClickAway={() => {
                    setOpenMore(false)
                    setAnchorElMore(null)
                }}>
                    <Grid container sx={{background: "black"}} key={"containerButtons"}>
                        {seen && episodeLinks.length > 0 &&
                            <Grid item xs={12} sx={{display: "flex", justifyContent: "center", padding: "10px 0"}}>
                                <PlayEpisodeButton
                                    isPopover={true}
                                    episodeIndex={episode.episode_number}
                                    episodeLinks={episodeLinks}
                                    onPlaySingle={async () => {
                                        setOpenMore(false)
                                        setAnchorElMore(null)
                                        await onPlaySingle()
                                    }}
                                    id={id}
                                />
                            </Grid>}
                        <Grid item xs={12} sx={{display: "flex", justifyContent: "center", paddingBottom: "10px"}}>
                            <Stack direction={"row"}>
                                <AddLinksButton
                                    isPopover={true}
                                />
                            </Stack>
                        </Grid>
                        <Grid item xs={12} sx={{display: "flex", justifyContent: "center", paddingBottom: "10px"}}>
                            <Stack direction={"row"}>
                                <WatchWithFriendsButton/>
                            </Stack>
                        </Grid>
                        {episodeLinks.length > 0 &&
                            <Grid item xs={12} sx={{display: "flex", justifyContent: "center", paddingBottom: "10px"}}>
                                <DeleteLinksButton
                                    isPopover={true}
                                />
                            </Grid>}
                    </Grid>
                </ClickAwayListener>
            </Popper>

            {(isLite || !seen) && episodeLinks.length > 0 &&
                <PlayEpisodeButton
                    episodeIndex={episode.episode_number}
                    episodeLinks={episodeLinks}
                    onPlaySingle={onPlaySingle}
                    id={id}
                />
            }
            {!seen && episodeLinks.length === 0 && <AddLinksButton/>
            }
        </Stack>
    </Stack>

}

export default withCommonPropsLite(EpisodeCard)

const LinksList = withCommonPropsLite((props) => {
    let {store, onPlaySingle, episodeIndex} = props
    let {plyrProps, smallDialogHandler} = store
    let {player} = plyrProps
    let episodeLinks = player.episodeList[episodeIndex].links
    return <Grid container sx={{background: "black", padding: "0 0 5px 0"}} key={"containerButtons"}>
        {episodeLinks.map((xx, xxIndex) => {
            return <Grid key={"link" + xxIndex} item xs={12} sx={{margin: "5px 5px 0 5px"}}>
                <Button
                    fullWidth
                    variant={"contained"}
                    color={"primary"}
                    endIcon={<PlayArrow/>}
                    style={{fontSize: "10px"}}
                    onClick={() => {
                        if (episodeLinks[0].isPlayable === false) {
                            window.open(episodeLinks[0].link, "_blank")
                        } else {
                            player.setSelectedEpisodeLink(xx.link)
                            smallDialogHandler.reset()
                            onPlaySingle()
                        }
                    }}
                >
                    {(new URL(xx.link)).hostname.replace('www.', '')}
                </Button>
            </Grid>
        })}
    </Grid>
})

const PlayEpisodeButton = forwardRef((props, refBig) => {
    let {
        isPopover,
        onPlaySingle,
        customIcon, episodeIndex
    } = props
    let store = window.TNDglobalStoreMobx
    let {plyrProps, smallDialogHandler} = store
    let {player} = plyrProps

    let episodeLinks = props.episodeLinks || (player.episodeList[player.selectedEpisodeIndex] && player.episodeList[player.selectedEpisodeIndex].links)

    let CustomButton = forwardRef((props, ref) => <IconButton
        {...props}
        ref={ref}
    />)
    if (isPopover) {
        CustomButton = forwardRef((props, ref) => <Button
            {...props}
            ref={ref}
            variant={"contained"}
        />)
    }

    return <Stack ref={refBig}>
        <Tooltip title={"Play the episode"}>
            <CustomButton
                onClick={() => {
                    player.setSelectedEpisodeIndex(episodeIndex)
                    let episodeLinks = player.episodeList[episodeIndex].links
                    if (episodeLinks.length > 1) {
                        smallDialogHandler.set({
                            isDialog: true,
                            paperSizeIsSmallest: true,
                            customActions: <></>,
                            open: true,
                            title: <Stack direction={"row"} justifyContent={"space-between"}
                                          alignItems={"flex-start"}><Stack><Typography variant={"title"}>Select
                                which link to
                                play </Typography>
                            </Stack>
                                <IconButton onClick={() => {

                                    smallDialogHandler.reset()
                                }

                                }>
                                    <Close/>
                                </IconButton>
                            </Stack>,
                            customContent: <LinksList
                                {...{
                                    onPlaySingle, episodeIndex
                                }}
                            />,
                            handleClose: () => smallDialogHandler.reset()
                        })
                    } else {
                        if (episodeLinks[0].isPlayable === false) {
                            window.open(episodeLinks[0].link, "_blank")
                        } else {
                            player.setSelectedEpisodeLink(episodeLinks[0].link)
                            onPlaySingle()
                        }

                    }
                }}
            >
                {customIcon ? customIcon : episodeLinks.length > 1 ? <PlaylistPlay/> : <PlayArrow
                />}
            </CustomButton>
        </Tooltip>
    </Stack>
})

const VideoPlayerWrapper = withCommonPropsLite((props) => {

    let {
        seasonNumber,
        episode, show, history,
        searchValues,
        store,
        onPlaySingle
    } = props;
    let {keepWatchingItems, plyrProps, dialogHandler, episodeDialogHandler, smallDialogHandler, snackbarHandler} = store
    let {player} = plyrProps
    let isMobile = useMediaQuery('(max-width:868px)');

    let episodeNumber = episode.episode_number, episodeName = episode.name
    return <>
        <VideoPlayer
            idToRender={"video"}
            episodeProps={{
                id: show.id,
                seasonNumber,
                episodeNumber,
                selectedLink: player.selectedEpisodeLink,
                selectedMagnet: false
            }}
            topbarContent={
                <Stack direction={"row"} justifyContent={"space-between"}
                       alignItems={"center"}
                       height={"60px"}
                       padding={"10px"}
                >
                    <Stack direction={"row"} alignItems={"flex-start"}>
                        <Stack direction={"row"} alignItems={"flex-start"}
                               sx={{cursor: "pointer", padding: "10px"}}
                               onClick={async () => {
                                   await copyToClipboard(window.location.origin + "/"
                                       + "?idShow=" + show.id
                                       + "&seasonNumber=" + seasonNumber
                                       + "&episodeId=" + episode.id
                                       + "&episodeLink=" + player.selectedEpisodeLink
                                   )
                                   snackbarHandler.set(
                                       "top",
                                       "right",
                                       true,
                                       3000,
                                       "Sharable link copied to clipboard",
                                       "success",
                                   );
                               }}>
                            <Share/>
                            <Typography
                                sx={{marginLeft: "10px"}}
                                variant={"title"}>
                                {episodeNumber + " - " + (episodeName && episodeName)}</Typography>
                        </Stack>
                        {
                            isDeviceMobile() &&
                            <Zoom
                                in={player.isControlsShowed}
                            >
                                <Fab
                                    size="small"
                                    color={"primary"}
                                    sx={{position: "fixed", left: "20px", bottom: "50px", zIndex: 2}}
                                    onClick={async () => {
                                        player.setDisableTutorialMobile(!player.disableTutorialMobile)
                                    }}
                                >
                                    <Help/>
                                </Fab>
                            </Zoom>}
                        {(episodeNumber + 1) < Object.keys(player.episodeList).length + 1
                            &&
                            player.episodeList[(episodeNumber + 1)]
                            &&
                            player.episodeList[(episodeNumber + 1)].links.length > 0 && <Zoom
                                in={player.isControlsShowed}
                            >
                                <Tooltip title={"Next episode"}>
                                    <Fab
                                        onClick={() => {
                                            episodeNumber += 1
                                            player.setSelectedEpisodeIndex(episodeNumber)
                                            let episodeLinks = player.episodeList[episodeNumber].links
                                            if (episodeLinks.length > 1) {
                                                smallDialogHandler.set({
                                                    isDialog: true,
                                                    paperSizeIsSmallest: true,
                                                    customActions: <></>,
                                                    open: true,
                                                    title: <Stack direction={"row"} justifyContent={"space-between"}
                                                                  alignItems={"flex-start"}><Stack><Typography
                                                        variant={"title"}>Select
                                                        which link to
                                                        play </Typography>
                                                    </Stack>
                                                        <IconButton onClick={() => {

                                                            smallDialogHandler.reset()
                                                        }

                                                        }>
                                                            <Close/>
                                                        </IconButton>
                                                    </Stack>,
                                                    customContent: <LinksList
                                                        {...{
                                                            onPlaySingle, episodeIndex: episodeNumber
                                                        }}
                                                    />,
                                                    handleClose: () => smallDialogHandler.reset()
                                                })
                                            } else {

                                                player.setSelectedEpisodeLink(episodeLinks[0].link)
                                                onPlaySingle()

                                            }
                                        }}
                                        size="small"
                                        color={"primary"}
                                        sx={{position: "fixed", right: "70px", bottom: "50px", zIndex: 2}}
                                    >
                                        <SkipNext/>
                                    </Fab>
                                </Tooltip>

                            </Zoom>}
                        <Zoom
                            in={player.isControlsShowed}
                        >
                            <Tooltip title={"Episode list"}>
                                <Fab
                                    size="small"
                                    color={"primary"}
                                    sx={{position: "fixed", right: "20px", bottom: "50px", zIndex: 2}}
                                    onClick={async () => {
                                        episodeDialogHandler.set({
                                            open: true,
                                            title: <Button color={"primary"}
                                                           sx={{height: "30px",}}
                                                           variant={"contained"}
                                                           onClick={async () => {
                                                               await removeConfLocal("openLink")
                                                               await removeConfLocal("openEpisode")
                                                               episodeDialogHandler.reset()
                                                           }}><ArrowForwardIos/>

                                            </Button>
                                            ,
                                            customActions: <></>,
                                            customContent: <Stack width={"100%"} height={"100%"}
                                                                  sx={{overflow: "auto"}}>
                                                <EpisodesLinkLite
                                                    {...{
                                                        id: episode.id,
                                                        show,
                                                        seasonNumber,
                                                    }}
                                                /></Stack>,
                                            handleClose: async () => {
                                                await removeConfLocal("openLink")
                                                await removeConfLocal("openEpisode")
                                                episodeDialogHandler.reset()
                                            },
                                            anchor: "right",
                                            customStyle: {height: "100%", width: isMobile ? "100%" : "40%"}
                                        })
                                    }}
                                >
                                    <PlaylistPlay/>
                                </Fab>
                            </Tooltip>
                        </Zoom>


                    </Stack>

                    <IconButton onClick={async () => {
                        await removeConfLocal("openLink")
                        await removeConfLocal("openEpisode")
                        dialogHandler.reset()
                        episodeDialogHandler.reset()

                        keepWatchingItems.setRefresh(true)
                        if (
                            (
                                searchValues.episodeLink
                            )
                        ) {
                            history.push("/")
                        }
                    }}>
                        <Close/>
                    </IconButton>
                </Stack>
            }
        />

    </>
})

const AddLinksContent = ({id, show, refreshEpisodeLink, seasonNumber, episodeName, episode, rawEpisode}) => {

    let episodeNumber = episode.episode_number || parseInt(episode.episodeName)
    let [episodeLinks, setEpisodeLinks] = useState([])
    let [isPlayable, setIsPlayable] = useState(true)


    useEffect(() => {
        (async () => {
            await refreshEpisodeLinkLocal()
        })()
    }, [])

    const refreshEpisodeLinkLocal = async () => {
        let episodeLinks;
        if (rawEpisode) {
            episodeLinks = [episode];
        } else {
            episodeLinks = await getEpisodeLink(show.id, seasonNumber, episode.episode_number);
        }

        setEpisodeLinks(episodeLinks)

    }
    const handleAdd = async () => {
        let inputUrl = document.getElementById("linkInput").value
        if (validURL(inputUrl)) {
            let input = {
                tvShowId: show.id,
                episodeNumber,
                seasonNumber,
                showName: show.name,
                episodeName,
                link: inputUrl,
                isPlayable
            }
            await saveUrlEpisode(input);
            await refreshEpisodeLinkLocal()
            await refreshEpisodeLink()
        }
    }

    return <DialogContent>
        <DialogContentText color={"secondary"}>
            Add a link and press "plus" or "enter"
        </DialogContentText>
        {episodeLinks.map((x, number) => <Stack
                direction={"row"}
                justifyContent={"space-between"}
                alignItems={"center"}
                key={"li" + x.link}
                sx={{padding: "5px",}}
            >
                <TypographyEllipsed tooltip={x.link}>
                    {x.link}
                </TypographyEllipsed>
                <Tooltip title={"Open link in a new tab!"}>
                    <IconButton
                        href={x.link}
                    >
                        <OpenInNew
                            color={"secondary"}/>
                    </IconButton>
                </Tooltip>
                <Tooltip title={"Copy link to clipboard!"}>
                    <IconButton onClick={async () => {
                        await copyToClipboard(x.link)
                    }}>
                        <ContentCopy color={"secondary"}/>
                    </IconButton>
                </Tooltip>
                <Tooltip title={"Delete link"}>
                    <IconButton onClick={async () => {
                        await deleteLink(show.id, x.link, seasonNumber, episode.episode_number);
                        await refreshEpisodeLinkLocal()
                        await refreshEpisodeLink()

                    }}>
                        <DeleteForever color={"secondary"}/>
                    </IconButton>
                </Tooltip>
            </Stack>
        )}
        <OutlinedInput
            autoFocus
            id="linkInput"
            fullWidth
            color={"secondary"}
            onKeyDown={async (event) => {
                if (event.code === "Enter" || event.code === "NumpadEnter") {
                    await handleAdd()
                    document.getElementById("linkInput").value = ""
                }
            }}
            endAdornment={
                <Button
                    variant={"contained"}
                    onClick={async () => {
                        await handleAdd()
                        document.getElementById("linkInput").value = ""

                    }}
                    color="primary">
                    <Add/>
                </Button>
            }
        />
        <Stack direction={"row"} width={"100%"} justifyContent={"flex-end"} alignItems={"center"}>
            <Switch checked={isPlayable} onChange={(c) => {
                setIsPlayable(c.target.checked)
            }}/>
            <Typography>Is the link playable?</Typography>
        </Stack>

    </DialogContent>
}

const EpisodesLinkLite = (props) => {

    let {
        show, seasonNumber, id,
    } = props

    let [episodes, setEpisodes] = useState([])
    let [loader, stopLoading] = useLoadingSystem(true, {enabled: true, height: "40px", width: "100%"})


    useEffect(() => {
        (async () => {
            try {
                await refreshEpisodes()
                setTimeout(() => {

                    let myElement = document.getElementById("episodeLite" + id);
                    if (myElement) {
                        myElement.scrollIntoView({
                                behavior: "smooth",
                                block: "start",
                                inline: "nearest"
                            }
                        )
                    }

                }, 500)
            } catch (e) {
                console.error("errore recuper dettaglio serie", e, e.response)
            }
        })()

    }, [])

    const refreshEpisodes = async () => {
        let response = await getEpisodes(seasonNumber, show.id);
        setEpisodes(response.data.episodes)
        stopLoading()
    }


    return loader ? loader : <Grid container
                                   key={"episodeContainerPlayer"}
                                   id={"episodeContainerPlayer"}
    >
        {episodes.map(x => {
            return <Grid item xs={12}
                         sx={{padding: "2px"}}
                         key={"episodeLite" + x.id}
                         id={"episodeLite" + x.id}
            ><EpisodeCard

                id={x.id}
                show={show}
                episode={x}
                seasonNumber={seasonNumber}
                episodes={episodes}
                isLite={true}
                isOpened={id === x.id}
            />
            </Grid>
        })}
    </Grid>
}
