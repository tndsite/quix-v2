import {
    Button,
    Dialog,
    DialogContent,
    DialogContentText,
    DialogTitle,
    IconButton,
    Stack,
    Step,
    StepContent,
    StepLabel,
    Stepper,
    Typography
} from "@mui/material";
import {Close} from "@mui/icons-material";
import {useEffect, useState} from "react";
import {getConfLocal, setConfLocal} from "../../constants/DbLocal";
import useLoadingSystem from "../LoadingPage/useLoadingSystem";

const FirstAccessDialog = (props) => {

    let [activeStep, setActiveStep] = useState(0)
    let [checkFirstTime, setCheckFirstTime] = useState(getConfLocal("FIRST_TIME_IN_QUIX") || true)
    let [loading, stopLoading] = useLoadingSystem(true, {enabled: true, height: "40px", width: "100%"})

    useEffect(() => {

        (async () => {
            setCheckFirstTime(await getConfLocal("FIRST_TIME_IN_QUIX"))
            stopLoading()
        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    let step = 3;
    return loading ? loading : <Dialog
        disableEscapeKeyDown={true}

        open={checkFirstTime !== "false"}
        onClose={() => {
            setCheckFirstTime("false")
        }
        } aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
            <Stack direction={"row"} justifyContent={"space-between"}>
                Welcome to QuiX!
                {checkFirstTime ? (
                    <IconButton aria-label="close" onClick={() => setCheckFirstTime("false")}>
                        <Close/>
                    </IconButton>
                ) : null}
            </Stack>
        </DialogTitle>
        <DialogContent>
            <Stepper activeStep={activeStep} orientation="vertical">
                <Step key={"step1"}>
                    <StepLabel><Typography>A little about us</Typography></StepLabel>
                    <StepContent>
                        <DialogContentText style={{
                            maxHeight: "200px",
                            overflowY: "auto"
                        }}
                                           color={"primary"}>
                            Hi guys, we are developing a web portal to track tv series/film/anime/manga and
                            more.
                            Now we are in pre-apha phase but we want to share some result with the
                            community.<br/><br/>
                            The project name it's "QuiX", but it can change.<br/>
                            The proposal of the project is to develop a website like "trakt.tv" or
                            "simkl.com" but
                            with serverless architecture.<br/>
                            The principle is simple, you have a list of media data, taken from TMDB, you can
                            follow
                            a tv serie and check if you have seen episodes.<br/>
                            Then you can add to any episode a list of streaming link like netflix, youtube
                            or other
                            media.<br/>
                            If you add a link with a direct mp4 file you can actually reproduce it in the
                            website.<br/>
                            And finally you can share all the links of the tv series you added to your
                            collection
                            with friends with a simple click.<br/>
                            For now all the data are stored locally in your PC so until you login with
                            google all
                            the data is on a single device.<br/>
                            The login with google will request the permissions to read/write on google
                            drive, thanks
                            to that all your data is saved on your personal google drive account and nothing
                            remains
                            in the server.<br/>
                            This project is focused on privacy so no data are stored in the server, all the
                            operations are made client side (research,saving and other).<br/><br/>

                            As said in the beginning we are in pre-alpha at the moment, so you need to send
                            us your
                            gmail address and we will grant you the permissions to login with your google
                            account.<br/><br/>

                            In the near future we are going to work on other features like:<br/>
                            - Share screen/video with friends and see tv show togheter (Peer-to-peer
                            technology)<br/>
                            - Better interface and personalized interface<br/>
                            - Add specific support to other DB and not only tv show from tmdb (Like anime
                            from
                            MyAnilist)<br/><br/>

                            Other project feature will be:<br/>
                            - Support for p2p direct streaming (It's a little bit tricky in a web app maybe
                            a
                            desktop version)<br/>
                            - Android app<br/>
                            - Integration of official link of tv show like netfilx/primevideo<br/>
                            - Auto fetch and import from other service like trakt.tv<br/>
                            - Save data not only on google drive but other service (Personal drive, home
                            nas,
                            onedrive and other service)<br/><br/><br/>


                            We have many other ideas, we just need time.<br/>
                            The project maybe in the future will become open source, but for now it's
                            private just
                            because we have no time to maintain an official project on github.<br/>
                            We would like to keep the service ads-free, also because thanks to the
                            serverless
                            architecture we actually don't have fee to pay.
                        </DialogContentText>
                    </StepContent>
                </Step>
                <Step key={"step2"}>
                    <StepLabel><Typography>Your data, your account</Typography></StepLabel>
                    <StepContent>
                        <DialogContentText color={"primary"}>
                            You can choose to login with Google clicking on the top right corner or continue
                            like a guest. <br/>
                            A guest cannot save on the cloud, if you log with google all the date will be
                            saved on google drive, this is why we ask permission to write on it
                        </DialogContentText>
                    </StepContent>
                </Step>
                <Step key={"step3"}>
                    <StepLabel><Typography>Follow your favourite
                        series</Typography></StepLabel>
                    <StepContent>
                        <DialogContentText color={"primary"}>
                            Here you can track your favourite tv show, share with you friend and watch it
                            with them
                        </DialogContentText>
                    </StepContent>
                </Step>
                <Step key={"step4"}>
                    <StepLabel><Typography>Support us</Typography></StepLabel>
                    <StepContent>
                        <DialogContentText color={"primary"}>
                            We don't have ads or premium, share our application with all the people you can
                            and give us some feedback to support us ☻
                        </DialogContentText>
                    </StepContent>
                </Step>
                <Stack direction={"row"} justifyContent={"space-between"} marginTop={"20px"}>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={activeStep === 0}
                        onClick={() => {
                            setActiveStep(activeStep - 1)
                        }}
                    >
                        Back
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={async () => {
                            if (activeStep === step) {
                                await setConfLocal("FIRST_TIME_IN_QUIX", "false")
                                setCheckFirstTime("false")
                            } else {
                                setActiveStep(Math.min(activeStep + 1, step))
                            }
                        }}
                    >
                        {activeStep === step ? "Close" : "Next"}
                    </Button>
                </Stack>
            </Stepper>
        </DialogContent>
    </Dialog>
}
export default FirstAccessDialog