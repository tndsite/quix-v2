import {ClickAwayListener, Stack, Tooltip, Typography} from "@mui/material";
import React, {useState} from "react";

export const TypographyEllipsed = ({children, variant, fontWeight, sx, ellipsis,tooltip}) => {
    let [open, setOpen] = useState(false)
    if (!children) {
        return null
    }
    return <ClickAwayListener onClickAway={() => setOpen(false)}><Tooltip
        PopperProps={{
            sx: {
                "& .MuiTooltip-tooltip": {
                    backgroundColor: 'black'
                },
            },
        }}
        open={open}
        title={tooltip||children}><Stack

        sx={{cursor: "help",
            // width:"80%"
        }}
        onClick={() => setOpen(!open)}
    ><Typography variant={variant || "cardTitle"} fontWeight={fontWeight || "bold"} sx={{
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        display: '-webkit-box',
        WebkitLineClamp: ellipsis || '1',
        WebkitBoxOrient: 'vertical',
        wordWrap:"break-word",
        ...sx
    }}
    >{children}</Typography>
    </Stack>
    </Tooltip>
    </ClickAwayListener>
}