import React, {useState} from "react"
import {uuidGenerator} from "../../constants/Utility";
import {TypographyEllipsed} from "../TypographyEllipsed";
import {Button, Grid} from "@mui/material";

const UploadButton = (props) => {

    let [uploadValue, setUploadValue] = useState();
    let {
        sizeXS = 12, uploadButtonOnClick = () => {
        }, removeButtonOnClick = () => {
        }, customSize
    } = props


    let value = uploadValue;
    if (uploadValue === "") {
        value = uploadValue
    }
    return <Grid
        key={uuidGenerator()}
        id={"uploadButton"}
        container
        item xs={sizeXS}
        {...customSize}>
        <Grid item xs={12} style={{display: "flex", justifyContent: "flex-start"}}>
            <Button
                fullWidth
                variant={"contained"}
                color={"primary"}
                component={"label"}
            >
                UPLOAD
                <input
                    accept="file/!*,application/*,audio/*,video/*,image/*"
                    style={{display: "none"}}
                    id="icon-button-file"
                    type="file"
                    name="imageProfilePath"
                    onChange={async (event) => {
                        let uploadValue = await uploadButtonOnClick(event);
                        document.getElementById("linkAreaTextJSON").value=uploadValue
                        setUploadValue(uploadValue)
                    }}
                    onClick={(event) => {
                        event.target.value = null
                    }}
                />
            </Button>
        </Grid>
        <Grid item xs={12} sm={12} md={4} style={{display: "flex", justifyContent: "flex-start"}}>
            <TypographyEllipsed
                ellipsis={3}
            >{value}</TypographyEllipsed>
        </Grid>
        {value && value !== "" &&
            <Grid item xs={12} sm={12} md={4} style={{display: "flex", justifyContent: "flex-start"}}>
                <Button onClick={async (event) => {
                    let uploadValue = await removeButtonOnClick(event);
                    setUploadValue(uploadValue)
                }} variant="text"
                        color={"secondary"}>
                    Remove</Button>
            </Grid>
        }
    </Grid>

}


export default UploadButton