const fs = require("fs")
const path = require("path");
const moment = require("moment");

let xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">"

xml += "<url><loc>https://quix.tnl.one/</loc><lastmod>"+moment().format("YYYY-MM-DD")+"</lastmod><changefreq>weekly</changefreq><priority>0.8</priority></url>"
xml += "</urlset>"

fs.writeFileSync(path.join(__dirname, "../public/sitemap.xml"), xml, (err) => {
    if (err) throw err;
    console.info('The file has been saved!');
})
