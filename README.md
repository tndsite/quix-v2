![Quix](src/asset/image/logo-quix.png)

**Quix** is a free and alternative website to track you favourite media and also a 
custom player. 
It focuses on maintain privacy and personal information on you, 
no data is stored on a server is all in your indexedDB browser or 
synced in Google Drive personal space. 
(In the future will be added more secure cloud space)

Available features are:

- Track tv series and anime that you are watching
- Cloud sync with Google Drive (_Press the sign-in button in the top right corner, it can have still some bug_)
- Connect the direct link to the episode, watch and keep a trace of the seen portion

### How to use QuiX

Access the latest version of the website here. [QuiX](https://quix.tnl.one)

---

### How to use QuiX as media player

**QuiX** is easy to embed as a video player that keep a trace of the seen portion!

Checkout an example of embedded player here [Embedded video](https://quix-embeddable.stackblitz.io)

```javascript
<iframe
        width={'100%'}
        height={'100%'}
        allowfullscreen={true}
        src="https://quix.tnl.one/?isEmbedded=true&episodeLink=https://storage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
/>
```

Make a fork and try it yourself with your video [Stackblitz example](https://stackblitz.com/edit/quix-embeddable?file=src/App.js)

### Note

> There is a way to support this project?

**Yes!** Simply use the software, open issue if there is improvement/bug fixing that can be done and say thanks! It is
enough

Issue can be sent here. [Issue board](https://gitlab.com/tndsite/quix-v2/-/issues)

## License

MIT. Copyright (c) [Drakonseal](https://gitlab.com/tndsite/quix-v2/-/blob/master/LICENSE).
